﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDExplore
{
    public class Electrode
    {
        public struct BiosemiLoc
        {
            public int[] channelNumber;
            public double[] degrees;
            public double[] arcLength;
            public string[] channelName;
        };
    }
}

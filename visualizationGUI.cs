﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Threading;
using GUI;

namespace visualizationGUI
{
    public partial class Form2 : Form
    {
        // We're storing the reference here. 
        private string modelFilePath;
        private loadData box;
        private About about;
        private double[, ,] MRIdata;
        private double[,] transverse, sagittal, coronal;
        public int[] position;
        private double max;
        public int x, y, z;
        private int fileCounter;
        public header.ModelFileHeader modelFileHeader;
        private ColorMap cm;
        public Form2(string textBoxFilename, header.ModelFileHeader modelHeader)
        {
            InitializeComponent();
            modelFilePath = textBoxFilename;
            modelFileHeader = modelHeader;
            this.Text = "Visualization GUI";
            // initialize the picturebox 
            /*pictureBox1.Width = 256;
            pictureBox1.Height = 256;
            pictureBox2.Width = 256;
            pictureBox2.Height = 256;
            pictureBox3.Width = 256;
            pictureBox3.Height = 256;
            initImg = new double[pictureBox1.Width, pictureBox1.Height];
            for (int i = 0; i < pictureBox1.Height; i++)
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {
                    initImg[i, j] = 100;
                }
            }
            Bitmap bmp = getBitmap(initImg, pictureBox1.Width, pictureBox1.Height); //shown the second slice
            this.pictureBox1.Image = bmp;
            this.pictureBox2.Image = bmp;
            this.pictureBox3.Image = bmp;
            */
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            //buttonOpenImage.Enabled = false;
            buttonPrint.Enabled = false;
            buttonSave.Enabled = false;
            buttonPaint.Enabled = false;
            position = new int[3];
            position[0] = pictureBox1.Width / 2;
            position[1] = pictureBox1.Height / 2;
            position[2] = pictureBox3.Height / 2;
            OpenImage();
        }

        private Bitmap getBitmap(double[,] img, int height, int width)
        {

            byte[] buffer = new byte[height * width * 4];
            int count = 0;
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    for (int l = 0; l < 3; ++l)
                    {
                        buffer[count++] = (byte)img[i, j];
                    }
                    buffer[count++] = 255;
                }
            }
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
            bitmap.UnlockBits(bmpData);

            return bitmap;
        }

        //private void buttonOpenImage_Click(object sender, EventArgs e)
        private void OpenImage()
        {
            //this.Size = new Size(1084, 498);
            //panel1.Size = new Size(1078,465);
            //panel3.Size = new Size(1073, 265);
            /*
            if (textBoxX.Text.Length == 0)
            {
                MessageBox.Show("Input X dimention value is wrong.");
                return;
            }
            else
            {
                x = Convert.ToInt32(textBoxX.Text.ToString());
            }


            if (textBoxY.Text.Length == 0)
            {
                MessageBox.Show("Input Y dimention value is wrong.");
                return;
            }
            else
            {
                y = Convert.ToInt32(textBoxY.Text.ToString());
            }

            if (textBoxZ.Text.Length == 0)
            {
                MessageBox.Show("Input Z dimention value is wrong.");
                return;
            }
            else
            {
                z = Convert.ToInt32(textBoxZ.Text.ToString());
            }
            */


            //OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            //string modelFilePath = modelFilePath.Replace("modelFile", "mixingMatrix");
            
            if (File.Exists(modelFilePath)) // Test result.
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    //Form2.Cursor = Cursors.WaitCursor;
                    //progressBar1.Visible = true;
                    //progressBar1.Minimum = 0;
                    //progressBar1.Maximum = x*y*z-1;
                    box = new loadData();
                    box.Show();
                    box.Cursor = Cursors.WaitCursor;
                    max = 0;
                    using (BinaryReader binReader = new BinaryReader(File.Open(modelFilePath, FileMode.Open)))
                    {
                        //Console.WriteLine(dataPosition);
                        //Console.ReadLine();
                        //Console.ReadKey(true);
                        // Advance our position variable.
                        //for (int l = 0; l < dataPosition; ++l)
                        //{
                            // Read double.
                            //binReader.ReadDouble();
                            //Console.WriteLine(binReader.ReadDouble());
                            //Console.ReadLine();
                            //Console.ReadKey(true);
                        //}
                        /*headerInfo = new double[6];
                        for (int k = 0; k < 6; ++k)
                        {
                            // Read double.
                            headerInfo[k] = binReader.ReadDouble();

                            // Advance our position variable.
                            
                        }

                        //binReader.BaseStream.Seek(dataPosition, SeekOrigin.Begin);
                        binReader.BaseStream.Seek((long)((headerInfo[0]*headerInfo[1]*headerInfo[2]*12+6)*sizeof(double)), SeekOrigin.Begin);
                        x = Convert.ToInt16(binReader.ReadDouble());
                        y = Convert.ToInt16(binReader.ReadDouble());
                        z = Convert.ToInt16(binReader.ReadDouble());
                        */
                        double totalElectrodeRow = 0;
                        for (int i = 0; i< modelFileHeader.nSetup; ++i)
                        {
                            totalElectrodeRow = totalElectrodeRow + modelFileHeader.nElectrode[i];
                        }
                        long startPosition = Convert.ToInt64(modelFileHeader.nHeaderFields + modelFileHeader.nTargets*modelFileHeader.nOrientations*modelFileHeader.nCriteria*totalElectrodeRow);
                        binReader.BaseStream.Seek(startPosition * sizeof(double), SeekOrigin.Begin);
                        x = Convert.ToInt16(modelFileHeader.x);
                        y = Convert.ToInt16(modelFileHeader.y);
                        z = Convert.ToInt16(modelFileHeader.z);

                        MRIdata = new double[y, x, z];
                        transverse = new double[y, x];
                        sagittal = new double[z, y];
                        coronal = new double[z, x];

                        position[0] = (int)(x / 2) - 1;
                        position[1] = (int)(y / 2) - 1;
                        position[2] = (int)(z / 2) - 1;

                        for (int k = 0; k < x; ++k)
                        {
                            Application.DoEvents();
                            for (int j = 0; j < y; ++j)
                            {
                                for (int i = z - 1; i >= 0; --i)
                                {
                                    MRIdata[j, k, i] = binReader.ReadDouble();

                                    if (MRIdata[j, k, i] > max)
                                    {
                                        max = MRIdata[j, k, i];
                                    }
                                }
                            }
                        }
                    }

                    for (int j = 0; j < x; ++j)
                    {
                        for (int k = 0; k < y; ++k)
                        {
                            transverse[k, j] = MRIdata[k, j, position[2]] / max * 255;
                        }
                    }
                    cm = new ColorMap();
                    Bitmap bmp = getBitmap(transverse, y, x); //shown the second slice
                    pictureBox1.Image = bmp;
                    int[,] cmap = new int[64, 150];
                    
                    
                    //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox1.Height = bmp.Height;
                    pictureBox1.Width = bmp.Width;
                    pictureBox1.BorderStyle = BorderStyle.None;
                    //pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
                    //pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave); 
                    Graphics gr = Graphics.FromImage(bmp);
                    Pen p = new Pen(Color.Red);
                    p.Width = 1.0f;
                    //double pixelInensity = 
                    //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
                    gr.DrawRectangle(p, position[0], position[1], 1, 1);

                    for (int i = 0; i < z; ++i)
                    {
                        for (int j = 0; j < y; ++j)
                        {
                            sagittal[i, j] = MRIdata[j, position[0], i] / max * 255;
                        }
                    }

                    pictureBox2.Location = new Point(pictureBox1.Location.X + pictureBox1.Width + 10, pictureBox1.Location.Y);
                    bmp = getBitmap(sagittal, z, y);
                    pictureBox2.Image = bmp;
                    //pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox2.Height = bmp.Height;
                    pictureBox2.Width = bmp.Width;
                    pictureBox2.BorderStyle = BorderStyle.None;
                    //pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
                    //pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);

                    gr = Graphics.FromImage(bmp);
                    // p = new Pen(Color.Red);
                    //p.Width = 1.0f;
                    //double pixelInensity = 
                    //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
                    gr.DrawRectangle(p, position[1], position[2], 1, 1);

                    for (int i = 0; i < z; ++i)
                    {
                        for (int k = 0; k < x; ++k)
                        {
                            coronal[i, k] = MRIdata[position[1], k, i] / max * 255;
                        }
                    }

                    pictureBox3.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 10, pictureBox2.Location.Y);
                    bmp = getBitmap(coronal, z, x);
                    pictureBox3.Image = bmp;
                    //pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox3.Height = bmp.Height;
                    pictureBox3.Width = bmp.Width;
                    pictureBox3.BorderStyle = BorderStyle.None;
                    //pictureBox3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseMove);
                    //pictureBox3.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);

                    gr = Graphics.FromImage(bmp);
                    //p = new Pen(Color.Red);
                    //p.Width = 1.0f;
                    //double pixelInensity = 
                    //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
                    gr.DrawRectangle(p, position[0], position[2], 1, 1);

                    //pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
                    label3.Text = "Initial Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
                    label4.Text = "Initial Position: {" + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
                    label4.Location = new Point(pictureBox1.Location.X + pictureBox1.Width + 10, label3.Location.Y);
                    label5.Text = "Initial Position: {" + (position[0] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
                    label5.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 10, label4.Location.Y);

                    label1.Location = new Point(pictureBox1.Location.X + pictureBox1.Width + 10, label10.Location.Y);
                    label2.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 10, label10.Location.Y);

                    int maxHeight = pictureBox1.Height;
                    if (pictureBox2.Height > pictureBox1.Height)
                    {
                        maxHeight = pictureBox2.Height;
                    }
                    if (pictureBox3.Height > maxHeight)
                    {
                        maxHeight = pictureBox3.Height;
                    }

                    //panel3.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y);
                    panel3.Size = new Size(pictureBox3.Location.X + pictureBox3.Width + 10, maxHeight + 10);
                    //labelTransverseView.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + maxHeight + 15);
                    //labelSagittalView.Location = new Point(pictureBox2.Location.X, labelTransverseView.Location.Y);
                    //labelCoronalView.Location = new Point(pictureBox3.Location.X, labelTransverseView.Location.Y);

                    //this.Height = labelTransverseView.Location.Y + 10;

                    //this.Size = new System.Drawing.Size(this.Size.Width, labelTransverseView.Location.Y + 10);
                    box.Close();
                    pictureBox1.Enabled = true;
                    pictureBox2.Enabled = true;
                    pictureBox3.Enabled = true;
                    buttonPrint.Enabled = true;
                    buttonSave.Enabled = true;
                    buttonPaint.Enabled = true;

                    this.Cursor = Cursors.Default;
                    this.AutoSize = true;

                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                }
            }
            else
            {
                MessageBox.Show(modelFilePath + " not found.");
                return;
            }
        }

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Create a local version of the graphics object for the PictureBox.
            //Graphics g = e.Graphics;
            Graphics g = Graphics.FromImage(pictureBox1.Image);
            // Draw a string on the PictureBox.
            g.DrawString("This is a diagonal line drawn on the control",
                new Font("Arial", 10), System.Drawing.Brushes.Blue, new Point(30, 30));
            // Draw a line in the PictureBox.
            g.DrawLine(System.Drawing.Pens.Red, pictureBox1.Left, pictureBox1.Top,
                pictureBox1.Right, pictureBox1.Bottom);
        }



        private Point initialMousePos;

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //Point mouseDownLocation = (Control)sender.PointToClient(new Point(e.X, e.Y));
            this.initialMousePos = e.Location;
            position[0] = this.initialMousePos.X;
            position[1] = this.initialMousePos.Y;

            // update contents shown in pictureBox1
            for (int j = 0; j < x; ++j)
            {
                for (int k = 0; k < y; ++k)
                {
                    transverse[k, j] = MRIdata[k, j, position[2]] / max * 255;
                }
            }
            Bitmap bmp = getBitmap(transverse, y, x); //shown the second slice
            pictureBox1.Image = bmp;
            Graphics gr = Graphics.FromImage(bmp);
            Pen p = new Pen(Color.Red);
            p.Width = 1.0f;
            //double pixelInensity = 
            //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
            gr.DrawRectangle(p, position[0], position[1], 1, 1);
            //string coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(position[1]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[1]);
            label3.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
            pictureBox1.Image = bmp;
            pictureBox1.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    sagittal[i, j] = MRIdata[j, position[0], i] / max * 255;
                }
            }
            bmp = getBitmap(sagittal, z, y);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[1], position[2], 1, 1);
            //coordinate = Convert.ToString(position[1]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[1], position[2]);
            label4.Text = "Selected Position: {" + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox2.Image = bmp;
            pictureBox2.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int k = 0; k < x; ++k)
                {
                    coronal[i, k] = MRIdata[position[1], k, i] / max * 255;
                }
            }

            bmp = getBitmap(coronal, z, x);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[0], position[2], 1, 1);
            //coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[2]);
            label5.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox3.Image = bmp;
            pictureBox3.Invalidate();

            //bmp.Dispose();
            gr.Dispose();
            p.Dispose();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label10.Text = "Transverse View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label1.Text = "Sagittal View {" + (this.initialMousePos.Y+1).ToString() + "," + (position[2]+1).ToString() + "}";
            label2.Text = "Coronal View {" + (this.initialMousePos.X+1).ToString() + "," + (position[2]+1).ToString() + "}";
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            label10.Text = "Transverse View";
            label1.Text = "Sagittal View";
            label2.Text = "Coronal View";
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            //Point mouseDownLocation = (Control)sender.PointToClient(new Point(e.X, e.Y));
            this.initialMousePos = e.Location;
            position[1] = this.initialMousePos.X;
            position[2] = this.initialMousePos.Y;

            // update contents shown in pictureBox1
            for (int j = 0; j < x; ++j)
            {
                for (int k = 0; k < y; ++k)
                {
                    transverse[k, j] = MRIdata[k, j, position[2]] / max * 255;
                }
            }
            Bitmap bmp = getBitmap(transverse, y, x); //shown the second slice
            pictureBox1.Image = bmp;
            Graphics gr = Graphics.FromImage(bmp);
            Pen p = new Pen(Color.Red);
            p.Width = 1.0f;
            //double pixelInensity = 
            //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
            gr.DrawRectangle(p, position[0], position[1], 1, 1);
            //string coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(position[1]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[1]);
            label3.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
            pictureBox1.Image = bmp;
            pictureBox1.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    sagittal[i, j] = MRIdata[j, position[0], i] / max * 255;
                }
            }
            bmp = getBitmap(sagittal, z, y);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[1], position[2], 1, 1);
            //coordinate = Convert.ToString(position[1]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[1], position[2]);
            label4.Text = "Selected Position: {" + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox2.Image = bmp;
            pictureBox2.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int k = 0; k < x; ++k)
                {
                    coronal[i, k] = MRIdata[position[1], k, i] / max * 255;
                }
            }

            bmp = getBitmap(coronal, z, x);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[0], position[2], 1, 1);
            //coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[2]);
            label5.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox3.Image = bmp;
            pictureBox3.Invalidate();

            //bmp.Dispose();
            gr.Dispose();
            p.Dispose();
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label1.Text = "Sagittal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y+1).ToString() + "}";
            label10.Text = "Transverse View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.X+1).ToString() + "}";
            label2.Text = "Coronal View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            label10.Text = "Transverse View";
            label1.Text = "Sagittal View";
            label2.Text = "Coronal View";
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            //Point mouseDownLocation = (Control)sender.PointToClient(new Point(e.X, e.Y));
            this.initialMousePos = e.Location;
            position[0] = this.initialMousePos.X;
            position[2] = this.initialMousePos.Y;

            // update contents shown in pictureBox1
            for (int j = 0; j < x; ++j)
            {
                for (int k = 0; k < y; ++k)
                {
                    transverse[k, j] = MRIdata[k, j, position[2]] / max * 255;
                }
            }
            Bitmap bmp = getBitmap(transverse, y, x); //shown the second slice
            pictureBox1.Image = bmp;
            Graphics gr = Graphics.FromImage(bmp);
            Pen p = new Pen(Color.Red);
            p.Width = 1.0f;
            //double pixelInensity = 
            //gr.DrawLine(p, this.initialMousePos.X, this.initialMousePos.Y, this.initialMousePos.X+1, this.initialMousePos.Y+1);
            gr.DrawRectangle(p, position[0], position[1], 1, 1);
            //string coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(255-position[1]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[1]);
            label3.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
            pictureBox1.Image = bmp;
            pictureBox1.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    sagittal[i, j] = MRIdata[j, position[0], i] / max * 255;
                }
            }
            bmp = getBitmap(sagittal, z, y);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[1], position[2], 1, 1);
            //coordinate = Convert.ToString(position[1]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[1], position[2]);
            label4.Text = "Selected Position: {" + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox2.Image = bmp;
            pictureBox2.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < z; ++i)
            {
                for (int k = 0; k < x; ++k)
                {
                    coronal[i, k] = MRIdata[position[1], k, i] / max * 255;
                }
            }

            bmp = getBitmap(coronal, z, x);
            gr = Graphics.FromImage(bmp);
            p = new Pen(Color.Red);
            gr.DrawRectangle(p, position[0], position[2], 1, 1);
            //coordinate = Convert.ToString(position[0]) + "," + Convert.ToString(position[2]);
            //gr.DrawString(coordinate,
            //new Font("Arial", 10), System.Drawing.Brushes.Blue, position[0], position[2]);
            label5.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            pictureBox3.Image = bmp;
            pictureBox3.Invalidate();

            //bmp.Dispose();
            gr.Dispose();
            p.Dispose();
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label2.Text = "Coronal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y+1).ToString() + "}";
            label10.Text = "Transverse View {" + (this.initialMousePos.X+1).ToString() + "," + (position[1]+1).ToString() + "}";
            label1.Text = "Sagittal View {" + (position[1] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            label10.Text = "Transverse View";
            label1.Text = "Sagittal View";
            label2.Text = "Coronal View";
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            
            PrintDocument doc = new PrintDocument();
            //doc.PrintPage += this.Doc_PrintPage;
            PrintPreviewDialog previewdlg = new PrintPreviewDialog();
            // Associate the PrintPage event handler with the PrintPage event.
            doc.PrintPage += new PrintPageEventHandler(Doc_PrintPage);

            PrintDialog dlgSettings = new PrintDialog();
            dlgSettings.Document = doc;
            dlgSettings.UseEXDialog = true;
            //doc.OriginAtMargins = true;
            fileCounter = 3;
            if (dlgSettings.ShowDialog() == DialogResult.OK)
            {
                //previewdlg.Document = doc;
                //previewdlg.ShowDialog();
                doc.Print();
            }
        }


        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Font font = new Font("Arial", 18);

            float x = e.MarginBounds.Left;
            float y = e.MarginBounds.Top;

            float lineHeight = font.GetHeight(e.Graphics);

            float W = e.MarginBounds.Width;
            float H = e.MarginBounds.Height;
            string[] toPrint = { "pictureBox1.Image", "pictureBox2.Image", "pictureBox3.Image"};
            
            /*for (; fileCounter >= 0; fileCounter--)
            {
                try
                {
                    Bitmap Bmp = new Bitmap(toPrint[fileCounter]);
                    if (Bmp.Width / W < Bmp.Height / H)
                        W = Bmp.Width * H / Bmp.Height;
                    else
                        H = Bmp.Height * W / Bmp.Width;
                    e.Graphics.DrawImage(Bmp, 0, 0, W, H);
                    break;
                }
                catch
                {
                }
            }
            */
            if (fileCounter == 3)
            {
                if (pictureBox1.Image.Width / W < pictureBox1.Image.Height / H)
                    W = pictureBox1.Image.Width * H / pictureBox1.Image.Height;
                else
                    H = pictureBox1.Image.Height * W / pictureBox1.Image.Width;
                e.Graphics.DrawImage(pictureBox1.Image, 0, 0, W, H);
            }
            if (fileCounter == 2)
            {
                if (pictureBox2.Image.Width / W < pictureBox2.Image.Height / H)
                    W = pictureBox2.Image.Width * H / pictureBox2.Image.Height;
                else
                    H = pictureBox2.Image.Height * W / pictureBox2.Image.Width;
                e.Graphics.DrawImage(pictureBox2.Image, 0, 0, W, H);
            }
            if (fileCounter == 1)
            {
                if (pictureBox3.Image.Width / W < pictureBox3.Image.Height / H)
                    W = pictureBox3.Image.Width * H / pictureBox3.Image.Height;
                else
                    H = pictureBox3.Image.Height * W / pictureBox3.Image.Width;
                e.Graphics.DrawImage(pictureBox3.Image, 0, 0, W, H);
            }

            fileCounter -= 1;
            if (fileCounter > 0)
            {
                e.HasMorePages = true;
            }
            else
            {
                fileCounter = toPrint.Length - 1;
            }








            /*
            int i = 0;
            //for (int i = 0; i < 1; i++)
            //{
            int charactersOnPage = 0;
            int linesPerPage = 0;
            //string stringToPrint = "This is line " + i.ToString() + " of report.";
            //string stringToPrint = "But given the volatility in the markets recently, analysts were cautious about the prospects for the broad gains to stick. It is a bit of a relief rally, said Paul Zemsky, the chief investment officer of multiasset strategies for ING Investment Management. There was “a favorable outcome” to the German court ruling and the market was responding, he said, “but we need to see follow through.”Some analysts said that the recent declines in the sector appeared to be overdone. In a research report, analysts from Deutsche Bank noted that bank stocks have declined by 24 percent since July 21, the date to which the most recent sell-off period is often traced, while the broader market as measured by the Standard & Poor’s 500-stock index was down by 13 percent.“While numerous macro concerns remain, we believe the sell-off is overdone” if gross domestic product growth is more than 1 percent, the analysts said.Economists have been recalculating their outlook for the economy in the light of softer economic data and, to some extent, recent stock market volatility has increased the uncertainty for businesses. On Wednesday, a Federal Reserve survey of its 12 districts reported that many businesses had downgraded or become more cautious about their near-term outlooks.But the markets in the United States are also intertwined with global economies, and Mr. Zemsky noted that new industrial data from Germany provided some support. The data said German industrial production surged 4 percent in July, above expectations and reversing a decline in June. Still, the country must cope with slack demand.“It looks like the economies around the world are slowing, not stopping,” Mr. Zemsky said.Corporate news also propelled trading in key sectors.Bank orica was the most actively traded financial stock, and it rose nearly 7 percent. The bank shook up its top management team on Tuesday as it contended with a flagging share price and mounting legal liabilities.The technology sector rose solidly, led by Yahoo, which was up more than 5 percent. The company’s chief executive, Carol A. Bartz, was fired Tuesday, ending a rocky two-year tenure in which she tried to revitalize the online media company.HSBC hammered home the point on Wednesday that global economic fundamentals were significantly weaker now than before.The bank’s team of economists lowered their growth forecasts for this year and next, with particularly marked revisions for the developed world. They forecast that developed economies would expand just 1.3 percent this year, down from a previous projection of 1.8 percent. Growth in emerging economies is likely to hit 6.2 percent, rather than 6.3 percent, they said.“The developed world has succumbed to economic permafrost,” the team, headed by the global economist Stephen King, wrote in its report. “The message is simple: despite massive policy stimulus, healthy economic recovery is now but a distant dream.”In afternoon trading, the Euro Stoxx 50 rose 3.4 percent, although it was still down more than 20 percent for the year. In Britain, the FTSE 100 index rose 3.1 percent. The CAC 40 in France closed up 3.6 percent and the DAX in Germany rose more than 4 percent.The Nikkei 225 index in Japan, which closed at its weakest level since April 2009 on Tuesday, recouped some of the previous session’s losses with a rise of 2 percent.The Hang Seng in Hong Kong closed up 1.7 percent, while the Shanghai composite index gained 1.8 percent.";
            //e.HasMorePages = true;
            //Console.WriteLine(stringToPrint);
            while (e.HasMorePages)
            {
                // Sets the value of charactersOnPage to the number of characters 
                // of stringToPrint that will fit within the bounds of the page.
                e.Graphics.MeasureString(stringToPrint, font,
                    e.MarginBounds.Size, StringFormat.GenericTypographic,
                    out charactersOnPage, out linesPerPage);
                e.Graphics.DrawString(stringToPrint, font, Brushes.Black, e.MarginBounds, StringFormat.GenericTypographic);//x, y);
                // Remove the portion of the string that has been printed.
                stringToPrint = stringToPrint.Substring(charactersOnPage);

                // Check to see if more pages are to be printed.
                e.HasMorePages = (stringToPrint.Length > 0);
                //Console.WriteLine(e.HasMorePages);
                //Console.WriteLine(charactersOnPage);
                //Console.WriteLine(stringToPrint);
                //Console.ReadKey(true);
            }

            */
            //y += lineHeight;
            //}
            //y += lineHeight;

            //Bitmap panelImage = new Bitmap(panel3.Width, panel3.Height);
            //panel3.DrawToBitmap(panelImage, panel3.ClientRectangle);
            //e.Graphics.DrawImage(panelImage, x, y);
            /*
            e.Graphics.DrawString("Transverse view", font, Brushes.Black, x, y);
            y += lineHeight;
            e.Graphics.DrawImage(pictureBox1.Image, x, y);
            y = y + pictureBox1.Image.Height + lineHeight;
            e.Graphics.DrawString("Sagittal view", font, Brushes.Black, x, y);
            y += lineHeight;
            e.Graphics.DrawImage(pictureBox2.Image, x, y);
            y = y + pictureBox2.Image.Height + lineHeight;
            e.Graphics.DrawString("Coronal view", font, Brushes.Black, x, y);
            y += lineHeight;
            e.Graphics.DrawImage(pictureBox3.Image, x, y);
            y += lineHeight;
            */
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                Bitmap panelImage = new Bitmap(panel3.Width, panel3.Height);
                panel3.DrawToBitmap(panelImage, panel3.ClientRectangle);

                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }

                fs.Close();
            }
        }

        private void buttonPaint_Click(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(pictureBox2.Image);
            Graphics gr = Graphics.FromImage(bmp);
            Pen p = new Pen(Color.Red, 1);
            p.Width = 2.0f;
            //gr.DrawRectangle(p, 10, 20, 30, 40);
            //gr.SmoothingMode = SmoothingMode.AntiAlias;
            //gr.FillRectangle(Brushes.White, this.ClientRectangle);

            //p.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            //LineCap.Round;
            p.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            gr.DrawLine(p, 126, 46, 143, 46);
            p.Dispose();
            pictureBox2.Image = bmp;
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            about = new About();
            about.Show();
        }
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Threading;
using System.Drawing.Drawing2D;
using HDExplore;

// current: -2 mA to 2 mA
// field: range 0 V/m to maxField V/m. 
// values are normalized according to the (colormapLength-1). -1 is to adjust for C# indexing
namespace GUI
{
    public partial class Form1 : Form
    {
        private About about;
        private LoadData box;
        private newSession newSessionWindow;
        public ModelFileHeader.DataHeader modelFileHeader;
        public Electrode.BiosemiLoc biosemiLoc;
        // electric field
        private double[, , ,] electricFields;
        private double[, ,] electricField3D, brainMask, mri;
        private double maxField, maxField3D, fieldScale, scaleFactor, percentileRank;
        private int maxX, maxY, maxZ;
        private double[,] transeverse, sagittal, coronal, electrodeCenterCoorginates;
        private int[] position;
        private int overlayAlpha;
        //current intensity
        private double[] currentIntensity;
        private double[] currentIntensityNormalized;
        private double currentMax;
        private double currentMin;
        //colormap
        private int colormapLength;
        private int alpha;
        private int colorbarWidth;
        private int[,] colormapJet, colormapGray;
        private double electricFieldColormapMax, electricFieldColormapMin, mriMax;
        private string modelDataPath, dataPath;
        private int fileCounter;
        private bool showField, showMRI, showOverlay, showBackgroundImage, selectElectrode;
        private string reference;
        private bool isElectrode, isRepeated, currentValueChanged;
        private Image pictureBox2BackgroundImage0, pictureBox2BackgroundImage1;
        //topplot
        private int circleRadius, electrodeSize, leftTopX, leftTopY;
        //TODO
        private int offset;
        private int currentTabIndex = 0;
        private int dataGridViewCellValueChangedRows;
        private int fieldOrientationStep = 10;

        private int number_of_anodes = 0;
        private int number_of_cathodes = 0;
        private int old_anode_index = 0;
        private int old_cathode_index = 0;

        private ArrayList electrodes_list = new ArrayList();

        // Class to change the background color of the menu 
        public class RedTextRenderer : System.Windows.Forms.ToolStripRenderer
        {
            protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
            {
                e.TextColor = Color.Black;
                e.ToolStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234))))); //Color of tool Strip at top CNTTEST
                base.OnRenderItemText(e);
            }
        }

        public Form1()
        {
            // initialize the form
            InitializeComponent();
            pictureBox2BackgroundImage0 = panel2.BackgroundImage;
            pictureBox2BackgroundImage1 = pictureBox2BackgroundImage0;
            // set the position of the form components.
            this.Location = new Point(1, 1);
            // set the initial mouse click position
            position = new int[3];
            position[0] = pictureBox3.Width / 2;
            position[1] = pictureBox3.Height / 2;
            position[2] = pictureBox1.Height / 2;
            // set the colorbar parameters
            colormapLength = 256;
            alpha = 255;
            colorbarWidth = 30;
            // read the toolTip message from configuration XML
            ReadXMLDocument();
            // customize the form
            initialGUIState();
            panel2.BackgroundImage = pictureBox2BackgroundImage0;
            //panel3.Visible = false;
            pictureBox8.Visible = false;
            tabControl1.Location = new Point(0, panel1.Height - textBoxName.Location.Y - textBoxName.Height - 10);
            tabControl1.Size = new Size(panel1.Width, panel1.Height - textBoxName.Location.Y - textBoxName.Height - 10);
            tabControl1.ItemSize = new Size(Convert.ToInt16(panel1.Width / 3 - 5), tabControl1.ItemSize.Height);
            // offset between windows 7 and window xp: window xp : offset  = 10; windows 7 : offset = 0;
            offset = 10;
            // change the background color of the menu 
            ToolStripManager.Renderer = new RedTextRenderer();
            currentMax = Convert.ToDouble(2.0);
            currentMin = -currentMax;
            // set activility of electrode models.
            string mDataPath = dataPath + "Adult_male_1_93_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_1_93_electrode_model.Enabled = false;
            }
            mDataPath = dataPath + "Adult_male_1_332_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_1_332_electrode_model.Enabled = false;
            }
            mDataPath = dataPath + "Adult_male_2_93_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_2_93_electrode_model.Enabled = false;
            }
            mDataPath = dataPath + "Adult_male_3_93_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_3_93_electrode_model.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_AH\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_AH.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_DE\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_DE.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_JC\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_JC.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_JE\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_JE.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_KS\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_KS.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_LM\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_LM.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_MC1\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_MC1.Enabled = false;
            }
            mDataPath = dataPath + "Stroke_patient_RS\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Stroke_patient_RS.Enabled = false;
            }
            mDataPath = dataPath + "MNI_152_93_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                MNI_152_93_electrode_model.Enabled = false;
            }
        }

        private void initialGUIState()
        {
            this.Size = new Size(1024, 726);
            panel1.Size = new Size(pictureBox7.Width + 10, 693);
            panel2.Size = new Size(1024 - panel1.Width, 693);
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            selectElectrode = false;
            printToolStripMenuItem.Enabled = false;
            buttonField.Visible = false;
            buttonField.Enabled = false;
            buttonMRI.Visible = false;
            buttonMRI.Enabled = false;
            buttonOverlay.Visible = false;
            buttonOverlay.Enabled = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
            labelFieldIntensity.Visible = false;
            label19.Visible = false;
            label20.Visible = false;
            label21.Visible = false;
            label22.Visible = false;
            label23.Visible = false;
            label24.Visible = false;
            label25.Visible = false;
            label26.Visible = false;
            label27.Visible = false;
            label28.Visible = false;
            label29.Visible = false;
            label30.Visible = false;

            groupBox1.Visible = false;

            label13.Visible = false;

            domainUpDown1.Visible = false;
            domainUpDown2.Visible = false;
            domainUpDown3.Visible = false;
            domainUpDown4.Visible = false;
            domainUpDown5.Visible = false;

            showMRI = false;
            showField = false;
            showOverlay = false;
            showBackgroundImage = false;

            textBox_current.Visible = false;
            radioButton_anode.Visible = false;
            radioButton_cathode.Visible = false;
            //toolStrip4.Enabled = false;
            label_total_current.Visible = false;
            groupBox3.Visible = false;
            label3.Visible = false;
            toolStrip4.Visible = false;
            groupBox_pads.Visible = false;
            panel5.Visible = false;
            dataGridView1.Visible = false;

            saveAll3ToolStripMenuItem.Enabled = false;

            pictureBox1.BackgroundImage = null;
            pictureBox2.BackgroundImage = null;
            pictureBox3.BackgroundImage = null;
            alpha = 255;
            dataGridView1.Size = new Size(dataGridView1.Width, textBox2.Location.Y - dataGridView1.Location.Y - 10);
            groupBox2.Location = new Point(0, textBox3.Location.Y + textBox3.Height + 20);
            groupBox4.Location = new Point(0, groupBox2.Location.Y + groupBox2.Height);
            groupBox5.Location = new Point(0, textBox4.Location.Y - groupBox5.Height - 3);
            groupBox4.Size = new Size(groupBox4.Width, groupBox5.Location.Y - groupBox4.Location.Y);
            dataGridView2.Size = new Size(dataGridView2.Width, groupBox4.Height - label4.Height - 40);
            pictureBox1.Location = new Point(24, 75);
            pictureBox1.Size = new Size(panel2.Size.Width - 2 * pictureBox1.Location.X - 10, panel2.Size.Height - 2 * pictureBox1.Location.Y);
            pictureBox1.BackColor = Color.White;
            //pictureBox2.BackColor = Color.White;
            pictureBox8.BackColor = Color.White;

            //tissue and view selection
            comboBox3.Visible = false;
            comboBox4.Visible = false;
            label12.Visible = false;
            label17.Visible = false;
            label18.Visible = false;
        }

        private void toolStripComboBox1_TextChanged(object sender, EventArgs e)
        {
            toolStripComboBox2.Items.Clear();
            comboBox3.ResetText();
            comboBox4.ResetText();
            tissue_image.Visible = false;
            if (toolStripComboBox1.Text != "Subject")
            {
                modelDataPath = dataPath + toolStripComboBox1.Text + "\\";
                initialGUIState();
                panel2.BackgroundImage = null;
                colorbarWidth = 0;
                bool electrodeLocationFileExist = showElectrodeLocation();
                if (electrodeLocationFileExist)
                {
                    currentIntensity = new double[modelFileHeader.nElectrode];
                    for (int i = 2; i < modelFileHeader.nElectrode; ++i)
                        toolStripComboBox2.Items.Add(Convert.ToString(i));

                    toolStrip4.Enabled = true;
                    button1.Enabled = false;
                    textBox7.Text = toolStripComboBox1.Text;
                }
                else
                {
                    panel2.BackgroundImage = pictureBox2BackgroundImage0;
                    toolStripComboBox1.Text = "Subject";
                    textBox7.Text = "";
                }
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
               
               textBox8.Text = toolStripComboBox2.Text;
            
            // tissue and view selection
               comboBox3.Visible = true;
               comboBox4.Visible = true;
               label12.Visible = true;
               label17.Visible = true;
               label18.Visible = true;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        { 
            if (tabControl1.SelectedTab == tabPage3) 
            {
                if (radioButton_flexible.Checked) // CNT 06/10/2015 Correct number of electrodes for flexible choice
                {
                    textBox8.Text = toolStripComboBox2.Text; 
                }
                else textBox8.Text = (number_of_anodes + number_of_cathodes).ToString();

                }
            if (tabControl1.SelectedTab == tabPage3 && groupBox1.Visible == false)
            {
                for (int i = 0; i < dataGridView1.RowCount; ++i)
                {
                    dataGridView2.Rows[i].Cells[0].Value = Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).ToUpper();
                    dataGridView2.Rows[i].Cells[0].Selected = false;
                    dataGridView2.Rows[i].Cells[1].Value = dataGridView1.Rows[i].Cells[1].Value;
                    dataGridView2.Rows[i].Cells[1].Selected = false;
                }
            }

            if (tabControl1.SelectedTab == tabPage1)
            {
                pictureBox8.Enabled = false;
                pictureBox8.Visible = false;
            }

            if (tabControl1.SelectedTab == tabPage2)
            {
                tissue_image.Image = null;
                tissue_image.Visible = false;
                //reset number of electrodes in step 2
                toolStripComboBox2.Text = "sdf";
            }
            //reset zoom
            if (n == false) ZoomOut(pictureBox8);
            n = true;
        }

        private bool showElectrodeLocation()
        {
            bool electrodeLocationFileExist = false;
            string[] filePaths;
            try
            {
                filePaths = Directory.GetFiles(@modelDataPath, "*loc");
            }
            catch (Exception e1)
            {
                MessageBox.Show("Could not find a part of the path " + modelDataPath + ".");
                return electrodeLocationFileExist;
            }

            // only one LOC file for each model
            if (filePaths.Length > 0)
            {
                string electrodeLocationFilePath = filePaths[0];
                if (File.Exists(@electrodeLocationFilePath))
                {
                    readElectrodeLoc(electrodeLocationFilePath);
                    electrodeCenterCoorginates = new double[modelFileHeader.nElectrode, 2];
                }
                else
                {
                    MessageBox.Show(electrodeLocationFilePath + " does not exist.");
                    return electrodeLocationFileExist;
                }
            }
            else
            {
                MessageBox.Show("No LOC file found in " + modelDataPath + ".");
                return electrodeLocationFileExist;
            }

            electrodeLocationFileExist = true;
            MyColorMapClass cm = new MyColorMapClass(colormapLength, alpha);
            colormapJet = new int[colormapLength, 4];
            colormapJet = cm.Jet();

            colormapGray = new int[colormapLength, 4];
            colormapGray = cm.Gray();

            if (modelFileHeader.nElectrode >= 300)
            {
                circleRadius = (int)(panel2.Width / 4.5);
                electrodeSize = (int)(circleRadius / 12);
            }
            else
            {
                circleRadius = (int)(panel2.Width / 6);
                electrodeSize = (int)(circleRadius / 5.3);
            }

            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Size = new Size(panel2.Width - 5, panel2.Height - 5);

            leftTopX = (int)(pictureBox1.Width / 2 + 0.5) - circleRadius - colorbarWidth;
            leftTopY = (int)(pictureBox1.Height / 2 + 0.5) - circleRadius;
            double[] whiteTopplot = new double[modelFileHeader.nElectrode];
            for (int i = 0; i < whiteTopplot.Length; ++i)
                whiteTopplot[i] = colormapLength - 1;
            pictureBox1.Image = drawTopoPlot(panel2.Width, panel2.Height, whiteTopplot, colormapGray, leftTopX, leftTopY, circleRadius, electrodeSize, (float)5.1, true);
            pictureBox1.Visible = true;
            return electrodeLocationFileExist;
        }

        private void ReadXMLDocument()
        {
            // read the toolTip message from configuration XML
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };
            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "step1":
                                {
                                    tabControl1.TabPages[0].ToolTipText = reader.GetAttribute("step1Help");
                                    textBoxMessage.Text = reader.GetAttribute("step1Help");
                                    tabControl1.TabPages[0].Text = reader.GetAttribute("step1Tab");
                                    textBox6.Text = reader.GetAttribute("step1Title");
                                    toolStripComboBox1.Text = reader.GetAttribute("subjectSelectionDefaultText");
                                    item1dToolStripMenuItem.Text = reader.GetAttribute("normalSubject");
                                    item11ToolStripMenuItem.Text = reader.GetAttribute("normalSubject1");
                                    item12ToolStripMenuItem.Text = reader.GetAttribute("normalSubject2");
                                    adultMale3ToolStripMenuItem.Text = reader.GetAttribute("normalSubject3");
                                    item2ToolStripMenuItem.Text = reader.GetAttribute("strokesubject");
                                    Stroke_patient_AH.Text = reader.GetAttribute("strokesubject1");
                                    Stroke_patient_DE.Text = reader.GetAttribute("strokesubject2");
                                    Stroke_patient_JC.Text = reader.GetAttribute("strokesubject3");
                                    Stroke_patient_JE.Text = reader.GetAttribute("strokesubject4");
                                    Stroke_patient_KS.Text = reader.GetAttribute("strokesubject5");
                                    Stroke_patient_LM.Text = reader.GetAttribute("strokesubject6");
                                    Stroke_patient_MC1.Text = reader.GetAttribute("strokesubject7");
                                    Stroke_patient_RS.Text = reader.GetAttribute("strokesubject8");
                                    break;
                                }
                            case "step2":
                                {
                                    tabControl1.TabPages[1].ToolTipText = reader.GetAttribute("step2Help");
                                    textBox2.Text = reader.GetAttribute("step2Help");
                                    tabControl1.TabPages[1].Text = reader.GetAttribute("step2Tab");
                                    textBox1.Text = reader.GetAttribute("step2Title");
                                    label3.Text = reader.GetAttribute("numElectrode");
                                    toolTipDataGridView1.SetToolTip(this.dataGridView1, reader.GetAttribute("currentTableHelp"));
                                    dataGridView1.Columns[0].Name = reader.GetAttribute("currentTableLocation");
                                    dataGridView1.Columns[1].Name = reader.GetAttribute("currentTableCurrent");
                                    dataGridView2.Columns[0].Name = reader.GetAttribute("currentTableLocation");
                                    dataGridView2.Columns[1].Name = reader.GetAttribute("currentTableCurrent");
                                    toolTipNumElectrode.SetToolTip(label3, reader.GetAttribute("numElectrodeHelp"));
                                    break;
                                }
                            case "step3":
                                {
                                    tabControl1.TabPages[2].ToolTipText = reader.GetAttribute("step3Help");
                                    textBox3.Text = reader.GetAttribute("step3Title");
                                    textBox4.Text = reader.GetAttribute("step3Help");
                                    label2.Text = reader.GetAttribute("subjectStep3");
                                    label6.Text = label3.Text;
                                    tabControl1.TabPages[2].Text = reader.GetAttribute("step3Tab");
                                    button1.Text = reader.GetAttribute("explore");
                                    buttonOverlay.Text = reader.GetAttribute("overlayButton");
                                    buttonMRI.Text = reader.GetAttribute("mriButton");
                                    buttonField.Text = reader.GetAttribute("fieldButton");
                                    toolTipVisualizationGUI.SetToolTip(this.button1, reader.GetAttribute("computeFields"));
                                    toolTipDataGridView2.SetToolTip(this.dataGridView2, reader.GetAttribute("currentTableHelp"));
                                    break;
                                }
                            case "GUI":
                                {
                                    textBoxName.Text = reader.GetAttribute("softwareName");
                                    break;
                                }
                            case "dataPath":
                                {
                                    dataPath = reader.GetAttribute("dataPath");
                                    break;
                                }
                            case "electricField":
                                {
                                    overlayAlpha = Convert.ToInt16(reader.GetAttribute("overlayAlpha"));
                                    scaleFactor = Convert.ToDouble(reader.GetAttribute("scaleFactor"));
                                    percentileRank = Convert.ToDouble(reader.GetAttribute("percentileRank"));
                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private Bitmap getBitmap(double[,] img, int height, int width, int[,] cmap)
        {
            //change 2-d array to Bitmap.
            byte[] buffer = new byte[height * width * 4];
            int count = 0;
            for (int i = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                {
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 3]; //R
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 2]; //G
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 1]; //B
                    //if NaN, then make the pixel transparent
                    if (double.IsNaN(img[i, j]))
                        buffer[count++] = (byte)(cmap[(byte)(img[i, j]), 0] - 255);
                    else
                        buffer[count++] = (byte)alpha;
                }
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
            bitmap.UnlockBits(bmpData);
            return bitmap;
        }

        private double[] getMNIlocation(double[] location)
        {
            double[] MNI = new double[3];
            double[,] MRI2MNI;

            //transformation matrix for maroms head
            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")))
            {
            //transformation matrix (MRI2MNI)
            MRI2MNI = new double[4, 4] {
            {0.998622399112419,	-0.0501623708529074,	0.00626066613250805,	-63.1388880761555},
            {-0.0447528750291471,	-0.935012677655719,	-0.351778345365455,	91.5322494257266},
            {0.0234975058488254,	0.351048358558916,	-0.936062381559804,	39.5476441978910},
            {0,	0,	0,	1}};
            
            }
            else if(toolStripComboBox1.Text.Equals("MNI_152_93_electrode_model"))
            {
            //transformation matrix changed to 6th generation standard head AAA 01.07.2015
            //transformation matrix for the standard head
            MRI2MNI = new double[4, 4] {
            {-1,	0,	0,	90},
            {0,	1,	0,	-92},//218-126
            {0,	0,	1,	-110},//182-72
            {0,	0,	0,	1}};
            }
          else if (toolStripComboBox1.Text.Equals("Adult_male_2_93_electrode_model"))
            {

                //transformation matrix for the Lukas's Head
                MRI2MNI = new double[4, 4] {
            {-1,    0,  0,  83}, //Guessed values for anterior commissure 
            {0, 1,  0,  -112},
            {0, 0,  1, -96},
            {0, 0,  0,  1}};
            }
            else if(toolStripComboBox1.Text.Equals("Adult_male_3_93_electrode_model"))
                 //transformation matrix for Dylans's head
                MRI2MNI = new double[4, 4] {
            {-1,    0,  0,  85}, //Guessed values for anterior commissure 
            {0, 1,  0,  -98},
            {0, 0,  1, -95},
            {0, 0,  0,  1}};
            //matrix multiplication
            else //if(toolStripComboBox.Text.Equals("Stroke_patient_KS "))
                 //transformation matrix for Dylans's head
                MRI2MNI = new double[4, 4] {
            {-1,    0,  0,  85 }, //Guessed values for anterior commissure 
            {0, 1,  0,  -111},
            {0, 0,  1, -89},
            {0, 0,  0,  1}};
            //matrix multiplication
            return MNI = matrix_multiplication(MRI2MNI, location, 4);
        }

        private static double[] matrix_multiplication(double[,] trans_mat, double[] target_vec, int dimension)                                           //5
        {
            double[] new_mat4D = new double[dimension]; //4D for calculation
            double[] new_mat3D = new double[dimension - 1]; //return 3D location for display

            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)

                    new_mat4D[i] = new_mat4D[i] + (trans_mat[i, j] * target_vec[j]);

            }
            for (int k = 0; k < dimension - 1; k++)
            {
                new_mat3D[k] = new_mat4D[k];
            }
            return new_mat3D;
        }

        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            // sort dataGridView colume
            if (e.Column.Index == 2)
            {
                if (double.Parse(e.CellValue1.ToString()) > double.Parse(e.CellValue2.ToString()))
                    e.SortResult = 1;
                else if (double.Parse(e.CellValue1.ToString()) < double.Parse(e.CellValue2.ToString()))
                    e.SortResult = -1;
                else
                    e.SortResult = 0;
                e.Handled = true;
            }
        }

        private void showTopplot()
        {
            pictureBox6.Image = null;
            groupBox1.Visible = false;

            label13.Visible = false;
            domainUpDown1.Visible = false;
            domainUpDown2.Visible = false;
            domainUpDown3.Visible = false;

            if (isRepeated)
                return;
            if (dataGridView1.RowCount == 0 && !radioButton_uniform.Checked)
                initialGUIState();
            pictureBox1.Location = new Point(0, -20); //topoplot little higher for 332 model
            pictureBox1.Size = new Size(panel2.Width - 5, panel2.Height - 5);

            pictureBox1.Visible = false;

            if (modelFileHeader.nElectrode >= 100)
            {
                circleRadius = (int)(panel2.Width / 4.7);
                electrodeSize = (int)(circleRadius / 12.5);
            }
            else if (modelFileHeader.nElectrode >= 92)
            {
                circleRadius = 87;
                electrodeSize = (int)(circleRadius / 5.8);
            }
            else
            {
                circleRadius = 95;
                electrodeSize = (int)(circleRadius / 5.8);
            }

            int colorbarHeight = 104 * 2;
            panel2.BackgroundImage = null;

            leftTopX = (int)(panel2.Width * 0.646 + 0.5) - circleRadius - colorbarWidth;
            leftTopY = (int)(panel2.Height * 0.64 + 0.5 - 20) - circleRadius; //topoplot little higher for 332 model

            pictureBox1.Image = drawTopoPlot(panel2.Width, panel2.Height, currentIntensityNormalized, colormapJet, leftTopX, leftTopY, circleRadius, electrodeSize, (float)5.0, true);
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Visible = true;

            pictureBox2.Location = new Point(0, 0);
            pictureBox2.Size = new Size((int)(0 * 1.11), (int)(0));
            pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;

            pictureBox8.Location = new Point(0, 0);
            pictureBox8.Size = new Size((int)(200 * 1.11), (int)(200));
            // pictureBox8.BackgroundImageLayout = ImageLayout.Stretch;

            if (dataGridView1.RowCount == 0)
                //pictureBox2.BackgroundImage = null;
                pictureBox8.BackgroundImage = null;
            else
                //  pictureBox2.BackgroundImage = pictureBox2BackgroundImage1;
                pictureBox8.BackgroundImage = pictureBox2BackgroundImage1;
            pictureBox2.Image = null;
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.Visible = true;
            pictureBox4.Height = colorbarWidth + 60;
            pictureBox4.Width = 20 + colorbarHeight + 40;
            pictureBox4.Location = new Point(panel2.Width - pictureBox4.Width - 10, pictureBox2.Height / 6);

            pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, colormapJet, 10, 5, colorbarHeight, colorbarWidth, currentMax, currentMin, "Current Intensity (mA)", 33 + offset, "horizontal");
            pictureBox4.Visible = true;
            panel2.Invalidate();
        }

        bool IsNumber(string text)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(text);
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            toolTip1.RemoveAll();
            toolTip1.Hide(this);
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // Check the value of the e.ColumnIndex property if you want to apply this formatting only so some rcolumns.
                if (e.Value != null)
                {
                    e.Value = e.Value.ToString().ToUpper();
                    e.FormattingApplied = true;
                }
        }

        private void Form1_Move(object sender, System.EventArgs e)
        {
            toolTip1.Hide(this);
            toolTip1.RemoveAll();
        }

        private void getCurrentIntensity()
        {
            currentValueChanged = false;
            currentIntensity = new double[modelFileHeader.nElectrode];
            isElectrode = false;
            for (int i = 0; i < dataGridView1.RowCount; ++i)
            {
                if (dataGridView1.Rows[i].Cells[0].Value != null)
                {
                    string electrodeName = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    isElectrode = false;
                    isRepeated = false;
                    for (int j = 0; j < modelFileHeader.nElectrode; ++j)
                    {
                        if (electrodeName.ToUpper().Trim() == biosemiLoc.channelName[j].ToUpper().Trim())
                        {
                            isElectrode = true;
                            if (dataGridViewCellValueChangedRows != i && dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value != null)
                            {
                                if (electrodeName.ToUpper().Trim() == dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value.ToString().ToUpper().Trim())
                                {
                                    //catch repeated electrodes

                                    isRepeated = true;
                                    var cell = dataGridView1.CurrentCell;
                                    var cellDisplayRect = dataGridView1.GetCellDisplayRectangle(0, dataGridViewCellValueChangedRows, false);
                                    toolTip1.Show(string.Format("Repeated entry", 0, 0),
                                                  dataGridView1,
                                                  cellDisplayRect.X + cell.Size.Width / 2,
                                                  cellDisplayRect.Y + cell.Size.Height / 2,
                                                  2000);
                                    dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value = " ";

                                    if (radioButton_uniform.Checked)
                                    {
                                        dataGridView1.Rows.RemoveAt(dataGridViewCellValueChangedRows);
                                        dataGridView2.Rows.RemoveAt(dataGridViewCellValueChangedRows);
                                        dataGridViewCellValueChangedRows--;
                                    }

                                    if (radioButton_cathode.Checked) number_of_cathodes--;
                                    if (radioButton_anode.Checked) number_of_anodes--;

                                    for (int l = 0; l < number_of_anodes; l++)
                                    {

                                        dataGridView1.Rows[l].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                        dataGridView2.Rows[l].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                    }
                                    for (int l = number_of_anodes; l < number_of_anodes + number_of_cathodes; l++)
                                    {

                                        dataGridView1.Rows[l].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                        dataGridView2.Rows[l].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                    }

                                   // MessageBox.Show("Not a valid entry");

                                    break;
                                }
                            }

                            if (isRepeated)
                                break;

                            if (dataGridView1.Rows[i].Cells[1].Value != null)
                            {
                                if (IsNumber(dataGridView1.Rows[i].Cells[1].Value.ToString().Trim()))
                                {
                                    if (currentIntensity[j] != Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value))
                                    {
                                        currentValueChanged = true;
                                        currentIntensity[j] = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                                    }
                                }
                            }

                            if (currentIntensity[j] > currentMax)
                                currentMax = currentIntensity[j];
                            if (currentIntensity[j] < currentMin)
                                currentMin = currentIntensity[j];
                            currentMax = Math.Max(Math.Abs(currentMax), Math.Abs(currentMin));
                            currentMin = -currentMax;
                            if (currentMin == currentMax)
                            {
                                currentMin = -Convert.ToDouble(2.0);
                                currentMax = Convert.ToDouble(2.0);
                            }
                            break;
                        }
                    }

                    if (isRepeated)
                        return;

                    if (!isElectrode && dataGridView1.Rows[i].Cells[0].Value.ToString().Trim() != "")
                    {            


                        break;
                    }
                }

            }
            // normalize the current to [0 255]
            currentIntensityNormalized = new double[modelFileHeader.nElectrode];
            for (int i = 0; i < currentIntensity.Length; ++i)
                currentIntensityNormalized[i] = (currentIntensity[i] - currentMin) * (colormapLength - 1) / (currentMax - currentMin);
            this.Current.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        }

        private double[, ,] getField()
        {
            double[, ,] electricFieldVolumn = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
            for (int i = 0; i < modelFileHeader.y; ++i)
            {
                Application.DoEvents();
                for (int j = 0; j < modelFileHeader.x; ++j)
                    for (int k = 0; k < modelFileHeader.z; ++k)
                    {
                        for (int m = 0; m < modelFileHeader.r; ++m)
                            electricFieldVolumn[i, j, k] = electricFieldVolumn[i, j, k] + Math.Pow(electricFields[i, j, k, m], 2.0);
                        electricFieldVolumn[i, j, k] = Math.Sqrt(electricFieldVolumn[i, j, k]) * brainMask[i, j, k];
                        if (electricFieldVolumn[i, j, k] > maxField)
                        {
                            maxField = electricFieldVolumn[i, j, k];
                            maxX = j;
                            maxY = i;
                            maxZ = k;
                        }
                    }
            }
            return electricFieldVolumn;
        }

        private void calculateElectricFields()
        {
            fieldScale = 0.0;
            // load the mixing matrixes and calculate the electric fields
            string brainFileName = modelDataPath + "nan_mask_brain";
            if (File.Exists(@brainFileName))
            {
                using (BinaryReader binReader = new BinaryReader(File.Open(@brainFileName, FileMode.Open)))
                {
                    modelFileHeader.version = Convert.ToSingle(binReader.ReadSingle());
                    modelFileHeader.x = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.y = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.z = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.r = 3;
                    if (domainUpDown1.Items.Count == 0)
                    {
                        domainUpDown1.Items.Clear();
                        for (int i = modelFileHeader.x; i > 0; --i)
                            domainUpDown1.Items.Add(i);
                        domainUpDown2.Items.Clear();
                        for (int i = modelFileHeader.y; i > 0; --i)
                            domainUpDown2.Items.Add(i);
                        domainUpDown3.Items.Clear();
                        for (int i = modelFileHeader.z; i > 0; --i)
                            domainUpDown3.Items.Add(i);

                        position[0] = (int)(modelFileHeader.x / 2);
                        position[1] = (int)(modelFileHeader.y / 2);
                        position[2] = (int)(modelFileHeader.z / 2);

                        domainUpDown1.SelectedItem = Convert.ToString(position[0] + 1);
                        domainUpDown1.SelectedIndex = position[0];
                        domainUpDown2.SelectedItem = Convert.ToString(position[1] + 1);
                        domainUpDown2.SelectedIndex = position[1];
                        domainUpDown3.SelectedItem = Convert.ToString(position[2] + 1);
                        domainUpDown3.SelectedIndex = position[2];

                        domainUpDown1.Text = Convert.ToString(position[0] + 1);
                        domainUpDown2.Text = Convert.ToString(position[1] + 1);
                        domainUpDown3.Text = Convert.ToString(position[2] + 1);
                    }
                    brainMask = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
                    for (int j = modelFileHeader.z - 1; j >= 0; --j)
                    {
                        Application.DoEvents();
                        for (int k = 0; k < modelFileHeader.y; ++k)

                            //read out maroms head solution files
                            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")))
                            {
                                for (int m = modelFileHeader.x - 1; m >= 0; --m)
                                {
                                    int thisByte = Convert.ToInt16(binReader.ReadByte());
                                    if (thisByte == 0)
                                        brainMask[k, m, j] = Double.NaN;
                                    else
                                    {
                                        brainMask[k, m, j] = Convert.ToDouble(thisByte);
                                    }
                                }
                            }
                            //standard head
                            else
                            {
                                for (int m = 0; m < modelFileHeader.x; ++m) // changed to make standard head work
                                {
                                    int thisByte = Convert.ToInt16(binReader.ReadByte());
                                    if (thisByte == 0)
                                        brainMask[k, m, j] = Double.NaN;
                                    else
                                    {
                                        brainMask[k, m, j] = Convert.ToDouble(thisByte);
                                    }
                                }
                            }
                    }
                }
            }
            else
            {
                MessageBox.Show(brainFileName + " does not exist.");
                return;
            }

            // load the MRI
            string mriFileName = modelDataPath + "mri";
            if (File.Exists(@mriFileName))
            {
                using (BinaryReader binReader = new BinaryReader(File.Open(@mriFileName, FileMode.Open)))
                {
                    binReader.ReadSingle(); //version
                    double scalingFactor = Convert.ToDouble(binReader.ReadSingle());
                    binReader.ReadUInt16(); //x
                    binReader.ReadUInt16(); //y
                    binReader.ReadUInt16(); //z

                    mri = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
                    mriMax = 0.0;
                    for (int j = modelFileHeader.z - 1; j >= 0; --j)
                    {
                        Application.DoEvents();
                        for (int k = 0; k < modelFileHeader.y; ++k)
                            //read out maroms head solution files
                            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")))
                            {
                                for (int m = modelFileHeader.x - 1; m >= 0; --m)
                                {
                                    mri[k, m, j] = Convert.ToDouble(binReader.ReadByte()) / scalingFactor;
                                    if (mri[k, m, j] > mriMax)
                                        mriMax = mri[k, m, j];
                                }
                            }
                            else//standard head
                            {
                                for (int m = 0; m < modelFileHeader.x; ++m) // changed to make standard head work
                                {
                                    mri[k, m, j] = Convert.ToDouble(binReader.ReadByte()) / scalingFactor;
                                    if (mri[k, m, j] > mriMax)
                                        mriMax = mri[k, m, j];
                                }
                            }
                    }
                }
            }
            else
            {
                MessageBox.Show(toolStripComboBox1.Text + " MRI " + " does not exist.");
                return;
            }

            transeverse = new double[modelFileHeader.y, modelFileHeader.x];
            sagittal = new double[modelFileHeader.z, modelFileHeader.y];
            coronal = new double[modelFileHeader.z, modelFileHeader.x];

            electricFields = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z, modelFileHeader.r];
            electricField3D = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
            maxX = 0;
            maxY = 0;
            maxZ = 0;
            maxField = 0.0;
            int nonZeroCurrentCount = 0;
            for (int n = 0; n < currentIntensity.Length - 1; ++n) //-1: The last value is always to be ignored! Indeed, there is no corresponding volume.
            {
                if (currentIntensity[n] != 0.0)
                    nonZeroCurrentCount++;
            }

            box.setProgressBarMaximum(nonZeroCurrentCount * modelFileHeader.r * modelFileHeader.z);

            int dataCount = 0;
            int count = 0;
            bool firstCurrent = true;

       

            for (int n = 0; n < currentIntensity.Length - 1; ++n) //-1: The last value is always to be ignored! Indeed, there is no corresponding volume.
            {

                if (currentIntensity[n] != 0.0)
                {
                    count++;
                    string fileName = modelDataPath + "A_all_" + (n + 1).ToString();

                    try
                    {
                        maxField3D = 0.0;
                        maxField = 0.0;
                        using (BinaryReader binReader = new BinaryReader(File.Open(fileName, FileMode.Open)))
                        {

                            //read header
                            binReader.ReadSingle(); //version
                            double scalingFactor = Convert.ToDouble(binReader.ReadSingle());
                            double minA = Convert.ToDouble(binReader.ReadSingle());
                            binReader.ReadUInt16(); //x 
                            binReader.ReadUInt16(); //y
                            binReader.ReadUInt16(); //z
                            binReader.ReadUInt16(); //3
                            for (int i = 0; i < modelFileHeader.r; ++i)
                            {

                                for (int j = modelFileHeader.z - 1; j >= 0; --j)
                                {
                                    dataCount++;
                                    box.updateProgressBar(dataCount);
                                    Application.DoEvents();
                                    for (int k = 0; k < modelFileHeader.y; ++k)
                                    {
                                        //read out maroms head solution files
                                        if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")| toolStripComboBox1.Text.Equals("Stroke_patient_KS"))) // Testing Stroke Patient KS Added "| toolStripComboBox1.Text.Equals("Stroke_patient_KS")" to flip solution for KS, CNT 9/4/15
                                        {

                                            for (int m = modelFileHeader.x - 1; m >= 0; --m)
                                            {
                                                byte thisByte = binReader.ReadByte();
                                                if (thisByte == 255)
                                                    electricFields[k, m, j, i] = Double.NaN;
                                                else
                                                    electricFields[k, m, j, i] = electricFields[k, m, j, i] + (Convert.ToDouble(thisByte) / scalingFactor + minA) * (currentIntensity[n]);// / 0.1131;

                                                if (Math.Abs(electricFields[k, m, j, i]) > maxField3D)
                                                    maxField3D = Math.Abs(electricFields[k, m, j, i]);
                                            }
                                        }
                                        else//standard head
                                        {
                                            for (int m = 0; m < modelFileHeader.x; ++m) // changed to make standard head work
                                            {
                                                byte thisByte = binReader.ReadByte();
                                                if (thisByte == 255)
                                                    electricFields[k, m, j, i] = Double.NaN;
                                                else
                                                    electricFields[k, m, j, i] = electricFields[k, m, j, i] + (Convert.ToDouble(thisByte) / scalingFactor + minA) * (currentIntensity[n]);// / 0.1131;

                                                if (Math.Abs(electricFields[k, m, j, i]) > maxField3D)
                                                    maxField3D = Math.Abs(electricFields[k, m, j, i]);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        electricField3D = getField();

                        electricFieldColormapMax = Math.Ceiling(2 * maxField / 3 / .05) * .05; //round up to nearest .05
                        electricFieldColormapMin = 0.0;

                        if (count == nonZeroCurrentCount)
                        {
                            domainUpDown4.Items.Clear();
                            domainUpDown5.Items.Clear();

                            for (double i = Math.Floor(maxField / .01) * .01; i >= 0; i = i - 0.01)
                            {
                                domainUpDown4.Items.Add(Math.Round(i, 2));
                                domainUpDown5.Items.Add(Math.Round(i, 2));
                            }
                            domainUpDown4.Items.Add(0);
                            domainUpDown5.Items.Add(0);
                            domainUpDown4.SelectedIndex = Convert.ToInt16(1 * domainUpDown4.Items.Count / 3);
                            domainUpDown5.SelectedIndex = domainUpDown5.Items.Count - 1;
                            electricFieldColormapMax = Convert.ToDouble(domainUpDown4.Items[domainUpDown4.SelectedIndex]);
                            electricFieldColormapMin = Convert.ToDouble(domainUpDown5.Items[domainUpDown5.SelectedIndex]);

                            double[] fieldArray = new double[modelFileHeader.y * modelFileHeader.x * modelFileHeader.z * modelFileHeader.r];
                            double xy, yz, xz;
                            long arrayCount = 0;
                            for (int i = 0; i < electricFields.GetLength(0); ++i)
                                for (int j = 0; j < electricFields.GetLength(1); ++j)
                                    for (int k = 0; k < electricFields.GetLength(2); ++k)
                                        if (!double.IsNaN(electricFields[i, j, k, 0]) && !double.IsNaN(electricFields[i, j, k, 1]) && !double.IsNaN(electricFields[i, j, k, 2]))
                                        {
                                            xy = Math.Sqrt(Math.Pow(electricFields[i, j, k, 0], 2.0) + Math.Pow(electricFields[i, j, k, 1], 2.0));
                                            fieldArray[arrayCount] = xy;
                                            arrayCount++;
                                            yz = Math.Sqrt(Math.Pow(electricFields[i, j, k, 1], 2.0) + Math.Pow(electricFields[i, j, k, 2], 2.0));
                                            fieldArray[arrayCount] = yz;
                                            arrayCount++;
                                            xz = Math.Sqrt(Math.Pow(electricFields[i, j, k, 0], 2.0) + Math.Pow(electricFields[i, j, k, 2], 2.0));
                                            fieldArray[arrayCount] = xz;
                                            arrayCount++;
                                        }
                            double[] fieldArray2 = new double[arrayCount];
                            Array.Copy(fieldArray, fieldArray2, arrayCount);
                            double percentileTmp = percentile(fieldArray2, percentileRank / 100);
                            fieldScale = Convert.ToDouble(fieldOrientationStep) / percentileTmp;
                        }

                        if (firstCurrent)
                        {
                            // setup pictureBox1
                            pictureBox1.Visible = false;
                            pictureBox1.Dock = DockStyle.None;
                            pictureBox4.Visible = false;
                            pictureBox2.Visible = false;
                            pictureBox2.Dock = DockStyle.None;
                            panel2.Location = new Point(panel1.Width + panel1.Location.X, 0);
                            pictureBox1.Height = modelFileHeader.z;
                            pictureBox1.Width = modelFileHeader.x;
                            pictureBox1.Location = new Point(24, 75);
                            pictureBox1.BackColor = Color.Black;
                            label8.Location = new Point(pictureBox1.Location.X + pictureBox1.Width / 2 + 30, labelFieldIntensity.Location.Y);
                            label11.Location = new Point(label8.Location.X, label8.Location.Y + label8.Height);
                            label5.Location = new Point(pictureBox1.Location.X, label8.Location.Y - label5.Height - 8);
                            // setup pictureBox2
                            pictureBox2.Location = new Point(pictureBox1.Location.X + pictureBox1.Width + 96, pictureBox1.Location.Y);
                            pictureBox2.Height = modelFileHeader.z; ;
                            pictureBox2.Width = modelFileHeader.x;
                            pictureBox2.Size = new Size(modelFileHeader.y, modelFileHeader.z);
                            pictureBox2.BackColor = Color.Black;
                            label6.Location = new Point(pictureBox2.Location.X, label5.Location.Y);
                            label9.Location = new Point(pictureBox2.Location.X, label8.Location.Y);
                            // setup pictureBox3
                            label7.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + pictureBox1.Height + 20);
                            label10.Location = new Point(pictureBox1.Location.X, label7.Location.Y + label7.Height + 8);


                            pictureBox3.Location = new Point(pictureBox1.Location.X, label10.Location.Y + label10.Height + 8);
                            pictureBox3.Height = modelFileHeader.y;
                            pictureBox3.Width = modelFileHeader.x;
                            pictureBox3.BackColor = Color.Black;
                            pictureBox6.Visible = false;

                            label13.Location = new Point(label5.Location.X, pictureBox1.Location.Y - label13.Height - 5);
                            label14.Location = new Point(label6.Location.X, pictureBox2.Location.Y - label14.Height - 5);
                            label15.Location = new Point(label7.Location.X, pictureBox3.Location.Y - label15.Height - 5);
                            label19.Location = new Point(label5.Location.X - label19.Width, pictureBox1.Height/2 +55);
                            label20.Location = new Point(label5.Location.X + pictureBox1.Width + 2, pictureBox1.Height / 2 + 55);
                            label21.Location = new Point(label6.Location.X - label21.Width, pictureBox2.Height / 2 + 55);
                            label22.Location = new Point(label6.Location.X + pictureBox2.Width + 2, pictureBox2.Height / 2 + 55);
                            label23.Location = new Point(label7.Location.X - label23.Width, pictureBox3.Height / 2 + pictureBox3.Location.Y-15);
                            label24.Location = new Point(label7.Location.X + pictureBox3.Width + 2, pictureBox3.Height / 2 + pictureBox3.Location.Y - 15);
                            label25.Location = new Point(label19.Location.X + pictureBox1.Width/ 2, 55);
                            label26.Location = new Point(label19.Location.X + pictureBox1.Width/2, pictureBox1.Height + 55);
                            label27.Location = new Point(label21.Location.X + pictureBox2.Width / 2, 55);
                            label28.Location = new Point(label21.Location.X + pictureBox2.Width / 2, pictureBox2.Height + 55);
                            label29.Location = new Point(label23.Location.X + pictureBox3.Width / 2,label15.Location.Y);
                            label30.Location = new Point(label23.Location.X + pictureBox3.Width / 2, label29.Location.Y + pictureBox3.Height);

                            // setup pictureBox4
                            int topMaxHeight = Math.Max(pictureBox1.Height, pictureBox2.Height);

                            int colorbarX = pictureBox2.Location.X + pictureBox2.Width + 58;
                            int colorbarY = pictureBox2.Location.Y + topMaxHeight;
                            pictureBox4.Location = new Point(colorbarX, pictureBox2.Location.Y - 10);
                            pictureBox4.Width = colorbarWidth + 60;
                            pictureBox4.Height = 10 + topMaxHeight + 20;
                            labelFieldIntensity.Location = new Point(pictureBox2.Location.X + pictureBox2.Width, label8.Location.Y);

                            // setup pictureBox5
                            groupBox1.Location = new Point(pictureBox3.Location.X + pictureBox3.Width + 46, pictureBox2.Location.Y + pictureBox2.Height + 24 - 5);
                            pictureBox5.Location = new Point(0, 1);

                            pictureBox5.Size = new Size(pictureBox2.Width + 113, pictureBox3.Height + 103);

                            // setup pictureBox6
                            pictureBox6.Location = new Point(pictureBox4.Location.X - groupBox1.Location.X + 1, pictureBox5.Location.Y);

                            pictureBox6.Width = pictureBox4.Width;
                            pictureBox6.Height = pictureBox5.Size.Height;
                            groupBox1.Size = new Size(pictureBox5.Width + pictureBox6.Width + 5, panel1.Height - groupBox1.Location.Y - 20);
                            groupBox1.Visible = true;
                            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);

                            if (modelFileHeader.nElectrode >= 100)
                            {
                                circleRadius = (int)(pictureBox3.Height / 2.1);
                                electrodeSize = 7;
                            }
                            else
                            {
                                circleRadius = (int)(pictureBox3.Height / 2.3);
                                electrodeSize = (int)(circleRadius / 5.3);
                            }
                            int colorbarHeight = pictureBox2.Height + 5;
                            if (colorbarHeight % 8 <= 5)
                                colorbarHeight = colorbarHeight - colorbarHeight % 8;
                            else
                                colorbarHeight = colorbarHeight + colorbarHeight % 8;
                            pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, colormapJet, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical");

                            if (modelFileHeader.nElectrode >= 100)
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius, electrodeSize, (float)3.4, false);
                            if (toolStripComboBox1.Text.Equals("MNI_152_93_electrode_model"))
                            {
                                pictureBox5.Location = new Point(15, 16);
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius - 15, electrodeSize - 3, (float)3.4, false);
                            }
                            else if (toolStripComboBox1.Text.Equals("Adult_male_2_93_electrode_model")) //Added adult male 2 since it looked off for it looked off, added by CNT 06/01/2015
                            {
                                pictureBox5.Location = new Point(30, 16);
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius - 40, electrodeSize - 7, (float)4.8, false);
                                circleRadius = circleRadius - 20; // to correct it for the current intensity bar, added by CNT 06/01/2015
                            }
                            else if (toolStripComboBox1.Text.Equals("Adult_male_3_93_electrode_model")) //added adult male 3 since it looked off for it looked off, added by cnt 06/03/2015
                            {
                                pictureBox5.Location = new Point(30, 10);
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius - 15, electrodeSize - 3, (float)4.8, false);
                                circleRadius = circleRadius - 15; // to correct it for the current intensity bar, added by CNT 06/03/2015

                            }
                            else if (toolStripComboBox1.Text.Equals("Stroke_patient_KS")) //added Stroke Patient KS since it looked off for it looked off, added by CNT 
                            {
                                pictureBox5.Location = new Point(35, 15);
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius - 20, electrodeSize - 4, (float)4.8, false);

                            }
                            else
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius, electrodeSize, (float)4.8, false);
                            colorbarHeight = 2 * circleRadius + 10;
                            if (colorbarHeight % 8 <= 5)
                                colorbarHeight = colorbarHeight - colorbarHeight % 8;
                            else
                                colorbarHeight = colorbarHeight + 8 - colorbarHeight % 8;
                            pictureBox6.Image = drawColorBar(pictureBox6.Width, pictureBox6.Height, colormapJet, 5, 256 / 4 + 2 * circleRadius + 10, colorbarWidth, colorbarHeight, currentMax, currentMin, "Current Intensity (mA)", circleRadius - 5 + offset, "vertical");

                            pictureBox5.Size = new Size(groupBox1.Width - pictureBox6.Width, pictureBox3.Height + 103);

                            pictureBox2.BackgroundImage = null;
                            pictureBox8.BackgroundImage = null;

                            pictureBox1.Visible = true;
                            pictureBox2.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            pictureBox5.Visible = true;
                            pictureBox6.Visible = true;
                            label8.Visible = true;

                            //disable MNI locations display for non-Marom heads (as long as the other transformation matrices are missing
                            //if (toolStripComboBox1.Text != "Adult_male_1_93_electrode_model" & toolStripComboBox1.Text != "Adult_male_1_332_electrode_model")// & toolStripComboBox1.Text != "MNI_152_93_electrode_model")
                            //    //added by AAA 01.07.2015
                            //{
                                
                            //    label11.Visible = false;
                            //}
                            //else label11.Visible = true;
                            label11.Visible = true;
                            
                            label13.Visible = true;
                            label14.Visible = true;
                            label15.Visible = true;
                            labelFieldIntensity.Visible = true;
                            label19.Visible = true;
                            label20.Visible = true;
                            label21.Visible = true;
                            label22.Visible = true;
                            label23.Visible = true;
                            label24.Visible = true;
                            //label25.Visible = true;
                            //label26.Visible = true;
                            //label27.Visible = true;
                            //label28.Visible = true;
                            //label29.Visible = true;
                            //label30.Visible = true;

                            this.Cursor = Cursors.Default;
                            this.AutoSize = true;
                            groupBox1.Invalidate();
                            firstCurrent = false;
                        }
                        else
                            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);

                        panel2.Invalidate();
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show("The solution file '" + fileName + "' for electrode '" + biosemiLoc.channelName[n] + "'" + " does not exist.");
                        getCurrentIntensity();
                        colorbarWidth = 30;
                        showTopplot();
                        selectElectrode = true;
                        pictureBox1.Enabled = true;
                        panel2.Invalidate();
                        selectElectrode = true;
                        showField = false;
                        showMRI = false;
                        showOverlay = false;
                        label5.Visible = false;
                        label6.Visible = false;
                        label7.Visible = false;
                        label8.Visible = false;
                        label9.Visible = false;
                        label10.Visible = false;
                        labelFieldIntensity.Visible = false;
                        this.Cursor = Cursors.Arrow;
                        tabControl1.SelectTab(tabPage2);

                        return;
                    }
                }
            }

            pictureBox1.Enabled = true;
            pictureBox2.Enabled = true;
            pictureBox3.Enabled = true;
            printToolStripMenuItem.Enabled = true;
            buttonOverlay.Visible = true;
            buttonOverlay.Enabled = true;

            //// curent max
            //domainUpDown4.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 40, pictureBox2.Location.Y);
            //// curent min
            //domainUpDown5.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 40, pictureBox2.Location.Y + pictureBox2.Height - domainUpDown4.Height);
      
            // x 
            domainUpDown1.Location = new Point(label14.Location.X + label14.Width, pictureBox2.Location.Y - domainUpDown1.Height - 1);
            // y
            domainUpDown2.Location = new Point(label13.Location.X + label13.Width, pictureBox1.Location.Y - domainUpDown2.Height - 1);
            // z
            domainUpDown3.Location = new Point(label15.Location.X + label15.Width, pictureBox3.Location.Y - domainUpDown3.Height - 1);

            domainUpDown1.Visible = true;
            domainUpDown2.Visible = true;
            domainUpDown3.Visible = true;
            domainUpDown4.Visible = true;
            domainUpDown5.Visible = true;

            buttonMRI.Visible = true;
            buttonMRI.Enabled = true;

            buttonField.Visible = true;
            buttonField.Enabled = true;

            saveAll3ToolStripMenuItem.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // visualize the current distribution
            panel1.Focus();
            selectElectrode = false;
            showField = true;
            string brainFileName = modelDataPath + "nan_mask_brain";
            if (File.Exists(@brainFileName))
            {
                this.Enabled = false;
                showField = true;
                showMRI = false;
                showOverlay = false;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    box = new LoadData(modelFileHeader.nSource);

                    box.Show();
                    box.Cursor = Cursors.WaitCursor;

                    maxField = 0.0;
                    try
                    {
                        calculateElectricFields();
                        button1.Enabled = false;
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.ToString());
                        box.Close();
                        this.Cursor = Cursors.Arrow;
                        return;
                    }
                    this.Enabled = true;
                    box.Close();
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                    return;
                }
            }
            else
            {
                MessageBox.Show(brainFileName + " does not exist.");
                return;
            }
            printToolStripMenuItem.Enabled = true;
            toolTipVisualizationGUI.Active = false;
            toolTipVisualizationGUI.Active = true;
        }

        protected Bitmap drawColorBar(int pictureWidth, int pictureHeight, int[,] cmap, int x, int y, int width, int height, double max, double min, string label, int coordinate, string direction)
        {
            //draw the color bar
            Bitmap bmp = new Bitmap(pictureWidth, pictureHeight);
            Graphics g = Graphics.FromImage(bmp);
            Font aFont = new Font(button1.Font.Name, button1.Font.Size, button1.Font.Style, button1.Font.Unit);
            Pen p = new Pen(Color.White);

            if (direction == "vertical")
            {
                int ymin = 0;
                int ymax = (int)(height / 2);
                int dy = height / (ymax - ymin);
                int m = cmap.GetLength(0);

                for (int i = 0; i < ymax; i++)
                {
                    int colorIndex = (int)((i - ymin) * m / (ymax - ymin));
                    SolidBrush aBrush = new SolidBrush(Color.FromArgb(
                        cmap[colorIndex, 0], cmap[colorIndex, 1],
                        cmap[colorIndex, 2], cmap[colorIndex, 3]));
                    g.FillRectangle(aBrush, x, y - i * dy - dy, width, dy);
                    if (i % (height / 8) == 0)
                    {
                        g.DrawLine(p, x + (width - 1) - 3, y - i * dy, x + (width - 1), y - i * dy);
                        g.DrawLine(p, x, y - i * dy, x + 3, y - i * dy);
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(Convert.ToString(Math.Round((double)colorIndex / m * (max - min) + min, 2)), aFont, Brushes.Black, x + width + 2, y - i * dy - 6);
                    }
                }
                g.DrawLine(p, x + (width - 1) - 3, y - height - 1, x + (width - 1), y - height - 1);
                g.DrawLine(p, x, y - height - 1, x + 3, y - height - 1);

                g.DrawString(Convert.ToString(max), aFont, Brushes.Black, x + width + 2, y - (ymax - 1) * dy - 6);

                aFont = new Font("Microsoft Sans Serif", 9.8F);
                StringFormat sf = new StringFormat(StringFormatFlags.DirectionVertical);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString(label, aFont, Brushes.Black, x + width + 2 + 33, coordinate, sf);
            }
            else if (direction == "horizontal")
            {
                int xmin = 0;
                int xmax = (int)(width / 2);
                int dx = width / (xmax - xmin);
                int m = cmap.GetLength(0);

                for (int i = 0; i < xmax; i++)
                {
                    int colorIndex = (int)((i - xmin) * m / (xmax - xmin));
                    SolidBrush aBrush = new SolidBrush(Color.FromArgb(
                        cmap[colorIndex, 0], cmap[colorIndex, 1],
                        cmap[colorIndex, 2], cmap[colorIndex, 3]));
                    g.FillRectangle(aBrush, x + i * dx + dx, y, dx, height);
                    if (i % (width / 8) == 0)
                    {
                        g.DrawLine(p, x + i * dx, y + (height - 1) - 3, x + i * dx, y + (height - 1));
                        g.DrawLine(p, x + i * dx, y, x + i * dx, y + 3);
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(Convert.ToString(Math.Round((double)colorIndex / m * (max - min) + min, 2)), aFont, Brushes.Black, x + i * dx - dx - 3, y + height + 2);
                    }
                }
                g.DrawLine(p, x - width - 1, y + (height - 1) - 3, x - width - 1, y + (height - 1));
                g.DrawLine(p, x - width - 1, y, x - width - 1, y + 3);

                g.DrawString(Convert.ToString(max), aFont, Brushes.Black, x + (xmax - 1) * dx - 4, y + height + 2);

                aFont = new Font("Microsoft Sans Serif", 9.8F);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString(label, aFont, Brushes.Black, coordinate, y + height + 23);
            }
            g.Dispose();
            p.Dispose();
            return bmp;
        }

        private void readElectrodeLoc(string locFileNames)
        {
            using (BinaryReader binReader = new BinaryReader(File.Open(@locFileNames, FileMode.Open)))
            {
                modelFileHeader.nElectrode = Convert.ToInt32(binReader.ReadSingle());
                biosemiLoc.channelNumber = new int[modelFileHeader.nElectrode];
                biosemiLoc.channelName = new string[modelFileHeader.nElectrode];
                biosemiLoc.degrees = new double[modelFileHeader.nElectrode];
                biosemiLoc.arcLength = new double[modelFileHeader.nElectrode];
                string channelName;
                int nChart = Convert.ToByte(binReader.ReadSingle());
                for (int i = 0; i < modelFileHeader.nElectrode; ++i)
                {
                    biosemiLoc.channelNumber[i] = Convert.ToInt32(binReader.ReadSingle());
                    biosemiLoc.degrees[i] = (Convert.ToDouble(binReader.ReadSingle()) - 90.0) * Math.PI / 180;
                    biosemiLoc.arcLength[i] = Convert.ToDouble(binReader.ReadSingle());
                    channelName = new string(binReader.ReadChars(nChart));
                    biosemiLoc.channelName[i] = channelName.Replace('.', ' ');
                    biosemiLoc.channelName[i] = biosemiLoc.channelName[i].Trim();
                    if (i == modelFileHeader.nElectrode - 1)
                        reference = biosemiLoc.channelName[i];
                }
            }
        }

        protected Bitmap drawTopoPlot(int pictureWidth, int pictureHeight, double[] currentIntensity, int[,] cmap, int leftTopX, int leftTopY, int circleRadius, int electrodeSize, float fontSize, bool showLabel)
        {
            // draw topplot
            Bitmap bmp = new Bitmap(pictureWidth, pictureHeight);
            Graphics g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Pen p = new Pen(Color.Gray);
            p.Width = 2.0F;
            int lineLength = 15;
            //nose
            g.DrawLine(p, leftTopX + circleRadius - lineLength, leftTopY, leftTopX + circleRadius, leftTopY - lineLength);
            g.DrawLine(p, leftTopX + circleRadius + lineLength, leftTopY, leftTopX + circleRadius, leftTopY - lineLength);
            ////left ear
            //g.DrawLine(p, leftTopX, leftTopY + circleRadius - 2 * lineLength, leftTopX - lineLength, leftTopY + circleRadius - lineLength);
            //g.DrawLine(p, leftTopX - lineLength, leftTopY + circleRadius - lineLength, leftTopX, leftTopY + circleRadius);
            //g.DrawLine(p, leftTopX, leftTopY + circleRadius, leftTopX - lineLength, leftTopY + circleRadius + lineLength);
            //g.DrawLine(p, leftTopX - lineLength, leftTopY + circleRadius + lineLength, leftTopX, leftTopY + circleRadius + 2 * lineLength);
            ////right ear
            //g.DrawLine(p, leftTopX + circleRadius * 2, leftTopY + circleRadius - 2 * lineLength, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius - lineLength);
            //g.DrawLine(p, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius - lineLength, leftTopX + circleRadius * 2, leftTopY + circleRadius);
            //g.DrawLine(p, leftTopX + circleRadius * 2, leftTopY + circleRadius, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius + lineLength);
            //g.DrawLine(p, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius + lineLength, leftTopX + circleRadius * 2, leftTopY + circleRadius + 2 * lineLength);

            // Describes the brush's color using RGB values. 
            // Each value has a range of 0-255.
            Color topplot = Color.FromArgb(alpha, cmap[(int)(colormapLength / 2), 3], cmap[(int)(colormapLength / 2), 2], cmap[(int)(colormapLength / 2), 1]);
            SolidBrush mySolidBrush = new SolidBrush(topplot);
            g.DrawEllipse(p, leftTopX, leftTopY, circleRadius * 2, circleRadius * 2);

            Font aFont = new Font(button1.Font.Name, fontSize, button1.Font.Style, button1.Font.Unit);

            for (int i = 0; i < currentIntensity.Length; ++i)
            {
                topplot = Color.FromArgb(alpha, cmap[(int)(currentIntensity[i] + 0.5), 1], cmap[(int)(currentIntensity[i] + 0.5), 2], cmap[(int)(currentIntensity[i] + 0.5), 3]);
                mySolidBrush = new SolidBrush(topplot);
                g.FillEllipse(mySolidBrush, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 2), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 2), electrodeSize, electrodeSize);
                electrodeCenterCoorginates[i, 0] = (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - 2);
                electrodeCenterCoorginates[i, 1] = (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - 2);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                if (showLabel)
                {
                    if (modelFileHeader.nElectrode >= 100)
                    {
                        if (biosemiLoc.channelName[i].Length == 4)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.1), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 3)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.47), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 2)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.8), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                    }
                    else
                    {
                        if (biosemiLoc.channelName[i].Length == 4)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.7), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 3)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 2.3), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 2)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 3.2), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                    }
                }
            }

            g.Dispose();
            p.Dispose();
            mySolidBrush.Dispose();

            return bmp;
        }

        private Point initialMousePos;

        private void showImage(double[, ,] image, int[,] map, double colormapMin, double colormapMax)
        {
            label8.Text = "Selected Slice Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";

            //targ positions subtracted by 1 again for MNI conversion - AAA 12.21.2014
            double[] targ = new double[4] { modelFileHeader.x - position[0] - 2, position[1] + 1 , position[2] + 1 , 1 };
            //-1 added by AAA 01.07.2015
            double[] MNI = getMNIlocation(targ);

            //change MNI coords signs for Maroms resp. Standard head
            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")))
            {
                label11.Text = "Selected MNI Position: {" + Math.Round(-MNI[0]).ToString() + "," + Math.Round(MNI[1]).ToString() + "," + Math.Round(MNI[2]).ToString() + "}";
            }
            else label11.Text = "Selected MNI Position: {" + Math.Round(MNI[0]).ToString() + "," + Math.Round(-MNI[1]).ToString() + "," + Math.Round(-MNI[2]).ToString() + "}";


            if (Double.IsNaN(image[position[1], position[0], position[2]]))
                labelFieldIntensity.Text = "Selected Field Intensity: " + image[position[1], position[0], position[2]].ToString();
            else
                labelFieldIntensity.Text = "Selected Field Intensity: " + Math.Round(image[position[1], position[0], position[2]], 2).ToString() + " V/m";

            // update contents shown in pictureBox1
            for (int j = 0; j < modelFileHeader.x; ++j)
                for (int k = 0; k < modelFileHeader.y; ++k)
                {
                    transeverse[k, j] = (image[k, j, position[2]] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (transeverse[k, j] > (colormapLength - 1))
                        transeverse[k, j] = (colormapLength - 1);
                    if (transeverse[k, j] < 0)
                        transeverse[k, j] = 0;
                }
            Bitmap bmp = getBitmap(transeverse, modelFileHeader.y, modelFileHeader.x, map);

            Graphics gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            Pen p = new Pen(Color.White);
            p.Width = 2.0F;

            Pen pField = new Pen(Color.Black);
            pField.Width = 1.0F;
            pField.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            if (!showMRI)
            {
                for (int j = 0; j < modelFileHeader.x; j = j + fieldOrientationStep)
                    for (int k = 0; k < modelFileHeader.y; k = k + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[k, j, position[2]]))
                        {
                            PointF point1 = new PointF((float)j, (float)k);
                            PointF point2 = new PointF((float)j, (float)k);

                            //draw arrows for maroms head
                            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")| toolStripComboBox1.Text.Equals("Stroke_patient_KS")))//Testing Stroke Patient KS CNT 9/4/15
                            {
                                point2 = new PointF(-(float)(electricFields[k, j, position[2], 0] * fieldScale) + (float)j, (float)(electricFields[k, j, position[2], 1] * fieldScale) + (float)k);
                            }
                            else//standard head
                            {
                                point2 = new PointF((float)(electricFields[k, j, position[2], 0] * fieldScale) + (float)j, -(float)(electricFields[k, j, position[2], 1] * fieldScale) + (float)k);// changed to make standard head work
                            }
                            gr.DrawLine(pField, point1, point2);
                        }
                labelFieldIntensity.Visible = true;
            }
            gr.DrawEllipse(p, position[0] - 5, position[1] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox3.BackgroundImage = bmp;
            else
                pictureBox3.Image = bmp;
            pictureBox3.Invalidate();


            //added 10.17.2014 by AAA
            //Rotated the images for MNI 152 usage
            //if (toolStripComboBox1.Text.Equals("MNI_152_93_electrode_model"))
            //    pictureBox3.Image.RotateFlip(RotateFlipType.Rotate180FlipNone);


            // update contents shown in pictureBox2
            for (int i = 0; i < modelFileHeader.z; ++i)
                for (int j = 0; j < modelFileHeader.y; ++j)
                {
                    sagittal[i, j] = (image[j, position[0], i] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (sagittal[i, j] > (colormapLength - 1))
                        sagittal[i, j] = (colormapLength - 1);
                    if (sagittal[i, j] < 0)
                        sagittal[i, j] = 0;

                }
            bmp = getBitmap(sagittal, modelFileHeader.z, modelFileHeader.y, map);
            gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            if (!showMRI)
            {
                for (int i = 0; i < modelFileHeader.z; i = i + fieldOrientationStep)
                    for (int j = 0; j < modelFileHeader.y; j = j + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[j, position[0], i]))
                        {
                            PointF point1 = new PointF((float)j, (float)i);
                            PointF point2 = new PointF((float)j, (float)i);

                            //draw arrows for maroms head
                            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")| toolStripComboBox1.Text.Equals("Stroke_patient_KS")))//Testing Stroke Patient KS CNT 9/4/15
                            {
                                point2 = new PointF((float)(electricFields[j, position[0], i, 1] * fieldScale) + (float)j, -(float)(electricFields[j, position[0], i, 2] * fieldScale) + (float)i);
                            }
                            else//standard head
                            {
                                point2 = new PointF(-(float)(electricFields[j, position[0], i, 1] * fieldScale) + (float)j, -(float)(electricFields[j, position[0], i, 2] * fieldScale) + (float)i);// changed to make standard head work
                            }
                                    gr.DrawLine(pField, point1, point2);
                        }
                labelFieldIntensity.Visible = true;
            }
            gr.DrawEllipse(p, position[1] - 5, position[2] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox2.BackgroundImage = bmp;
            else
                pictureBox2.Image = bmp;
            pictureBox2.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < modelFileHeader.z; ++i)
                for (int k = 0; k < modelFileHeader.x; ++k)
                {
                    coronal[i, k] = (image[position[1], k, i] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (coronal[i, k] > (colormapLength - 1))
                        coronal[i, k] = (colormapLength - 1);
                    if (coronal[i, k] < 0)
                        coronal[i, k] = 0;
                }

            bmp = getBitmap(coronal, modelFileHeader.z, modelFileHeader.x, map);
            gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;

            if (!showMRI)
            {
                for (int i = 0; i < modelFileHeader.z; i = i + fieldOrientationStep)
                    for (int k = 0; k < modelFileHeader.x; k = k + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[position[1], k, i]))
                        {
                            PointF point1 = new PointF((float)k, (float)i);
                            PointF point2 = new PointF((float)k, (float)i);
                            //draw arrows for maroms head
                            if ((toolStripComboBox1.Text.Equals("Adult_male_1_93_electrode_model") | toolStripComboBox1.Text.Equals("Adult_male_1_332_electrode_model")))//| toolStripComboBox1.Text.Equals("Stroke_patient_KS"))) //Testing Stroke Patient KS CNT 9/4/15
                            {
                                point2 = new PointF(-(float)(electricFields[position[1], k, i, 0] * fieldScale) + (float)k, -(float)(electricFields[position[1], k, i, 2] * fieldScale) + (float)i);
                            }
                            else//standard head
                            {
                                point2 = new PointF((float)(electricFields[position[1], k, i, 0] * fieldScale) + (float)k, -(float)(electricFields[position[1], k, i, 2] * fieldScale) + (float)i);// changed to make standard head work
                            }
                                    gr.DrawLine(pField, point1, point2);
                        }
                int colorbarHeight = pictureBox2.Height + 5;
                if (colorbarHeight % 8 <= 5)
                    colorbarHeight = colorbarHeight - colorbarHeight % 8;
                else
                    colorbarHeight = colorbarHeight + colorbarHeight % 8; //maybe unnecessary

                pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, map, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical"); //72 + 10 xp

                pictureBox4.Invalidate();
                pictureBox4.Visible = true;

                // curent max
                domainUpDown4.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 40, pictureBox2.Location.Y);
                // curent min
                domainUpDown5.Location = new Point(pictureBox2.Location.X + pictureBox2.Width + 40, pictureBox2.Location.Y + pictureBox2.Height - domainUpDown4.Height);

                domainUpDown4.Visible = true;
                domainUpDown5.Visible = true;
            }
            else
            {
                int colorbarHeight = pictureBox2.Height + 5;
                if (colorbarHeight % 8 <= 5)
                    colorbarHeight = colorbarHeight - colorbarHeight % 8;
                else
                    colorbarHeight = colorbarHeight + colorbarHeight % 8;

                pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, map, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical"); //72 + 10 xp
                pictureBox4.Invalidate();
                pictureBox4.Visible = false;
                labelFieldIntensity.Visible = false;
                domainUpDown4.Visible = false;
                domainUpDown5.Visible = false;
            }
            gr.DrawEllipse(p, position[0] - 5, position[2] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox1.BackgroundImage = bmp;
            else
                pictureBox1.Image = bmp;
            pictureBox1.Invalidate();

            gr.Dispose();
            p.Dispose();
            pField.Dispose();
            panel2.Invalidate();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;

            if (groupBox1.Visible)
            {
                position[0] = this.initialMousePos.X;
                position[2] = this.initialMousePos.Y;
                domainUpDown1.SelectedIndex = modelFileHeader.x - position[0] - 1;
                domainUpDown3.SelectedIndex = modelFileHeader.z - position[2] - 1;
            }
            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            }
            else if (selectElectrode)
            {
                for (int i = 0; i < currentIntensity.Length; ++i)
                {
                    double distance = Math.Sqrt(Math.Pow(this.initialMousePos.X - electrodeCenterCoorginates[i, 0], 2) + Math.Pow(this.initialMousePos.Y - electrodeCenterCoorginates[i, 1], 2));
                    if (distance <= electrodeSize / 2)
                    {
                        if (radioButton_uniform.Checked && radioButton_anode.Checked)
                        {
                            getCurrentIntensity();
                            number_of_anodes++;                            


                            if (number_of_anodes > 0 && number_of_cathodes > 0)
                            {
                                dataGridView1.Rows.Insert(number_of_anodes - 1, " ", " ");
                                dataGridView2.Rows.Insert(number_of_anodes - 1, " ", " ");

                                for (int j = 0 ; j < number_of_anodes ; j++)
                                {

                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                }
                                for (int j = number_of_anodes; j < number_of_anodes + number_of_cathodes; j++)
                                {

                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                }

                                dataGridView1.CurrentCell = dataGridView1[0, number_of_anodes-1];
                            }
                            else
                            {
                                dataGridView1.Rows.Add(" ", " ");
                                dataGridView2.Rows.Add(" ", " ");

                                for (int j = 0; j < number_of_anodes; j++)
                                {
                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                }
                                dataGridView1.CurrentCell = dataGridView1[0, number_of_anodes - 1];
                            }
                        }
                        if (radioButton_uniform.Checked && radioButton_cathode.Checked)
                        {
                            getCurrentIntensity();
                            number_of_cathodes++;

                            if (number_of_anodes > 0 && number_of_cathodes > 0)
                            {
                                dataGridView1.Rows.Insert(number_of_anodes + number_of_cathodes - 1, " ", " ");
                                dataGridView2.Rows.Insert(number_of_anodes + number_of_cathodes - 1, " ", " ");



                                for (int j = 0; j < number_of_anodes; j++)
                                {

                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                                }
                                for (int j = number_of_anodes; j < number_of_anodes + number_of_cathodes; j++)
                                {

                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                }

                                dataGridView1.CurrentCell = dataGridView1[0, number_of_anodes + number_of_cathodes - 1];
                            }
                            else
                            {
                                dataGridView1.Rows.Add(" ", " ");
                                dataGridView2.Rows.Add(" ", " ");

                                for (int j = number_of_anodes; j < number_of_anodes + number_of_cathodes; j++)
                                {
                                    dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                    dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                                }
                                dataGridView1.CurrentCell = dataGridView1[0, number_of_anodes + number_of_cathodes - 1];
                            }
                        }
                      
                        if(radioButton_flexible.Checked || radioButton_anode.Checked || radioButton_cathode.Checked) dataGridView1.CurrentRow.Cells[0].Value = biosemiLoc.channelName[i];

                        if (number_of_anodes > 0) { radioButton_cathode.Enabled = true; }
                        
                        dataGridView1.Focus();

                        string imagePath = modelDataPath + "Screenshots/" + biosemiLoc.channelName[i] + ".png";
                        if (File.Exists(@imagePath))
                        {
                            pictureBox2BackgroundImage1 = Image.FromFile(@imagePath);
                            pictureBox8.BackgroundImage = pictureBox2BackgroundImage1;
                        }
                        else
                        {
                            MessageBox.Show(biosemiLoc.channelName[i] + ".png does not exist.");
                            pictureBox2BackgroundImage1 = null;
                            pictureBox8.BackgroundImage = pictureBox2BackgroundImage1;

                            return;
                        }
                        break;
                    }
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label5.Text = "Coronal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label7.Text = "Transverse View {" + (this.initialMousePos.X + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
            label6.Text = "Sagittal View {" + (position[1] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            if (groupBox1.Visible)
            {
                position[1] = this.initialMousePos.X;
                position[2] = this.initialMousePos.Y;
                domainUpDown2.SelectedIndex = modelFileHeader.y - position[1] - 1;
                domainUpDown3.SelectedIndex = modelFileHeader.z - position[2] - 1;
            }

            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            }
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label6.Text = "Sagittal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label7.Text = "Transverse View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.X + 1).ToString() + "}";
            label5.Text = "Coronal View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            if (groupBox1.Visible)
            {
                position[0] = this.initialMousePos.X;
                position[1] = this.initialMousePos.Y;
                domainUpDown1.SelectedIndex = modelFileHeader.x - position[0] - 1;
                domainUpDown2.SelectedIndex = modelFileHeader.y - position[1] - 1;
            }
            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            }
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label7.Text = "Transverse View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label6.Text = "Sagittal View {" + (this.initialMousePos.Y + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            label5.Text = "Coronal View {" + (this.initialMousePos.X + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            buttonMRI.Visible = false;
            buttonOverlay.Visible = false;
            buttonField.Visible = false;
            float x = 20;
            float y = 20;
            Font font = new Font("Arial", 18, FontStyle.Bold);
            float lineHeight = 2 * font.GetHeight(e.Graphics);

            float W = e.MarginBounds.Width;
            float H = e.MarginBounds.Height;
            string[] toPrint = { "settings", "panelImage.Image" };
            Bitmap panelImage = new Bitmap(panel2.Width, panel2.Height);
            panel2.DrawToBitmap(panelImage, panel2.ClientRectangle);

            if (fileCounter == 2)
            {

                e.Graphics.DrawString("Result", font, Brushes.Black, x, y);
                y += lineHeight;
                lineHeight = font.GetHeight(e.Graphics);

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Model: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                e.Graphics.DrawString(toolStripComboBox1.Text, font, Brushes.Black, x + 230, y);
                y += lineHeight;

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Number of current sources: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                e.Graphics.DrawString(Convert.ToString(modelFileHeader.nSource), font, Brushes.Black, x + 230, y);
                y += lineHeight;

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Current intensity: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                y += lineHeight;
                e.Graphics.DrawString("Location" + "\t\t" + "Current (mA)", font, Brushes.Black, x + 230, y);
                y += lineHeight;
                for (int i = 0; i < dataGridView1.RowCount; ++i)
                {
                    e.Graphics.DrawString(Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).ToUpper() + "\t\t" + Convert.ToString(dataGridView1.Rows[i].Cells[1].Value), font, Brushes.Black, x + 230, y);
                    y += lineHeight;
                }

                y += lineHeight;
            }
            if (fileCounter == 1)
            {
                if (panelImage.Width / W < panelImage.Height / H)
                    W = panelImage.Width * H / panelImage.Height;
                else
                    H = panelImage.Height * W / panelImage.Width;
                e.Graphics.DrawImage(panelImage, 20, 20, W, H);
            }

            fileCounter -= 1;
            if (fileCounter > 0)
                e.HasMorePages = true;
            else
                fileCounter = toPrint.Length - 1;

            buttonMRI.Visible = true;
            buttonOverlay.Visible = true;
            buttonField.Visible = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            toolTipDataGridView1.Active = false;
            toolTipDataGridView1.Active = true;
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewCellValueChangedRows = e.RowIndex;
            if (dataGridViewCellValueChangedRows < 0) dataGridViewCellValueChangedRows = 0;
            if (dataGridView1.Rows.Count > 0)
            {

                double count = 0;
                for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
                {
                    if (IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value)))
                    {
                        dataGridView2.Rows[i].Cells[1].Value = dataGridView1.Rows[i].Cells[1].Value;
                        count = count + Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                    }
                    else if (!IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value)) && Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() != "" && Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() != "(REFERENCE)")
                    {
                        count = Math.Sqrt(-1);
                        MessageBox.Show("Not a valid entry");
                        dataGridView1.Rows[i].Cells[1].Value = "";
                        //dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[1].Value = "(REFERENCE)";
                        return;
                    }
                    else
                        count = Math.Sqrt(-1);
                    isElectrode = false;

                    for (int j = 0; j < modelFileHeader.nElectrode; ++j)
                    {
                        if (dataGridView1.Rows[i].Cells[0].Value != null && dataGridView1.Rows[i].Cells[0].Value.ToString().ToUpper().Trim() == biosemiLoc.channelName[j].ToUpper().Trim())
                        {
                            isElectrode = true;
                            break;
                        }
                    }
                    if (!isElectrode && Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).Trim() != "")
                    {
                        MessageBox.Show("Not a valid entry");
                        dataGridView1.Rows[i].Cells[0].Value = "";
                        dataGridView1.CurrentCell = dataGridView1[0, dataGridView1.CurrentRow.Index - 1];
                        return;
                    }
                }

                if (!double.IsNaN(count) && !radioButton_uniform.Checked)
                {
                    dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = count * (-1);
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = count * (-1);
                }

                currentMax = 0;
                currentMin = 0;
                getCurrentIntensity();
                if (currentValueChanged)
                {
                    showTopplot();
                    if (currentTabIndex == 1)
                        pictureBox1.Enabled = true;
                }
                selectElectrode = true;
                panel2.Invalidate();

                if (CheckDataGridViewAllEntered())
                    button1.Enabled = true;
            }


        }

        private bool CheckDataGridViewAllEntered()
        {
            bool allEntered = true;
            if (dataGridView1.Rows.Count > 1)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    if ((i < dataGridView1.Rows.Count - 1) && !IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim()))
                    {
                        allEntered = false;
                        break;
                    }
                    if (Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() == "" || Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).Trim() == "")
                    {
                        allEntered = false;
                        break;
                    }
                }
            }
            else
                allEntered = false;
            return allEntered && !isRepeated;
        }

        private void buttonField_Click(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = null;
            pictureBox2.BackgroundImage = null;
            pictureBox3.BackgroundImage = null;
            showField = true;
            showMRI = false;
            showOverlay = false;
            alpha = 255;
            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
        }

        private void buttonOverlay_Click(object sender, EventArgs e)
        {
            showOverlay = true;
            showMRI = false;
            showField = false;
            alpha = 255;
            showBackgroundImage = true;
            showImage(mri, colormapGray, 0.0, mriMax);
            alpha = overlayAlpha;
            showBackgroundImage = false;
            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
        }

        private void buttonMRI_Click(object sender, EventArgs e)
        {
            showMRI = true;
            showOverlay = false;
            showField = false;
            alpha = 255;
            showImage(mri, colormapGray, 0.0, mriMax);
        }

        //find percentile using c# writen by jibin 
        //Array sholud be sorted one
        //Count is the no of elements in array
        //Statistical_Level/Percentile level should between 0 - 99
        public double Leq_Stat(double[] InputNos, int count, double Statistical_Level)
        {
            double returnResult = 0;
            try
            {
                #region From the soted array calculate the percentile
                bool valid_Statistical_Level = true;
                double _percentilevalue = 0;

                if ((0 < Statistical_Level) && (Statistical_Level < 1))
                {
                    _percentilevalue = Statistical_Level;
                }
                else if ((1 <= Statistical_Level) && (Statistical_Level <= 99))
                {
                    _percentilevalue = 1 - (Statistical_Level / 100);
                }
                else
                    valid_Statistical_Level = false;

                if (valid_Statistical_Level)
                {
                    double allindex = 0, floatval = 0;
                    int intvalindex = 0;
                    double _percentile = 0;

                    allindex = (count - 1) * _percentilevalue;
                    intvalindex = Convert.ToInt32(allindex);
                    floatval = allindex - intvalindex;

                    //to check floatval is float itself or an integer 
                    double diff = floatval - Convert.ToInt32(floatval);

                    if (diff == 0)
                    {
                        _percentile = InputNos[intvalindex];
                    }
                    else
                    {
                        if (count > intvalindex + 1)
                            _percentile = floatval * (InputNos[intvalindex + 1] - InputNos[intvalindex]) + InputNos[intvalindex];
                        else
                            _percentile = InputNos[intvalindex];
                    }

                    //   _percentile=Math.Round(_percentile, 3);

                    returnResult = _percentile;
                }
                else
                    returnResult = 0;// "Invalid No As Statistical Level";


                #endregion
            }
            catch (Exception e1)
            {
                returnResult = 0;
            }

            return returnResult;
        }

        //http://en.wikipedia.org/wiki/Percentile
        public double percentile(double[] sequence, double excelPercentile)
        {
            // excelPercentile between [0 1]
            Array.Sort(sequence);
            int N = sequence.Length;
            double n = (N - 1) * excelPercentile + 1;
            // Another method: double n = (N + 1) * excelPercentile;
            if (n == 1d) return sequence[0];
            else if (n == N) return sequence[N - 1];
            else
            {
                int k = (int)n;
                double d = n - k;
                return sequence[k - 1] + d * (sequence[k] - sequence[k - 1]);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            PrintPreviewDialog previewdlg = new PrintPreviewDialog();
            doc.PrintPage += new PrintPageEventHandler(Doc_PrintPage);

            PrintDialog dlgSettings = new PrintDialog();
            dlgSettings.Document = doc;
            dlgSettings.UseEXDialog = true;
            fileCounter = 2;
            if (dlgSettings.ShowDialog() == DialogResult.OK)
                doc.Print();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap panelImage = new Bitmap(panel2.Width - 1, panel2.Height - 1, PixelFormat.Format32bppArgb);

            Graphics graphics = Graphics.FromImage(panelImage as Image);

            Point location = panel2.PointToScreen(Point.Empty);
            graphics.CopyFromScreen(location.X - 1, location.Y - 1, 0, 0, panelImage.Size, CopyPixelOperation.SourceCopy);
          
            // Displays a SaveFileDialog so the user can save the Image assigned to buttonSave.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "BITMAP|*.bmp|JPEG|*.jpg|GIF|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            buttonMRI.Visible = false;
            buttonOverlay.Visible = false;
            buttonField.Visible = false;
            panel2.BorderStyle = BorderStyle.None;

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                //System.IO.FileStream fs2 = (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;
                        
                    case 2:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 3:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }
                fs.Close();
            }
            buttonMRI.Visible = true;
            buttonOverlay.Visible = true;
            buttonField.Visible = true;
            panel2.BorderStyle = BorderStyle.Fixed3D;
        }

        private void coronalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Windows BMP Image (*.bmp)|*.bmp";//|JPEG Image (*.jpg)|*.jpg|GIF Image (*.gif)|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                //System.IO.FileStream fs2 = (System.IO.FileStream)saveFileDialog1.OpenFile();

                Bitmap coronal = new Bitmap(pictureBox1.Image.Width * 4, pictureBox1.Image.Height * 4);

                using (Graphics graphics2 = Graphics.FromImage(coronal))
                {
                    //set the resize quality modes to high quality
                    graphics2.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    graphics2.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphics2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    //draw the image into the target bitmap
                    graphics2.DrawImage(pictureBox1.Image, 0, 0, coronal.Width, coronal.Height);
                }
      
                coronal.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                // saggital.Save(fs2, System.Drawing.Imaging.ImageFormat.Bmp);
                // axial.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);

                fs.Close();
            }
        }

        private void sagittalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Windows BMP Image (*.bmp)|*.bmp";//|JPEG Image (*.jpg)|*.jpg|GIF Image (*.gif)|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                //System.IO.FileStream fs2 = (System.IO.FileStream)saveFileDialog1.OpenFile();

                Bitmap saggital = new Bitmap(pictureBox2.Image.Width * 4, pictureBox2.Image.Height * 4);

                using (Graphics graphics2 = Graphics.FromImage(saggital))
                {
                    //set the resize quality modes to high quality
                    graphics2.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    graphics2.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphics2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    //draw the image into the target bitmap
                    graphics2.DrawImage(pictureBox2.Image, 0, 0, saggital.Width, saggital.Height);
                }

                saggital.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                fs.Close();
            }
        }

        private void axialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Windows BMP Image (*.bmp)|*.bmp";//|JPEG Image (*.jpg)|*.jpg|GIF Image (*.gif)|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                //System.IO.FileStream fs2 = (System.IO.FileStream)saveFileDialog1.OpenFile();

                Bitmap axial = new Bitmap(pictureBox3.Image.Width * 4, pictureBox3.Image.Height * 4);

                using (Graphics graphics2 = Graphics.FromImage(axial))
                {
                    //set the resize quality modes to high quality
                    graphics2.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    graphics2.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphics2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    //draw the image into the target bitmap
                    graphics2.DrawImage(pictureBox3.Image, 0, 0, axial.Width, axial.Height);
                }

                axial.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                fs.Close();
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutHDExploreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            about = new About();
            about.Show();
        }





        private void domainUpDown1_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown1.Text.Trim()))
            {
                if (Convert.ToInt16(domainUpDown1.Text.Trim()) > 0 && Convert.ToInt16(domainUpDown1.Text.Trim()) <= modelFileHeader.x)
                {
                    position[0] = Convert.ToInt16(domainUpDown1.Text.Trim()) - 1;
                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown1.Text) <= 0)
                {
                    domainUpDown1.SelectedIndex = domainUpDown1.Items.Count - 1;
                }
                else if (Convert.ToInt32(domainUpDown1.Text) > modelFileHeader.x)
                {
                    domainUpDown1.SelectedIndex = 0;
                }
            }
        }

        private void domainUpDown2_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown2.Text.Trim()))
            {
                if (Convert.ToInt32(domainUpDown2.Text.Trim()) > 0 && Convert.ToInt32(domainUpDown2.Text.Trim()) <= modelFileHeader.y)
                {
                    position[1] = Convert.ToInt16(domainUpDown2.Text.Trim()) - 1;

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown2.Text) <= 0)
                {
                    domainUpDown2.SelectedIndex = domainUpDown2.Items.Count - 1;
                }
                else if (Convert.ToInt32(domainUpDown2.Text) > modelFileHeader.y)
                {
                    domainUpDown2.SelectedIndex = 0;
                }

            }
        }

        private void domainUpDown3_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown3.Text.Trim()))
            {
                if (Convert.ToInt16(domainUpDown3.Text.Trim()) > 0 && Convert.ToInt16(domainUpDown3.Text.Trim()) <= modelFileHeader.z)
                {
                    position[2] = Convert.ToInt16(domainUpDown3.Text.Trim()) - 1;

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown3.Text) <= 0)
                {
                    domainUpDown3.SelectedIndex = domainUpDown3.Items.Count - 1;
                }
                else if (Convert.ToInt32(domainUpDown3.Text) > modelFileHeader.z)
                {
                    domainUpDown3.SelectedIndex = 0;
                }
            }
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            Rectangle mouseRect = new Rectangle(e.X, e.Y, 1, 1);
            for (int i = 0; i < tabControl1.TabCount; i++)
            {
                if (tabControl1.GetTabRect(i).IntersectsWith(mouseRect))
                {
                    currentTabIndex = i;
                    break;
                }
            }

          

            if (currentTabIndex == 2 && groupBox1.Visible == false)
            {
                dataGridView1.Size = new Size(dataGridView1.Width, textBox2.Location.Y - dataGridView1.Location.Y - 10);
                groupBox2.Location = new Point(0, textBox3.Location.Y + textBox3.Height);
                groupBox4.Location = new Point(0, groupBox2.Location.Y + groupBox2.Height);
                groupBox5.Location = new Point(0, textBox4.Location.Y - groupBox5.Height - 3);
                groupBox4.Size = new Size(groupBox4.Width, groupBox5.Location.Y - groupBox4.Location.Y);

                dataGridView2.Size = new Size(dataGridView2.Width, groupBox4.Height - label4.Height - 40);

                if (CheckDataGridViewAllEntered())
                    button1.Enabled = true;
                pictureBox1.Enabled = false;
                pictureBox8.Visible = false;
                tissue_image.Visible = false;

            }
            if (currentTabIndex == 1 && toolStripComboBox1.Text != "Subject")
            {
                //disable mask selection
                tissue_image.Visible = false;
                comboBox3.ResetText();
                comboBox4.ResetText();

                radioButton_flexible.Checked = false;
                radioButton_uniform.Checked = false;
                button1.Enabled = false;
                if (tabControl1.SelectedIndex != 1)
                {
                    if (groupBox1.Visible == true)
                    {
                        tabControl1.TabPages[2].Focus();
                        tabControl1.SelectTab(tabPage3);
                        newSessionWindow = new newSession();
                        newSessionWindow.ShowDialog();
                        bool startNewSession = newSessionWindow.getSession();
                        if (startNewSession)
                        {
                            initialGUIState();
                            panel2.BackgroundImage = null;
                            pictureBox2BackgroundImage1 = null;
                            getCurrentIntensity();
                            showTopplot();
                            selectElectrode = true;
                            if (IsNumber(toolStripComboBox2.Text.Trim()))
                                pictureBox1.Enabled = true;
                            panel2.Invalidate();
                            tabControl1.SelectTab(tabPage2);
                            //reset model-specific cursor position
                            domainUpDown1.Items.Clear();
                        }
                        else pictureBox1.Enabled = true;
                        pictureBox8.Visible = true;
                        pictureBox8.Enabled = true;
                    }
                    else
                    {
                        getCurrentIntensity();
                        colorbarWidth = 30;
                        showTopplot();
                        selectElectrode = true;
                        if (IsNumber(toolStripComboBox2.Text.Trim()))
                            pictureBox1.Enabled = true;
                        panel2.Invalidate();
                        tabControl1.SelectTab(tabPage2);
                        //panel3.Visible = true;
                        pictureBox8.Visible = true;
                        pictureBox8.Enabled = true;
                    }
                }
            }

            if (toolStripComboBox1.Text == "Subject") { panel3.Visible = false; dataGridView1.Visible = false; dataGridView2.Visible = false; }
            else { panel3.Visible = true; dataGridView1.Visible = true; dataGridView2.Visible = true; }

            if (currentTabIndex == 0)
            {
                //display mask selection
                label12.Visible = true;
                label17.Visible = true;
                label18.Visible = true;
                comboBox4.Visible = true;
                comboBox3.Visible=true;



                pictureBox4.Enabled = false;
                pictureBox4.Visible = false;
                if (groupBox1.Visible)
                {
                    if (tabControl1.SelectedIndex != 0)
                    {
                        tabControl1.TabPages[2].Focus();
                        tabControl1.SelectTab(tabPage3);
                        newSessionWindow = new newSession();
                        newSessionWindow.ShowDialog();
                        bool startNewSession = newSessionWindow.getSession();
                        if (startNewSession)
                        {
                            initialGUIState();
                            pictureBox2BackgroundImage1 = null;
                            showElectrodeLocation();
                            tabControl1.SelectTab(tabPage1);
                            //reset model-specific cursor position
                            domainUpDown1.Items.Clear();
                        }
                        else pictureBox1.Enabled = true;
                    }
                }
            }

            //reset model-specific cursor position
           // if (currentTabIndex != 2) domainUpDown1.Items.Clear();
        }

        private void domainUpDown5_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true)
            {
                if (domainUpDown5.SelectedIndex > domainUpDown4.SelectedIndex) //if-else: in case not readonly
                {
                    electricFieldColormapMin = Convert.ToDouble(domainUpDown5.Items[domainUpDown5.SelectedIndex]);

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else
                {
                    for (int i = 0; i < domainUpDown5.Items.Count; ++i)
                        if (Math.Round(Convert.ToDouble(domainUpDown5.Items[i]), 2) == Math.Round(electricFieldColormapMin, 2))
                        {
                            domainUpDown5.SelectedIndex = i;
                            break;
                        }
                }
            }
        }

        private void domainUpDown4_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled)
            {
                if (domainUpDown5.SelectedIndex > domainUpDown4.SelectedIndex)
                {
                    electricFieldColormapMax = Convert.ToDouble(domainUpDown4.Items[domainUpDown4.SelectedIndex]);
                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else
                {
                    for (int i = 0; i < domainUpDown4.Items.Count; ++i)
                        if (Math.Round(Convert.ToDouble(domainUpDown4.Items[i]), 2) == Math.Round(electricFieldColormapMax, 2))
                        {
                            domainUpDown4.SelectedIndex = i;
                            break;
                        }
                }
            }
        }

        private void domainUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            return;
        }

        private void domainUpDown2_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            return;
        }

        private void domainUpDown3_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            return;
        }

        private void domainUpDown4_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void domainUpDown5_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void Adult_male_1_93_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_1_93_electrode_model.Name;
        }

        private void Adult_male_1_332_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_1_332_electrode_model.Name;
        }

        private void Adult_male_2_93_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_2_93_electrode_model.Name;
            //disable MNI locations display
            //label11.Visible = false;
        }
        private void Adult_male_3_93_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_3_93_electrode_model.Name;
            //disable MNI locations display
            //label11.Visible = false;
        }
        private void MNI_152_93_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = MNI_152_93_electrode_model.Name;
            //enable MNI locations display
             label8.Visible = false;
           //  label11.Visible = true;
        }
        private void Stroke_patient_KS_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_KS.Name;
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            if (dataGridView1.RowCount != 0 && dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[1].ToString().Trim() != "")
            {
               // initialGUIState();
                panel2.BackgroundImage = null;
                label3.Visible = true;
                toolStrip4.Visible = true;
                toolStripComboBox2.Visible = true;
            }
            

            if (IsNumber(toolStripComboBox2.Text.Trim()))
            {
                if (currentTabIndex == 1)
                    pictureBox1.Enabled = true;
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                for (int j = 0; j < Convert.ToInt16(toolStripComboBox2.Text); j++)
                {
                    dataGridView1.Rows.Add(" ", " ");
                    dataGridView2.Rows.Add(" ", " ");
                }
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = "(REFERENCE)";
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = "(REFERENCE)";
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].ReadOnly = true;
                modelFileHeader.nSource = Convert.ToInt32(toolStripComboBox2.Text.ToString()) - 1;
                button1.Enabled = false;

                textBox8.Text = toolStripComboBox2.Text;
                selectElectrode = true;

                currentMax = Convert.ToDouble(2.0);
                currentMin = -currentMax;
                pictureBox2BackgroundImage1 = null;
                getCurrentIntensity();
                showTopplot();
                panel2.Invalidate();
            }
            else
            {
                MessageBox.Show("Not a valid entry");
                if (dataGridView1.Rows.Count > 1)
                    toolStripComboBox2.SelectedIndex = dataGridView1.Rows.Count - 2;
            }
        }



        //ZOOM functionality electrode screenshot
        bool m=true;
        private void pictureBox8_DoubleClick(object sender, EventArgs e)
        {
            PictureBox screenshot_elecs = (PictureBox)sender;
            if (m)
            { ZoomIn(screenshot_elecs); m = false; }
            else { ZoomOut(screenshot_elecs); m = true; }
        }


       

        private void ZoomOut(PictureBox pic)
        {
             int MINMAX = 5;
             double ZOOMFACTOR = 6;

            if ((pic.Width > (panel2.Width / MINMAX)) &&
                  (pic.Height > (panel2.Height / MINMAX)))
            {
                pic.SizeMode = PictureBoxSizeMode.StretchImage;
                pic.Width = Convert.ToInt32(pic.Width / ZOOMFACTOR);
                pic.Height = Convert.ToInt32(pic.Height / ZOOMFACTOR);

                pictureBox4.Visible = true;
            }
        }

        private void ZoomIn(PictureBox pic)
        {
           // int MINMAX = 5;
            double ZOOMFACTOR = 6;

            if ((pic.Width < (panel2.Width)) &&
           (pic.Height < (panel2.Height)))
            {
                pic.Width = Convert.ToInt32(pic.Width * ZOOMFACTOR);
                pic.Height = Convert.ToInt32(pic.Height * ZOOMFACTOR);
                pic.SizeMode = PictureBoxSizeMode.StretchImage;

                panel2.HorizontalScroll.Value = pic.Width / 4;
                panel2.VerticalScroll.Value = pic.Height / 4;
                panel2.HorizontalScroll.Value = panel2.HorizontalScroll.Value + 1;

                pictureBox4.Visible = false;
            }
        }


        private void pictureBox8_MouseEnter(object sender, EventArgs e)
        {
            if (pictureBox8.Focused == false)
            {
                pictureBox8.Focus();
            }

            if (n == true)
            {
                toolTip_zoom.Show(string.Format("Double-click to zoom in and out.", 0, 0),
                                  this,
                                  MousePosition.X,
                                  MousePosition.Y,
                                  2000);
            }
        }

        private void textBox_current_TextChanged(object sender, EventArgs e)
        {

            if (modelFileHeader.nElectrode > 300) radioButton_pads.Enabled = true;
            else radioButton_pads.Enabled = false;

            number_of_anodes = 0;
            number_of_cathodes = 0;
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();

                double Num;
                if (double.TryParse(textBox_current.Text, out  Num) && Convert.ToDouble(textBox_current.Text) > 2)
                {
                    toolTip_maxcurrent.Show(string.Format("Maximum 2 mA current injection.", 0, 0),
                                                  textBox_current,
                                                  textBox_current.Width,
                                                  0,
                                                  2000);
                    textBox_current.Text = "";
                    panel5.Visible = false;
                    groupBox3.Visible = false;
                }
                else
                {
                    radioButton_anode.Visible = true;
                    radioButton_cathode.Visible = true;
                    radioButton_anode.Checked = false;
                    radioButton_cathode.Checked = false;
                    radioButton_electrodes.Checked = false;
                    radioButton_pads.Checked = false;
                    panel5.Visible = true;
                }
        }

        private void radioButton_flexible_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();

            if (radioButton_flexible.Checked)
            {
                dataGridView1.Columns[0].ReadOnly = true;
                dataGridView1.Columns[1].ReadOnly = false;
                dataGridView2.Columns[0].ReadOnly = true;
                dataGridView2.Columns[1].ReadOnly = true;
            }

            toolStrip4.Enabled = true;
            textBox_current.Visible = false;
            label_total_current.Visible = false;
            radioButton_anode.Visible = false;
            radioButton_cathode.Visible = false;
            label3.Visible = true;
            toolStrip4.Visible = true;
            toolStripComboBox2.SelectedText = "";
            groupBox3.Visible = false;
            panel5.Visible = false;
            groupBox_pads.Visible = false;
            selectElectrode = false;
        }


        private void radioButton_uniform_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            number_of_anodes = 0;
            number_of_cathodes = 0;

            if (radioButton_uniform.Checked)
            {
                dataGridView1.Columns[0].ReadOnly = true;
                dataGridView1.Columns[1].ReadOnly = true;
                dataGridView2.Columns[0].ReadOnly = true;
                dataGridView2.Columns[1].ReadOnly = true;
            }

            textBox_current.Visible = true;
            label_total_current.Visible = true;
            textBox_current.Text = "";
            label3.Visible = false;
            toolStrip4.Visible = false;
            panel5.Visible = false;
            groupBox_pads.Visible = false;
            selectElectrode = false;


        }
       

        private void radioButton_anode_CheckedChanged(object sender, EventArgs e)
        {
            
            if (radioButton_anode.Checked)
            {
                selectElectrode = true;

                if (currentTabIndex == 1)
                    pictureBox1.Enabled = true;

                button1.Enabled = false;
                
                selectElectrode = true;

                currentMax = Convert.ToDouble(2.0);
                currentMin = -currentMax;
                pictureBox2BackgroundImage1 = null;

                showTopplot();
                panel2.Invalidate();

                
            }
         
      
        }

        private void radioButton_cathode_CheckedChanged(object sender, EventArgs e)
        {
            
            if (radioButton_cathode.Checked)
            {
                selectElectrode = true;

                if (currentTabIndex == 1)
                    pictureBox1.Enabled = true;

                button1.Enabled = false;
                
                selectElectrode = true;

                currentMax = Convert.ToDouble(2.0);
                currentMin = -currentMax;
                pictureBox2BackgroundImage1 = null;

                showTopplot();
                panel2.Invalidate();
            }
   
        }

        private bool different_anode_cathode()
        {
            string[] anodes = Regex.Split(electrodes_list[comboBox1.SelectedIndex].ToString(), ", ");
            string[] cathodes = Regex.Split(electrodes_list[comboBox2.SelectedIndex].ToString(), ", ");
            ArrayList odds = new ArrayList();

            foreach (string txt in anodes)
            {
                if (Array.IndexOf(cathodes, txt) == -1)
                    odds.Add(txt);
            }

            foreach (string txt in cathodes)
            {
                if (Array.IndexOf(anodes, txt) == -1)
                    odds.Add(txt);
            }

            if (odds.Count == anodes.Length + cathodes.Length) return true;
            else return false;
            
        }

        private void radioButton_electrodes_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton_electrodes.Checked) selectElectrode = true;
            groupBox3.Visible = true;
            groupBox_pads.Visible = false;
            radioButton_anode.Checked = false;
            radioButton_cathode.Checked = false;
        }

        private void radioButton_pads_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_pads.Checked)
            {
                number_of_anodes = 0;
                number_of_cathodes = 0;
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                selectElectrode = false;
                read_pad_config();
            }
            groupBox3.Visible = false;
            groupBox_pads.Location = groupBox3.Location;
            groupBox_pads.Visible = true;
            
  

            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 1;

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }



        //read text file with pad configurations for 332 electrodes model
        private void read_pad_config()
        {
            int counter = 0;
            string line;
            string configFileName = modelDataPath + "pad_configurations.txt";
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            electrodes_list.Clear();

            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@configFileName);
                //skip first header line
                string skip = file.ReadLine();

                while ((line = file.ReadLine()) != null)
                {
                    string[] config = Regex.Split(line, ": ");
                    comboBox1.Items.Add(config[0]);
                    comboBox2.Items.Add(config[0]);
                    electrodes_list.Add(config[1]);
                    counter++;
                }

                file.Close();
            }

            catch
            {
                MessageBox.Show(configFileName + " does not exist.");
                return;       
            }
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != comboBox2.SelectedIndex)
            {
                if (different_anode_cathode())
                {
                    string[] anodes = Regex.Split(electrodes_list[comboBox1.SelectedIndex].ToString(), ", ");

                    //remove old anode pad configuration
                    if (number_of_anodes > 0)
                    {
                        for (int i = number_of_anodes - 1; i >= 0; i--)
                        {
                            dataGridView1.Rows.RemoveAt(i);
                            dataGridView2.Rows.RemoveAt(i);
                        }
                    }

                    number_of_anodes = 0;
                    //fill table with anodes
                    for (int i = 0; i < anodes.Length; i++)
                    {
                        dataGridView1.Rows.Add(" ", " ");
                        dataGridView2.Rows.Add(" ", " ");
                        dataGridView1.Rows[i].Cells[0].Value = anodes[i];
                        dataGridView2.Rows[i].Cells[0].Value = anodes[i];
                        number_of_anodes++;
                    }
                    Array.Clear(anodes, 0, anodes.Length);

                    // if cathodes already in the table, remove them, and add again after anodes in the table
                    if (number_of_cathodes > 0)
                    {
                        string[] cathodes = Regex.Split(electrodes_list[comboBox2.SelectedIndex].ToString(), ", ");

                        for (int i = number_of_anodes + number_of_cathodes - 1; i >= number_of_anodes; i--)
                        {
                            dataGridView1.Rows.RemoveAt(i);
                            dataGridView2.Rows.RemoveAt(i);
                        }

                        number_of_cathodes = 0;

                        for (int i = 0; i < cathodes.Length; i++)
                        {
                            dataGridView1.Rows.Add(" ", " ");
                            dataGridView2.Rows.Add(" ", " ");
                            dataGridView1.Rows[i + number_of_anodes].Cells[0].Value = cathodes[i];
                            dataGridView2.Rows[i + number_of_anodes].Cells[0].Value = cathodes[i];
                            number_of_cathodes++;
                        }
                        Array.Clear(cathodes, 0, cathodes.Length);
                    }

                    //fill table with currents
                    for (int j = 0; j < number_of_anodes; j++)
                    {
                        dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                        dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                    }
                    for (int j = number_of_anodes; j < number_of_anodes + number_of_cathodes; j++)
                    {
                        dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                        dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                    }
                }
                else { MessageBox.Show("The anodal pad you have selected overlaps with the cathodal pad. Please choose a different anode location."); comboBox1.SelectedIndex = old_anode_index; }
                old_cathode_index = 0;
                old_cathode_index = comboBox2.SelectedIndex;
            }
            else { MessageBox.Show("You have selected the same anodal as cathodal pad. Please choose a different anode location."); comboBox1.SelectedIndex = old_anode_index; }

            old_cathode_index = 0;
            old_cathode_index = comboBox2.SelectedIndex;
        }

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex != comboBox1.SelectedIndex)
            {
                if (different_anode_cathode())
                {
                    string[] cathodes = Regex.Split(electrodes_list[comboBox2.SelectedIndex].ToString(), ", ");

                    //remove old cathode pad configuration if existing
                    if (number_of_cathodes > 0)
                    {
                        for (int i = number_of_anodes + number_of_cathodes - 1; i >= number_of_anodes; i--)
                        {
                            dataGridView1.Rows.RemoveAt(i);
                            dataGridView2.Rows.RemoveAt(i);
                        }
                    }
                    // if anodes already in the table, remove them, and add them again, then add cathodes in the table
                    if (number_of_anodes > 0)
                    {
                        string[] anodes = Regex.Split(electrodes_list[comboBox1.SelectedIndex].ToString(), ", ");

                        for (int i = number_of_anodes - 1; i >= 0; i--)
                        {
                            dataGridView1.Rows.RemoveAt(i);
                            dataGridView2.Rows.RemoveAt(i);
                        }

                        number_of_anodes = 0;

                        for (int i = 0; i < anodes.Length; i++)
                        {
                            dataGridView1.Rows.Add(" ", " ");
                            dataGridView2.Rows.Add(" ", " ");
                            dataGridView1.Rows[i].Cells[0].Value = anodes[i];
                            dataGridView2.Rows[i].Cells[0].Value = anodes[i];
                            number_of_anodes++;
                        }
                        Array.Clear(anodes, 0, anodes.Length);
                    }

                    number_of_cathodes = 0;

                    for (int i = 0; i < cathodes.Length; i++)
                    {
                        dataGridView1.Rows.Add(" ", " ");
                        dataGridView2.Rows.Add(" ", " ");
                        dataGridView1.Rows[i + number_of_anodes].Cells[0].Value = cathodes[i];
                        dataGridView2.Rows[i + number_of_anodes].Cells[0].Value = cathodes[i];
                        number_of_cathodes++;
                    }
                    Array.Clear(cathodes, 0, cathodes.Length);

                    //fill table with currents
                    for (int j = 0; j < number_of_anodes; j++)
                    {
                        dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                        dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_anodes)).ToString().Replace(",", ".")), 3);
                    }
                    for (int j = number_of_anodes; j < number_of_anodes + number_of_cathodes; j++)
                    {
                        dataGridView1.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                        dataGridView2.Rows[j].Cells[1].Value = Math.Round(Convert.ToDouble((-Convert.ToDouble(textBox_current.Text) / Convert.ToDouble(number_of_cathodes)).ToString().Replace(",", ".")), 3);
                    }
                }
                else { MessageBox.Show("The cathodal pad you have selected overlaps with the anodal pad. Please choose a different cathode location."); comboBox1.SelectedIndex = old_anode_index; }
                old_anode_index = 0;
                old_anode_index = comboBox1.SelectedIndex;
            }
            else { MessageBox.Show("You have selected the same cathodal as anodal pad. Please choose a different cathodal location."); comboBox2.SelectedIndex = old_cathode_index; }

            old_anode_index = 0;
            old_anode_index = comboBox1.SelectedIndex;
        }

        private void domainUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void domainUpDown2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void domainUpDown3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox_current_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            return;
        }

        private void textBox_current_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column2_KeyPress);
            if (dataGridView1.CurrentCell.ColumnIndex == 1) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column2_KeyPress);
                }
            }
        }

        private void Column2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void Patient_1_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_AH.Name;
        }

        private void Patient_2_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_DE.Name;
        }

        private void Patient_3_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_JC.Name;
        }

        private void Patient_4_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_JE.Name;
        }

        private void Patient_5_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_KS.Name;
        }

        private void Patient_6_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_LM.Name;
        }

        private void Patient_7_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_MC1.Name;
        }

        private void Patient_8_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Stroke_patient_RS.Name;
        }

        private void textBox_current_Validating(object sender, CancelEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void item1dToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedItem != "" & comboBox3.SelectedItem != "") tissue_image.Visible = true;
            string tissueImagePath = modelDataPath +"masks\\"+ comboBox3.SelectedItem + comboBox4.SelectedItem +".png";
            if (File.Exists(@tissueImagePath))
            {

                tissue_image.Visible = true;
                tissue_image.Size = new System.Drawing.Size(650, 650);
                tissue_image.SizeMode = PictureBoxSizeMode.Zoom;
                tissue_image.Image = Image.FromFile(@tissueImagePath);

            }
            else tissue_image.Visible = false;
            
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedItem != "" & comboBox3.SelectedItem != "") tissue_image.Visible = true;
            string tissueImagePath = modelDataPath +"masks\\" + comboBox3.SelectedItem + comboBox4.SelectedItem + ".png";
            if (File.Exists(@tissueImagePath))
            {

                tissue_image.Visible = true;
                tissue_image.Size = new System.Drawing.Size(650, 650);
                tissue_image.SizeMode = PictureBoxSizeMode.Zoom;
                tissue_image.Image = Image.FromFile(@tissueImagePath);

            }
            else tissue_image.Visible = false;
            
        }

        //ZOOM functionality tissues
        bool n = true;
        //private void tissue_image_DoubleClick(object sender, EventArgs e)
        //{
        //    PictureBox screenshot_tissue = (PictureBox)sender;
        //    if (n)
        //    { ZoomIn(screenshot_tissue); n = false; }
        //    else { ZoomOut(screenshot_tissue); n = true; }
        //}
        // Commented out by CNT since feature is not required and distorts images once returning to normal view 7/8/2015

    }
}
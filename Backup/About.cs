﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;

namespace HDExplore
{
    public partial class About : Form
    {
        private string webLink;
        private string reference;
        private string referenceLink;
        public About()
        {
            InitializeComponent();
            labelName.Location = new Point(textBoxMessage.Location.X, 36);
            ReadXMLDocument();
        }

        private void ReadXMLDocument()
        {
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };
            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "aboutMessage":
                                {
                                    string aboutMessage = reader.GetAttribute("version");
                                    labelVersion.Text = "version: " + aboutMessage;
                                    aboutMessage = reader.GetAttribute("message");
                                    reference = reader.GetAttribute("reference");
                                    referenceLink = reader.GetAttribute("referenceLink");
                                    webLink = reader.GetAttribute("webLink");
                                    labelName.Text = reader.GetAttribute("name");// +"\u2122";
                                    textBoxMessage.Text = aboutMessage;
                                    //labelMessage.Text = aboutMessage;
                                    linkLabelWebLink.Text = webLink;
                                    //linkLabelreference.Text = "Referencere: \n" + reference;
                                    linkLabelreference.Text = "Reference:";
                                    textBoxRef.Text = reference;

                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void linkLabelreference_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(referenceLink);
        }

        private void linkLabelWebLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(webLink);
        }
    }
}

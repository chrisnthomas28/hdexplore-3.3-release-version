﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Threading;
using System.Drawing.Drawing2D;
using HDExplore;

// current: -2 mA to 2 mA
// field: range 0 V/m to maxField V/m. 
// values are normalized according to the (colormapLength-1). -1 is to adjust for C# indexing
namespace GUI
{
    public partial class Form1 : Form
    {
        private About about;
        private LoadData box;
        private newSession newSessionWindow;
        public ModelFileHeader.DataHeader modelFileHeader;
        public Electrode.BiosemiLoc biosemiLoc;
        // electric field
        private double[, , ,] electricFields;
        private double[, ,] electricField3D, brainMask, mri;
        private double maxField, maxField3D, fieldScale, scaleFactor, percentileRank;
        private int maxX, maxY, maxZ;
        private double[,] transeverse, sagittal, coronal, electrodeCenterCoorginates;
        private int[] position;
        private int overlayAlpha;
        //current intensity
        private double[] currentIntensity;
        private double[] currentIntensityNormalized;
        private double currentMax;
        private double currentMin;
        //colormap
        private int colormapLength;
        private int alpha;
        private int colorbarWidth;
        private int[,] colormapJet, colormapGray;
        private double electricFieldColormapMax, electricFieldColormapMin, mriMax;
        private string modelDataPath, dataPath;
        private int fileCounter;
        private bool showField, showMRI, showOverlay, showBackgroundImage, selectElectrode;
        private string reference;
        private bool isElectrode, isRepeated, currentValueChanged;
        private Image pictureBox2BackgroundImage0, pictureBox2BackgroundImage1;
        //topplot
        private int circleRadius, electrodeSize, leftTopX, leftTopY;
        //TODO
        private int offset;
        private int currentTabIndex = 0;
        private int dataGridViewCellValueChangedRows;
        private int fieldOrientationStep = 10;

        // Class to change the background color of the menu 
        public class RedTextRenderer : System.Windows.Forms.ToolStripRenderer
        {
            protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
            {
                e.TextColor = Color.Black;
                e.ToolStrip.BackColor = Color.LightGray;
                base.OnRenderItemText(e);
            }
        }

        public Form1()
        {
            // initialize the form
            InitializeComponent();
            pictureBox2BackgroundImage0 = panel2.BackgroundImage;
            pictureBox2BackgroundImage1 = pictureBox2BackgroundImage0;
            // set the position of the form components.
            this.Location = new Point(1, 1);
            // set the initial mouse click position
            position = new int[3];
            position[0] = pictureBox3.Width / 2;
            position[1] = pictureBox3.Height / 2;
            position[2] = pictureBox1.Height / 2;
            // set the colorbar parameters
            colormapLength = 256;
            alpha = 255;
            colorbarWidth = 30;
            // read the toolTip message from configuration XML
            ReadXMLDocument();
           // customize the form
            initialGUIState();
            panel2.BackgroundImage = pictureBox2BackgroundImage0;
            tabControl1.Location = new Point(0, panel1.Height - textBoxName.Location.Y - textBoxName.Height - 10);
            tabControl1.Size = new Size(panel1.Width, panel1.Height - textBoxName.Location.Y - textBoxName.Height - 10);            
            tabControl1.ItemSize = new Size(Convert.ToInt16(panel1.Width / 3 - 5), tabControl1.ItemSize.Height);
            // offset between windows 7 and window xp: window xp : offset  = 10; windows 7 : offset = 0;
            offset = 10;
            // change the background color of the menu 
            ToolStripManager.Renderer = new RedTextRenderer();
            currentMax = Convert.ToDouble(2.0);
            currentMin = -currentMax;
            // set activility of electrode models.
            string mDataPath = dataPath + "Adult_male_1_93_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_1_93_electrode_model.Enabled = false;
            }
            mDataPath = dataPath + "Adult_male_1_332_electrode_model\\";
            if (!(Directory.Exists(@mDataPath)))
            {
                Adult_male_1_332_electrode_model.Enabled = false;
            }
        }

        private void initialGUIState()
        {
            this.Size = new Size(1024, 726);
            panel1.Size = new Size(pictureBox7.Width+10, 693);
            panel2.Size = new Size(1024-panel1.Width, 693);
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            selectElectrode = false;
            printToolStripMenuItem.Enabled = false;
            buttonField.Visible = false;
            buttonField.Enabled = false;
            buttonMRI.Visible = false;
            buttonMRI.Enabled = false;
            buttonOverlay.Visible = false;
            buttonOverlay.Enabled = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            labelFieldIntensity.Visible = false;

            groupBox1.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label11.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
            domainUpDown1.Visible = false;
            domainUpDown2.Visible = false;
            domainUpDown3.Visible = false;
            domainUpDown4.Visible = false;
            domainUpDown5.Visible = false;

            showMRI = false;
            showField = false;
            showOverlay = false;
            showBackgroundImage = false;

            pictureBox1.BackgroundImage = null;
            pictureBox2.BackgroundImage = null;
            pictureBox3.BackgroundImage = null;
            alpha = 255;
            dataGridView1.Size = new Size(dataGridView1.Width, textBox2.Location.Y - dataGridView1.Location.Y - 10);
            groupBox2.Location = new Point(0, textBox3.Location.Y + textBox3.Height+20);
            groupBox4.Location = new Point(0, groupBox2.Location.Y + groupBox2.Height);
            groupBox5.Location = new Point(0, textBox4.Location.Y - groupBox5.Height-3);
            groupBox4.Size = new Size(groupBox4.Width, groupBox5.Location.Y - groupBox4.Location.Y);
            dataGridView2.Size = new Size(dataGridView2.Width, groupBox4.Height - label4.Height - 40);
            pictureBox1.Location = new Point(24, 75);
            pictureBox1.Size = new Size(panel2.Size.Width - 2 * pictureBox1.Location.X - 10, panel2.Size.Height - 2 * pictureBox1.Location.Y);
            pictureBox1.BackColor = Color.White;
            pictureBox2.BackColor = Color.White;
        }

        private void toolStripComboBox1_TextChanged(object sender, EventArgs e)
        {
            toolStripComboBox2.Items.Clear();
            if (toolStripComboBox1.Text != "Subject")
            {
                modelDataPath = dataPath + toolStripComboBox1.Text + "\\";
                initialGUIState();
                panel2.BackgroundImage = null;
                colorbarWidth = 0;
                bool electrodeLocationFileExist = showElectrodeLocation();
                if (electrodeLocationFileExist)
                {
                    currentIntensity = new double[modelFileHeader.nElectrode];
                    for (int i = 2; i < modelFileHeader.nElectrode; ++i)
                        toolStripComboBox2.Items.Add(Convert.ToString(i));

                    toolStrip4.Enabled = true;
                    button1.Enabled = false;
                    textBox7.Text = toolStripComboBox1.Text;
                }
                else
                {
                    panel2.BackgroundImage = pictureBox2BackgroundImage0;
                    toolStripComboBox1.Text = "Subject";
                    textBox7.Text = "";
                }
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                textBox8.Text = toolStripComboBox2.Text;
            }   
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage3 && groupBox1.Visible == false)
            {
                for (int i = 0; i < dataGridView1.RowCount; ++i)
                {
                    dataGridView2.Rows[i].Cells[0].Value = Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).ToUpper();
                    dataGridView2.Rows[i].Cells[0].Selected = false;
                    dataGridView2.Rows[i].Cells[1].Value = dataGridView1.Rows[i].Cells[1].Value;
                    dataGridView2.Rows[i].Cells[1].Selected = false;
                }
            }
        }

        private bool showElectrodeLocation()
        {
            bool electrodeLocationFileExist = false;
            string[] filePaths;
            try
            {
                filePaths = Directory.GetFiles(@modelDataPath, "*loc");
            }
            catch (Exception e1)
            {
                MessageBox.Show("Could not find a part of the path " + modelDataPath + ".");
                return electrodeLocationFileExist;
            }
            
            // only one LOC file for each model
            if (filePaths.Length > 0)
            {
                string electrodeLocationFilePath = filePaths[0];
                if (File.Exists(@electrodeLocationFilePath))
                {
                    readElectrodeLoc(electrodeLocationFilePath);
                    electrodeCenterCoorginates = new double[modelFileHeader.nElectrode, 2];
                }
                else
                {
                    MessageBox.Show(electrodeLocationFilePath + " does not exist.");
                    return electrodeLocationFileExist;
                }
            }
            else
            {
                MessageBox.Show("No LOC file found in " + modelDataPath + ".");
                return electrodeLocationFileExist;
            }

            electrodeLocationFileExist = true;
            MyColorMapClass cm = new MyColorMapClass(colormapLength, alpha);
            colormapJet = new int[colormapLength, 4];
            colormapJet = cm.Jet();

            colormapGray = new int[colormapLength, 4];
            colormapGray = cm.Gray();

            if (modelFileHeader.nElectrode >= 300)
            {
                circleRadius = (int)(panel2.Width / 4.5);
                electrodeSize = (int)(circleRadius / 12);
            }
            else
            {
                circleRadius = (int)(panel2.Width / 6);
                electrodeSize = (int)(circleRadius / 5.3);
            }

            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Size = new Size(panel2.Width-5, panel2.Height-5);
            
            leftTopX = (int)(pictureBox1.Width / 2 + 0.5) - circleRadius - colorbarWidth;
            leftTopY = (int)(pictureBox1.Height / 2 + 0.5) - circleRadius;
            double[] whiteTopplot = new double[modelFileHeader.nElectrode];
            for (int i = 0; i < whiteTopplot.Length; ++i)
                whiteTopplot[i] = colormapLength - 1;
            pictureBox1.Image = drawTopoPlot(panel2.Width, panel2.Height, whiteTopplot, colormapGray, leftTopX, leftTopY, circleRadius, electrodeSize, (float)5.1, true);
            pictureBox1.Visible = true;
            return electrodeLocationFileExist;
        }

        private void ReadXMLDocument()
        {
            // read the toolTip message from configuration XML
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };
            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "step1":
                                {
                                    tabControl1.TabPages[0].ToolTipText = reader.GetAttribute("step1Help");
                                    textBoxMessage.Text = reader.GetAttribute("step1Help");
                                    tabControl1.TabPages[0].Text = reader.GetAttribute("step1Tab");
                                    textBox6.Text = reader.GetAttribute("step1Title");
                                    toolStripComboBox1.Text = reader.GetAttribute("subjectSelectionDefaultText");
                                    item1dToolStripMenuItem.Text = reader.GetAttribute("normalSubject");
                                    item11ToolStripMenuItem.Text = reader.GetAttribute("normalSubject1");
                                    item12ToolStripMenuItem.Text = reader.GetAttribute("normalSubject2");
                                    item13ToolStripMenuItem.Text = reader.GetAttribute("normalSubject3");
                                    item2ToolStripMenuItem.Text = reader.GetAttribute("strokesubject");
                                    item21ToolStripMenuItem.Text = reader.GetAttribute("strokesubject1");
                                    item22ToolStripMenuItem.Text = reader.GetAttribute("strokesubject2");
                                    item23ToolStripMenuItem.Text = reader.GetAttribute("strokesubject3");
                                   break;
                                }
                            case "step2":
                                {
                                    tabControl1.TabPages[1].ToolTipText = reader.GetAttribute("step2Help");
                                    textBox2.Text = reader.GetAttribute("step2Help");
                                    tabControl1.TabPages[1].Text = reader.GetAttribute("step2Tab");
                                    textBox1.Text = reader.GetAttribute("step2Title");
                                    label3.Text = reader.GetAttribute("numElectrode");
                                    toolTipDataGridView1.SetToolTip(this.dataGridView1, reader.GetAttribute("currentTableHelp"));
                                    dataGridView1.Columns[0].Name = reader.GetAttribute("currentTableLocation");
                                    dataGridView1.Columns[1].Name = reader.GetAttribute("currentTableCurrent");
                                    dataGridView2.Columns[0].Name = reader.GetAttribute("currentTableLocation");
                                    dataGridView2.Columns[1].Name = reader.GetAttribute("currentTableCurrent");
                                    toolTipNumElectrode.SetToolTip(label3, reader.GetAttribute("numElectrodeHelp"));
                                    break;
                                }
                            case "step3":
                                {
                                    tabControl1.TabPages[2].ToolTipText = reader.GetAttribute("step3Help");
                                    textBox3.Text = reader.GetAttribute("step3Title");
                                    textBox4.Text = reader.GetAttribute("step3Help");
                                    label2.Text = reader.GetAttribute("subjectStep3");
                                    label6.Text = label3.Text;
                                    tabControl1.TabPages[2].Text = reader.GetAttribute("step3Tab");
                                    button1.Text = reader.GetAttribute("explore");
                                    buttonOverlay.Text = reader.GetAttribute("overlayButton");
                                    buttonMRI.Text = reader.GetAttribute("mriButton");
                                    buttonField.Text = reader.GetAttribute("fieldButton");
                                    toolTipVisualizationGUI.SetToolTip(this.button1, reader.GetAttribute("computeFields"));
                                    toolTipDataGridView2.SetToolTip(this.dataGridView2, reader.GetAttribute("currentTableHelp"));
                                    break;
                                }
                            case "GUI":
                                {
                                    textBoxName.Text = reader.GetAttribute("softwareName");
                                    break;
                                }
                            case "dataPath":
                                {
                                    dataPath = reader.GetAttribute("dataPath");
                                    break;
                                }
                            case "electricField":
                                {
                                    overlayAlpha = Convert.ToInt16(reader.GetAttribute("overlayAlpha"));
                                    scaleFactor = Convert.ToDouble(reader.GetAttribute("scaleFactor"));
                                    percentileRank = Convert.ToDouble(reader.GetAttribute("percentileRank"));
                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private Bitmap getBitmap(double[,] img, int height, int width, int[,] cmap)
        {
            //change 2-d array to Bitmap.
            byte[] buffer = new byte[height * width * 4];
            int count = 0;
            for (int i = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                {
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 3]; //R
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 2]; //G
                    buffer[count++] = (byte)cmap[(byte)(img[i, j]), 1]; //B
                    //if NaN, then make the pixel transparent
                    if (double.IsNaN(img[i, j]))
                        buffer[count++] = (byte)(cmap[(byte)(img[i, j]), 0] - 255);
                    else
                        buffer[count++] = (byte)alpha;
                }
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
            bitmap.UnlockBits(bmpData);
            return bitmap;
        }

        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            // sort dataGridView colume
            if (e.Column.Index == 2)
            {
                if (double.Parse(e.CellValue1.ToString()) > double.Parse(e.CellValue2.ToString()))
                    e.SortResult = 1;
                else if (double.Parse(e.CellValue1.ToString()) < double.Parse(e.CellValue2.ToString()))
                    e.SortResult = -1;
                else
                    e.SortResult = 0;
                e.Handled = true;
            }
        }

        private void showTopplot()
        {
            pictureBox6.Image = null;
            groupBox1.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            domainUpDown1.Visible = false;
            domainUpDown2.Visible = false;
            domainUpDown3.Visible = false;
            
            if (isRepeated)
                return;
            if (dataGridView1.RowCount == 0)
                initialGUIState();
            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Size = new Size(panel2.Width - 5, panel2.Height - 5);

            pictureBox1.Visible = false;

            if (modelFileHeader.nElectrode >= 100)
            {
                circleRadius = (int)(panel2.Width / 4.7);
                electrodeSize = (int)(circleRadius / 12.5); 
            }
            else if (modelFileHeader.nElectrode >= 92)
            {
                circleRadius = 87;
                electrodeSize = (int)(circleRadius / 5.8);
            }
            else
            {
                circleRadius = 95;
                electrodeSize = (int)(circleRadius / 5.8);
            }

            int colorbarHeight = 104 * 2;
            panel2.BackgroundImage = null;

            leftTopX = (int)(panel2.Width *0.646 + 0.5) - circleRadius - colorbarWidth;
            leftTopY = (int)(panel2.Height*0.64 + 0.5) - circleRadius;

            pictureBox1.Image = drawTopoPlot(panel2.Width, panel2.Height, currentIntensityNormalized, colormapJet, leftTopX, leftTopY, circleRadius, electrodeSize, (float)5.0, true);
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Visible = true;

            pictureBox2.Location = new Point(0, 0);
            pictureBox2.Size = new Size((int)(256 * 1.11), (int)(256));
            pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;
            if (dataGridView1.RowCount == 0)
                pictureBox2.BackgroundImage = null;
            else
                pictureBox2.BackgroundImage = pictureBox2BackgroundImage1;
            pictureBox2.Image = null;
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.Visible = true;
            pictureBox4.Height = colorbarWidth + 60;
            pictureBox4.Width = 20 + colorbarHeight + 40;
            pictureBox4.Location = new Point(panel2.Width - pictureBox4.Width - 10, pictureBox2.Height/6);

            pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, colormapJet, 10, 5, colorbarHeight, colorbarWidth, currentMax, currentMin, "Current Intensity (mA)", 33 + offset, "horizontal");
            pictureBox4.Visible = true;           
            panel2.Invalidate();
        }

        bool IsNumber(string text)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(text);
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            toolTip1.RemoveAll();
            toolTip1.Hide(this);
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // Check the value of the e.ColumnIndex property if you want to apply this formatting only so some rcolumns.
            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;
            }
        }

        private void Form1_Move(object sender, System.EventArgs e)
        {
            toolTip1.Hide(this);
            toolTip1.RemoveAll();
        }

        private void getCurrentIntensity()
        {
            currentValueChanged = false;
            currentIntensity = new double[modelFileHeader.nElectrode];
            isElectrode = false;
            for (int i = 0; i < dataGridView1.RowCount; ++i)
            {
                if (dataGridView1.Rows[i].Cells[0].Value != null)
                {
                    string electrodeName = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    isElectrode = false;
                    isRepeated = false;
                    for (int j = 0; j < modelFileHeader.nElectrode; ++j)
                    {
                        if (electrodeName.ToUpper().Trim() == biosemiLoc.channelName[j].ToUpper().Trim())
                        {
                            isElectrode = true;
                            if (dataGridViewCellValueChangedRows != i && dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value != null)
                            {
                                if (electrodeName.ToUpper().Trim() == dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value.ToString().ToUpper().Trim())
                                {
                                    isRepeated = true;
                                    var cell = dataGridView1.CurrentCell;
                                    var cellDisplayRect = dataGridView1.GetCellDisplayRectangle(0, dataGridViewCellValueChangedRows, false);
                                    toolTip1.Show(string.Format("Repeated entry", 0, 0),
                                                  dataGridView1,
                                                  cellDisplayRect.X + cell.Size.Width / 2,
                                                  cellDisplayRect.Y + cell.Size.Height / 2,
                                                  2000);
                                    dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value = " ";
                                    break;
                                }
                            }

                            if (isRepeated)
                                break;

                            if (IsNumber(dataGridView1.Rows[i].Cells[1].Value.ToString().Trim()))
                            {
                                if (currentIntensity[j] != Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value))
                                {
                                    currentValueChanged = true;
                                    currentIntensity[j] = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                                }
                            }
                            if (currentIntensity[j] > currentMax)
                                currentMax = currentIntensity[j];
                            if (currentIntensity[j] < currentMin)
                                currentMin = currentIntensity[j];
                            currentMax = Math.Max(Math.Abs(currentMax), Math.Abs(currentMin));
                            currentMin = -currentMax;
                            if (currentMin == currentMax)
                            {
                                currentMin = -Convert.ToDouble(2.0);
                                currentMax = Convert.ToDouble(2.0);
                            }
                            break;
                        }
                    }

                    if (isRepeated)
                        return;

                    if (!isElectrode && dataGridView1.Rows[i].Cells[0].Value.ToString().Trim() != "")
                    {
                        MessageBox.Show("Not a valid entry");
                        dataGridView1.Rows[dataGridViewCellValueChangedRows].Cells[0].Value = " ";
                        break;
                    }
                }
                
            }
            // normalize the current to [0 255]
            currentIntensityNormalized = new double[modelFileHeader.nElectrode];
            for (int i = 0; i < currentIntensity.Length; ++i)
                currentIntensityNormalized[i] = (currentIntensity[i] - currentMin) * (colormapLength - 1) / (currentMax - currentMin);           
            this.Current.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        }

        private double[, ,] getField()
        {
            double[, ,] electricFieldVolumn = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
            for (int i = 0; i < modelFileHeader.y; ++i)
            {
                Application.DoEvents();
                for (int j = 0; j < modelFileHeader.x; ++j)
                    for (int k = 0; k < modelFileHeader.z; ++k)
                    {
                        for (int m = 0; m < modelFileHeader.r; ++m)
                            electricFieldVolumn[i, j, k] = electricFieldVolumn[i, j, k] + Math.Pow(electricFields[i, j, k, m], 2.0);
                        electricFieldVolumn[i, j, k] = Math.Sqrt(electricFieldVolumn[i, j, k]) * brainMask[i, j, k];
                        if (electricFieldVolumn[i, j, k] > maxField)
                        {
                            maxField = electricFieldVolumn[i, j, k];
                            maxX = j;
                            maxY = i;
                            maxZ = k;
                        }
                    }
            }
            return electricFieldVolumn;
        }

        private void calculateElectricFields()
        {
            fieldScale = 0.0;
            // load the mixing matrixes and calculate the electric fields
            string brainFileName = modelDataPath + "nan_mask_brain";
            if (File.Exists(@brainFileName))
            {
                using (BinaryReader binReader = new BinaryReader(File.Open(@brainFileName, FileMode.Open)))
                {
                    modelFileHeader.version = Convert.ToSingle(binReader.ReadSingle());
                    modelFileHeader.x = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.y = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.z = Convert.ToInt16(binReader.ReadUInt16());
                    modelFileHeader.r = 3;
                    if (domainUpDown1.Items.Count == 0)
                    {
                        domainUpDown1.Items.Clear();
                        for (int i = modelFileHeader.x; i > 0; --i)
                            domainUpDown1.Items.Add(i);
                        domainUpDown2.Items.Clear();
                        for (int i = modelFileHeader.y; i > 0; --i)
                            domainUpDown2.Items.Add(i);
                        domainUpDown3.Items.Clear();
                        for (int i = modelFileHeader.z; i > 0; --i)
                            domainUpDown3.Items.Add(i);

                        position[0] = (int)(modelFileHeader.x / 2);
                        position[1] = (int)(modelFileHeader.y / 2);
                        position[2] = (int)(modelFileHeader.z / 2);

                        domainUpDown1.SelectedItem = Convert.ToString(position[0] + 1);
                        domainUpDown1.SelectedIndex = position[0];
                        domainUpDown2.SelectedItem = Convert.ToString(position[1] + 1);
                        domainUpDown2.SelectedIndex = position[1];
                        domainUpDown3.SelectedItem = Convert.ToString(position[2] + 1);
                        domainUpDown3.SelectedIndex = position[2];

                        domainUpDown1.Text = Convert.ToString(position[0] + 1);
                        domainUpDown2.Text = Convert.ToString(position[1] + 1);
                        domainUpDown3.Text = Convert.ToString(position[2] + 1);
                    }
                    brainMask = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
                    for (int j = modelFileHeader.z - 1; j >= 0; --j)
                    {
                        Application.DoEvents();
                        for (int k = 0; k < modelFileHeader.y; ++k)
                            for (int m = modelFileHeader.x - 1; m >= 0; --m)
                            {
                                int thisByte = Convert.ToInt16(binReader.ReadByte());
                                if (thisByte == 0)
                                    brainMask[k, m, j] = Double.NaN;
                                else
                                {
                                    brainMask[k, m, j] = Convert.ToDouble(thisByte);
                                }
                            }
                    }
                }
            }
            else
            {
                MessageBox.Show(brainFileName + " does not exist.");
                return;
            }

            // load the MRI
            string mriFileName = modelDataPath + "mri";
            if (File.Exists(@mriFileName))
            {
                using (BinaryReader binReader = new BinaryReader(File.Open(@mriFileName, FileMode.Open)))
                {
                    binReader.ReadSingle(); //version
                    double scalingFactor = Convert.ToDouble(binReader.ReadSingle());
                    binReader.ReadUInt16(); //x
                    binReader.ReadUInt16(); //y
                    binReader.ReadUInt16(); //z

                    mri = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
                    mriMax = 0.0;
                    for (int j = modelFileHeader.z - 1; j >= 0; --j)
                    {
                        Application.DoEvents();
                        for (int k = 0; k < modelFileHeader.y; ++k)
                            for (int m = modelFileHeader.x - 1; m >= 0; --m)
                            {
                                mri[k, m, j] = Convert.ToDouble(binReader.ReadByte()) / scalingFactor;
                                if (mri[k, m, j] > mriMax)
                                    mriMax = mri[k, m, j];
                            }
                    }
                }
            }
            else
            {
                MessageBox.Show(toolStripComboBox1.Text + " MRI " + " does not exist.");
                return;
            }

            transeverse = new double[modelFileHeader.y, modelFileHeader.x];
            sagittal = new double[modelFileHeader.z, modelFileHeader.y];
            coronal = new double[modelFileHeader.z, modelFileHeader.x];

            electricFields = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z, modelFileHeader.r];
            electricField3D = new double[modelFileHeader.y, modelFileHeader.x, modelFileHeader.z];
            maxX = 0;
            maxY = 0;
            maxZ = 0;
            maxField = 0.0;
            int nonZeroCurrentCount = 0;
            for (int n = 0; n < currentIntensity.Length - 1; ++n) //-1: The last value is always to be ignored! Indeed, there is no corresponding volume.
            {
                if (currentIntensity[n] != 0.0)
                    nonZeroCurrentCount++;
            }
            
            box.setProgressBarMaximum(nonZeroCurrentCount * modelFileHeader.r * modelFileHeader.z);

            int dataCount = 0;
            int count = 0;
            bool firstCurrent = true;
            
            for (int n = 0; n < currentIntensity.Length - 1; ++n) //-1: The last value is always to be ignored! Indeed, there is no corresponding volume.
            {

                if (currentIntensity[n] != 0.0)
                {
                    count++;
                    string fileName = modelDataPath + "A_all_" + (n + 1).ToString();

                    try
                    {
                        maxField3D = 0.0;
                        maxField = 0.0;
                        using (BinaryReader binReader = new BinaryReader(File.Open(fileName, FileMode.Open)))
                        {

                            //read header
                            binReader.ReadSingle(); //version
                            double scalingFactor = Convert.ToDouble(binReader.ReadSingle()); 
                            double minA = Convert.ToDouble(binReader.ReadSingle());
                            binReader.ReadUInt16(); //x 
                            binReader.ReadUInt16(); //y
                            binReader.ReadUInt16(); //z
                            binReader.ReadUInt16(); //3
                            for (int i = 0; i < modelFileHeader.r; ++i)
                            {

                                for (int j = modelFileHeader.z - 1; j >= 0; --j)
                                {
                                    dataCount++;
                                    box.updateProgressBar(dataCount);
                                    Application.DoEvents();
                                    for (int k = 0; k < modelFileHeader.y; ++k)
                                    {
                                        for (int m = modelFileHeader.x - 1; m >= 0; --m)
                                        {
                                            byte thisByte = binReader.ReadByte();
                                            if (thisByte == 255)
                                                electricFields[k, m, j, i] = Double.NaN;
                                            else
                                                electricFields[k, m, j, i] = electricFields[k, m, j, i] + (Convert.ToDouble(thisByte) / scalingFactor + minA) * currentIntensity[n];// / 0.1131;
                                            
                                            if (Math.Abs(electricFields[k, m, j, i]) > maxField3D)
                                                maxField3D = Math.Abs(electricFields[k, m, j, i]);
                                        }
                                    }
                                }
                            }                            
                        }
                        electricField3D = getField();

                        electricFieldColormapMax = Math.Ceiling(2 * maxField / 3 / .05) * .05; //round up to nearest .05
                        electricFieldColormapMin = 0.0;
                        
                        if (count == nonZeroCurrentCount)
                        {
                            domainUpDown4.Items.Clear();
                            domainUpDown5.Items.Clear();

                            for (double i = Math.Floor(maxField / .05) * .05; i >= 0; i = i - 0.05)
                            {
                                domainUpDown4.Items.Add(Math.Round(i,2));
                                domainUpDown5.Items.Add(Math.Round(i,2));
                            }
                            domainUpDown4.Items.Add(0);
                            domainUpDown5.Items.Add(0);
                            domainUpDown4.SelectedIndex = Convert.ToInt16(1 * domainUpDown4.Items.Count / 3);
                            domainUpDown5.SelectedIndex = domainUpDown5.Items.Count-1;
                            electricFieldColormapMax = Convert.ToDouble(domainUpDown4.Items[domainUpDown4.SelectedIndex]);
                            electricFieldColormapMin = Convert.ToDouble(domainUpDown5.Items[domainUpDown5.SelectedIndex]);

                            double[] fieldArray = new double[modelFileHeader.y * modelFileHeader.x * modelFileHeader.z * modelFileHeader.r];
                            double xy,yz,xz;
                            long arrayCount = 0;
                            for (int i = 0; i < electricFields.GetLength(0); ++i)
                                for (int j = 0; j < electricFields.GetLength(1); ++j)
                                    for (int k = 0; k < electricFields.GetLength(2); ++k)
                                        if (!double.IsNaN(electricFields[i, j, k, 0])&&!double.IsNaN(electricFields[i, j, k, 1])&&!double.IsNaN(electricFields[i, j, k, 2]))
                                        {
                                            xy = Math.Sqrt(Math.Pow(electricFields[i, j, k, 0],2.0) + Math.Pow(electricFields[i, j, k, 1],2.0));
                                            fieldArray[arrayCount] = xy;
                                            arrayCount++;
                                            yz = Math.Sqrt(Math.Pow(electricFields[i, j, k, 1],2.0) + Math.Pow(electricFields[i, j, k, 2],2.0));
                                            fieldArray[arrayCount] =yz;
                                            arrayCount++;
                                            xz = Math.Sqrt(Math.Pow(electricFields[i, j, k, 0],2.0) + Math.Pow(electricFields[i, j, k, 2],2.0));
                                            fieldArray[arrayCount] = xz;
                                            arrayCount++;
                                        }
                            double[] fieldArray2 = new double[arrayCount];
                            Array.Copy(fieldArray, fieldArray2, arrayCount);
                            double percentileTmp = percentile(fieldArray2, percentileRank/100);
                            fieldScale = Convert.ToDouble(fieldOrientationStep) / percentileTmp;
                        }
                        
                        if (firstCurrent)
                        {
                            // setup pictureBox1
                            pictureBox1.Visible = false;
                            pictureBox1.Dock = DockStyle.None;
                            pictureBox4.Visible = false;
                            pictureBox2.Visible = false;
                            pictureBox2.Dock = DockStyle.None;
                            panel2.Location = new Point(panel1.Width + panel1.Location.X, 0);
                            pictureBox1.Height = modelFileHeader.z;
                            pictureBox1.Width = modelFileHeader.x;
                            pictureBox1.Location = new Point(24, 75);
                            pictureBox1.BackColor = Color.Black;
                            label8.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y - label8.Height - 8);
                            label5.Location = new Point(pictureBox1.Location.X, label8.Location.Y - label5.Height - 8);
                            // setup pictureBox2
                            pictureBox2.Location = new Point(pictureBox1.Location.X + pictureBox1.Width + 96, pictureBox1.Location.Y);
                            pictureBox2.Height = modelFileHeader.z;;
                            pictureBox2.Width = modelFileHeader.x;
                            pictureBox2.Size = new Size(modelFileHeader.y, modelFileHeader.z);
                            pictureBox2.BackColor = Color.Black;
                            label6.Location = new Point(pictureBox2.Location.X, label5.Location.Y);
                            label9.Location = new Point(pictureBox2.Location.X, label8.Location.Y);
                            // setup pictureBox3
                            label7.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + pictureBox1.Height + 20);
                            label10.Location = new Point(pictureBox1.Location.X, label7.Location.Y + label7.Height + 8);
                            pictureBox3.Location = new Point(pictureBox1.Location.X, label10.Location.Y + label10.Height + 8);
                            pictureBox3.Height = modelFileHeader.y;
                            pictureBox3.Width = modelFileHeader.x;
                            pictureBox3.BackColor = Color.Black;
                            pictureBox6.Visible = false;
                            // setup pictureBox4
                            int topMaxHeight = Math.Max(pictureBox1.Height, pictureBox2.Height);

                            int colorbarX = pictureBox2.Location.X + pictureBox2.Width + 58;
                            int colorbarY = pictureBox2.Location.Y + topMaxHeight;
                            pictureBox4.Location = new Point(colorbarX, pictureBox2.Location.Y - 5);
                            pictureBox4.Width = colorbarWidth + 60;
                            pictureBox4.Height = 10 + topMaxHeight + 20;
                            labelFieldIntensity.Location = new Point(pictureBox2.Location.X + pictureBox2.Width, label6.Location.Y);

                            // setup pictureBox5
                            groupBox1.Location = new Point(pictureBox3.Location.X + pictureBox3.Width + 46, pictureBox2.Location.Y + pictureBox2.Height + 24 - 5);
                            pictureBox5.Location = new Point(0, 1); 

                            pictureBox5.Size = new Size(pictureBox2.Width + 113, pictureBox3.Height + 103);

                            // setup pictureBox6
                            pictureBox6.Location = new Point(pictureBox4.Location.X - groupBox1.Location.X + 1, pictureBox5.Location.Y);

                            pictureBox6.Width = pictureBox4.Width;
                            pictureBox6.Height = pictureBox5.Size.Height;
                            groupBox1.Size = new Size(pictureBox5.Width + pictureBox6.Width + 5, panel1.Height - groupBox1.Location.Y - 20);
                            groupBox1.Visible = true;
                            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);

                            if (modelFileHeader.nElectrode >= 100)
                            {
                                circleRadius = (int)(pictureBox3.Height / 2.1);
                                electrodeSize = 7;
                            }
                            else
                            {
                                circleRadius = (int)(pictureBox3.Height / 2.3);
                                electrodeSize = (int)(circleRadius / 5.3);
                            }
                            int colorbarHeight = pictureBox2.Height + 10;
                            if (colorbarHeight % 8 <= 4)
                                colorbarHeight = colorbarHeight - colorbarHeight % 8;
                            else
                                colorbarHeight = colorbarHeight + colorbarHeight % 8;
                            pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, colormapJet, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical");

                            if (modelFileHeader.nElectrode >= 100)
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius, electrodeSize, (float)3.4, false);
                            else
                                pictureBox5.Image = drawTopoPlot(pictureBox5.Size.Width, pictureBox5.Size.Height, currentIntensityNormalized, colormapJet, 256 / 4, 256 / 4, circleRadius, electrodeSize, (float)4.8, false);
                            colorbarHeight = 2 * circleRadius + 10;
                            if (colorbarHeight % 8 <= 4)
                                colorbarHeight = colorbarHeight - colorbarHeight % 8;
                            else
                                colorbarHeight = colorbarHeight + 8 - colorbarHeight % 8;
                            pictureBox6.Image = drawColorBar(pictureBox6.Width, pictureBox6.Height, colormapJet, 5, 256 / 4 + 2 * circleRadius + 10, colorbarWidth, colorbarHeight, currentMax, currentMin, "Current Intensity (mA)", circleRadius - 5 + offset, "vertical");

                            pictureBox5.Size = new Size(groupBox1.Width - pictureBox6.Width, pictureBox3.Height + 103);

                            pictureBox2.BackgroundImage = null;

                            pictureBox1.Visible = true;
                            pictureBox2.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            pictureBox5.Visible = true;
                            pictureBox6.Visible = true;
                            label5.Visible = true;
                            label6.Visible = true;
                            label7.Visible = true;
                            label8.Visible = true;
                            label9.Visible = true;
                            label10.Visible = true;
                            labelFieldIntensity.Visible = true;

                            this.Cursor = Cursors.Default;
                            this.AutoSize = true;
                            groupBox1.Invalidate();
                            firstCurrent = false;
                        }
                        else                     
                            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);

                        panel2.Invalidate();
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show("The solution file '" + fileName + "' for electrode '" + biosemiLoc.channelName[n] + "'" + " does not exist.");
                        getCurrentIntensity();
                        colorbarWidth = 30;
                        showTopplot();
                        selectElectrode = true;
                        pictureBox1.Enabled = true;
                        panel2.Invalidate();
                        selectElectrode = true;
                        showField = false;
                        showMRI = false;
                        showOverlay = false;
                        label5.Visible = false;
                        label6.Visible = false;
                        label7.Visible = false;
                        label8.Visible = false; 
                        label9.Visible = false;
                        label10.Visible = false;
                        labelFieldIntensity.Visible = false;
                        this.Cursor = Cursors.Arrow;
                        tabControl1.SelectTab(tabPage2);
                        
                        return;
                    }
                }
            }
 
            pictureBox1.Enabled = true;
            pictureBox2.Enabled = true;
            pictureBox3.Enabled = true;
            printToolStripMenuItem.Enabled = true;
            buttonOverlay.Visible = true;
            buttonOverlay.Enabled = true;
            // curent max
            label15.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, pictureBox4.Location.Y);
            domainUpDown4.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, label15.Location.Y + label15.Height + 2);
            // curent min
            label14.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, pictureBox4.Location.Y + pictureBox4.Height - label14.Height - domainUpDown5.Height - 22);
            domainUpDown5.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, label14.Location.Y + label14.Height + 2);

            // x 
            label13.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, pictureBox3.Location.Y);
            domainUpDown1.Location = new Point(label13.Location.X + label13.Width + 15, label13.Location.Y);
            // y
            label12.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, label13.Location.Y + pictureBox3.Height/2);
            domainUpDown2.Location = new Point(label12.Location.X + label12.Width + 15, label12.Location.Y);
            // z
            label11.Location = new Point(groupBox1.Location.X + groupBox1.Width + 15, label12.Location.Y + pictureBox3.Height/2);
            domainUpDown3.Location = new Point(label11.Location.X + label11.Width + 15, label11.Location.Y);

            label12.Visible = true;
            label13.Visible = true;
            label11.Visible = true;
            label14.Visible = true;
            label15.Visible = true;
            domainUpDown1.Visible = true;
            domainUpDown2.Visible = true;
            domainUpDown3.Visible = true;
            domainUpDown4.Visible = true;
            domainUpDown5.Visible = true;

            buttonMRI.Visible = true;
            buttonMRI.Enabled = true;

            buttonField.Visible = true;
            buttonField.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // visualize the current distribution
            panel1.Focus();
            selectElectrode = false;
            showField = true;
            string brainFileName = modelDataPath + "nan_mask_brain";
            if (File.Exists(@brainFileName))
            {
                this.Enabled = false;
                showField = true;
                showMRI = false;
                showOverlay = false;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    box = new LoadData(modelFileHeader.nSource);
                    
                    box.Show();
                    box.Cursor = Cursors.WaitCursor;

                    maxField = 0.0;
                    try
                    {
                        calculateElectricFields();
                        button1.Enabled = false;
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.ToString());
                        box.Close();
                        this.Cursor = Cursors.Arrow;
                        return;
                    }
                    this.Enabled = true;
                    box.Close();
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                    return;
                }
            }
            else
            {
                MessageBox.Show(brainFileName + " does not exist.");
                return;
            }
            printToolStripMenuItem.Enabled = true;
            toolTipVisualizationGUI.Active = false;
            toolTipVisualizationGUI.Active = true;
        }

        protected Bitmap drawColorBar(int pictureWidth, int pictureHeight, int[,] cmap, int x, int y, int width, int height, double max, double min, string label, int coordinate, string direction)
        {
            //draw the color bar
            Bitmap bmp = new Bitmap(pictureWidth, pictureHeight);
            Graphics g = Graphics.FromImage(bmp);
            Font aFont = new Font(button1.Font.Name, button1.Font.Size, button1.Font.Style, button1.Font.Unit);
            Pen p = new Pen(Color.White);

            if (direction == "vertical")
            {
                int ymin = 0;
                int ymax = (int)(height / 2);
                int dy = height / (ymax - ymin);
                int m = cmap.GetLength(0);

                for (int i = 0; i < ymax; i++)
                {
                    int colorIndex = (int)((i - ymin) * m / (ymax - ymin));
                    SolidBrush aBrush = new SolidBrush(Color.FromArgb(
                        cmap[colorIndex, 0], cmap[colorIndex, 1],
                        cmap[colorIndex, 2], cmap[colorIndex, 3]));
                    g.FillRectangle(aBrush, x, y - i * dy - dy, width, dy);
                    if (i % (height / 8) == 0)
                    {
                        g.DrawLine(p, x + (width - 1) - 3, y - i * dy, x + (width - 1), y - i * dy);
                        g.DrawLine(p, x, y - i * dy, x + 3, y - i * dy);
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(Convert.ToString(Math.Round((double)colorIndex / m * (max - min) + min,2)), aFont, Brushes.Black, x + width + 2, y - i * dy - 6);
                    }
                }
                g.DrawLine(p, x + (width - 1) - 3, y - height - 1, x + (width - 1), y - height - 1);
                g.DrawLine(p, x, y - height - 1, x + 3, y - height - 1);

                g.DrawString(Convert.ToString(max), aFont, Brushes.Black, x + width + 2, y - (ymax - 1) * dy - 6);

                aFont = new Font("Microsoft Sans Serif", 9.8F);
                StringFormat sf = new StringFormat(StringFormatFlags.DirectionVertical);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString(label, aFont, Brushes.Black, x + width + 2 + 33, coordinate, sf);
            }
            else if (direction == "horizontal")
            {
                int xmin = 0;
                int xmax = (int)(width / 2);
                int dx = width / (xmax - xmin);
                int m = cmap.GetLength(0);

                for (int i = 0; i < xmax; i++)
                {
                    int colorIndex = (int)((i - xmin) * m / (xmax - xmin));
                    SolidBrush aBrush = new SolidBrush(Color.FromArgb(
                        cmap[colorIndex, 0], cmap[colorIndex, 1],
                        cmap[colorIndex, 2], cmap[colorIndex, 3]));
                    g.FillRectangle(aBrush, x + i * dx + dx, y, dx, height);
                    if (i % (width / 8) == 0)
                    {
                        g.DrawLine(p, x + i * dx, y + (height - 1) - 3, x + i * dx, y + (height - 1));
                        g.DrawLine(p, x + i * dx, y, x + i * dx, y + 3);
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(Convert.ToString(Math.Round((double)colorIndex / m * (max - min) + min, 2)), aFont, Brushes.Black, x + i * dx - dx - 3, y + height + 2);
                    }
                }
                g.DrawLine(p, x - width - 1, y + (height - 1) - 3, x - width - 1, y + (height - 1));
                g.DrawLine(p, x - width - 1, y, x - width - 1, y + 3);

                g.DrawString(Convert.ToString(max), aFont, Brushes.Black, x + (xmax - 1) * dx - 4, y + height + 2);

                aFont = new Font("Microsoft Sans Serif", 9.8F);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString(label, aFont, Brushes.Black, coordinate, y + height + 23);
            }
            g.Dispose();
            p.Dispose();
            return bmp;
        }

        private void readElectrodeLoc(string locFileNames)
        {            
            using (BinaryReader binReader = new BinaryReader(File.Open(@locFileNames, FileMode.Open)))
            {
                modelFileHeader.nElectrode = Convert.ToInt32(binReader.ReadSingle());
                biosemiLoc.channelNumber = new int[modelFileHeader.nElectrode];
                biosemiLoc.channelName = new string[modelFileHeader.nElectrode];
                biosemiLoc.degrees = new double[modelFileHeader.nElectrode];
                biosemiLoc.arcLength = new double[modelFileHeader.nElectrode];
                string channelName;
                int nChart = Convert.ToByte(binReader.ReadSingle());
                for (int i = 0; i < modelFileHeader.nElectrode; ++i)
                {
                    biosemiLoc.channelNumber[i] = Convert.ToInt32(binReader.ReadSingle());
                    biosemiLoc.degrees[i] = (Convert.ToDouble(binReader.ReadSingle()) - 90.0) * Math.PI / 180;
                    biosemiLoc.arcLength[i] = Convert.ToDouble(binReader.ReadSingle());
                    channelName = new string(binReader.ReadChars(nChart));
                    biosemiLoc.channelName[i] = channelName.Replace('.', ' ');
                    biosemiLoc.channelName[i] = biosemiLoc.channelName[i].Trim();
                    if (i == modelFileHeader.nElectrode - 1)
                        reference = biosemiLoc.channelName[i];
                }
            }
        }

        protected Bitmap drawTopoPlot(int pictureWidth, int pictureHeight, double[] currentIntensity, int[,] cmap, int leftTopX, int leftTopY, int circleRadius, int electrodeSize, float fontSize, bool showLabel)
        {
            // draw topplot
            Bitmap bmp = new Bitmap(pictureWidth, pictureHeight);
            Graphics g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Pen p = new Pen(Color.Gray);
            p.Width = 2.0F;
            int lineLength = 15;
            //nose
            g.DrawLine(p, leftTopX + circleRadius - lineLength, leftTopY, leftTopX + circleRadius, leftTopY - lineLength);
            g.DrawLine(p, leftTopX + circleRadius + lineLength, leftTopY, leftTopX + circleRadius, leftTopY - lineLength);
            //left ear
            g.DrawLine(p, leftTopX, leftTopY + circleRadius - 2 * lineLength, leftTopX - lineLength, leftTopY + circleRadius - lineLength);
            g.DrawLine(p, leftTopX - lineLength, leftTopY + circleRadius - lineLength, leftTopX, leftTopY + circleRadius);
            g.DrawLine(p, leftTopX, leftTopY + circleRadius, leftTopX - lineLength, leftTopY + circleRadius + lineLength);
            g.DrawLine(p, leftTopX - lineLength, leftTopY + circleRadius + lineLength, leftTopX, leftTopY + circleRadius + 2 * lineLength);
            //right ear
            g.DrawLine(p, leftTopX + circleRadius * 2, leftTopY + circleRadius - 2 * lineLength, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius - lineLength);
            g.DrawLine(p, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius - lineLength, leftTopX + circleRadius * 2, leftTopY + circleRadius);
            g.DrawLine(p, leftTopX + circleRadius * 2, leftTopY + circleRadius, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius + lineLength);
            g.DrawLine(p, leftTopX + circleRadius * 2 + lineLength, leftTopY + circleRadius + lineLength, leftTopX + circleRadius * 2, leftTopY + circleRadius + 2 * lineLength);

            // Describes the brush's color using RGB values. 
            // Each value has a range of 0-255.
            Color topplot = Color.FromArgb(alpha, cmap[(int)(colormapLength / 2), 3], cmap[(int)(colormapLength / 2), 2], cmap[(int)(colormapLength / 2), 1]);
            SolidBrush mySolidBrush = new SolidBrush(topplot);
            g.DrawEllipse(p, leftTopX, leftTopY, circleRadius * 2, circleRadius * 2);

            Font aFont = new Font(button1.Font.Name, fontSize, button1.Font.Style, button1.Font.Unit);
            
            for (int i = 0; i < currentIntensity.Length; ++i)
            {
                topplot = Color.FromArgb(alpha, cmap[(int)(currentIntensity[i] + 0.5), 1], cmap[(int)(currentIntensity[i] + 0.5), 2], cmap[(int)(currentIntensity[i] + 0.5), 3]);
                mySolidBrush = new SolidBrush(topplot);
                g.FillEllipse(mySolidBrush, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 2), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 2), electrodeSize, electrodeSize);
                electrodeCenterCoorginates[i, 0] = (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius -2);
                electrodeCenterCoorginates[i, 1] = (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius -2);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                if (showLabel)
                {
                    if (modelFileHeader.nElectrode >= 100)
                    {
                        if (biosemiLoc.channelName[i].Length == 4)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.1), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 3)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.47), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 2)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.8), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                    }
                    else
                    {
                        if (biosemiLoc.channelName[i].Length == 4)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 1.7), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 3)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 2.3), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                        else if (biosemiLoc.channelName[i].Length == 2)
                            g.DrawString(biosemiLoc.channelName[i], aFont, Brushes.Black, (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Cos(biosemiLoc.degrees[i]) + leftTopX + circleRadius - electrodeSize / 3.2), (float)(biosemiLoc.arcLength[i] * circleRadius * 2 * Math.Sin(biosemiLoc.degrees[i]) + leftTopY + circleRadius - electrodeSize / 4));
                    }
                }
            }

            g.Dispose();
            p.Dispose();
            mySolidBrush.Dispose();

            return bmp;
        }

        private Point initialMousePos;

        private void showImage(double[, ,] image, int[,] map, double colormapMin, double colormapMax)
        {
            label8.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            label9.Text = "Selected Position: {" + (position[1] + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            label10.Text = "Selected Position: {" + (position[0] + 1).ToString() + "," + (position[1] + 1).ToString() + "}";

            if (Double.IsNaN(image[position[1], position[0], position[2]]))
                labelFieldIntensity.Text = "Field Intensity: " + image[position[1], position[0], position[2]].ToString();
            else
                labelFieldIntensity.Text = "Field Intensity: " + Math.Round(image[position[1], position[0], position[2]], 2).ToString() + " V/m";

            // update contents shown in pictureBox1
            for (int j = 0; j < modelFileHeader.x; ++j)
                for (int k = 0; k < modelFileHeader.y; ++k)
                {
                    transeverse[k, j] = (image[k, j, position[2]] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (transeverse[k, j] > (colormapLength - 1))
                        transeverse[k, j] = (colormapLength - 1);
                    if (transeverse[k, j] < 0)
                        transeverse[k, j] = 0;
                }
            Bitmap bmp = getBitmap(transeverse, modelFileHeader.y, modelFileHeader.x, map);

            Graphics gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            Pen p = new Pen(Color.White);
            p.Width = 2.0F;

            Pen pField = new Pen(Color.Black);
            pField.Width = 1.0F;
            pField.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            if (!showMRI)
            {
                for (int j = 0; j < modelFileHeader.x; j = j + fieldOrientationStep)
                    for (int k = 0; k < modelFileHeader.y; k = k + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[k, j, position[2]]))
                        {
                            PointF point1 = new PointF((float)j, (float)k);
                            PointF point2 = new PointF(-(float)(electricFields[k, j, position[2], 0] * fieldScale) + (float)j, (float)(electricFields[k, j, position[2], 1] * fieldScale) + (float)k);
                            gr.DrawLine(pField, point1, point2);
                        }
            }
            gr.DrawEllipse(p, position[0] - 5, position[1] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox3.BackgroundImage = bmp;
            else
                pictureBox3.Image = bmp;
            pictureBox3.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < modelFileHeader.z; ++i)
                for (int j = 0; j < modelFileHeader.y; ++j)
                {
                    sagittal[i, j] = (image[j, position[0], i] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (sagittal[i, j] > (colormapLength - 1))
                        sagittal[i, j] = (colormapLength - 1);
                    if (sagittal[i, j] < 0)
                        sagittal[i, j] = 0;

                }
            bmp = getBitmap(sagittal, modelFileHeader.z, modelFileHeader.y, map);
            gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            if (!showMRI)
            {
                for (int i = 0; i < modelFileHeader.z; i = i + fieldOrientationStep)
                    for (int j = 0; j < modelFileHeader.y; j = j + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[j, position[0], i]))
                        {
                            PointF point1 = new PointF((float)j, (float)i);
                            PointF point2 = new PointF((float)(electricFields[j, position[0], i, 1] * fieldScale) + (float)j, -(float)(electricFields[j, position[0], i, 2] * fieldScale) + (float)i);
                            gr.DrawLine(pField, point1, point2);
                        }                
            }
            gr.DrawEllipse(p, position[1] - 5, position[2] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox2.BackgroundImage = bmp;
            else
                pictureBox2.Image = bmp;
            pictureBox2.Invalidate();

            // update contents shown in pictureBox2
            for (int i = 0; i < modelFileHeader.z; ++i)
                for (int k = 0; k < modelFileHeader.x; ++k)
                {
                    coronal[i, k] = (image[position[1], k, i] - colormapMin) / (colormapMax - colormapMin) * (colormapLength - 1);
                    if (coronal[i, k] > (colormapLength - 1))
                        coronal[i, k] = (colormapLength - 1);
                    if (coronal[i, k] < 0)
                        coronal[i, k] = 0;
                }

            bmp = getBitmap(coronal, modelFileHeader.z, modelFileHeader.x, map);
            gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;

            if (!showMRI)
            {
                for (int i = 0; i < modelFileHeader.z; i = i + fieldOrientationStep)
                    for (int k = 0; k < modelFileHeader.x; k = k + fieldOrientationStep)
                        if (!double.IsNaN(brainMask[position[1], k, i]))
                        {
                            PointF point1 = new PointF((float)k, (float)i);
                            PointF point2 = new PointF(-(float)(electricFields[position[1], k, i, 0] * fieldScale) + (float)k, -(float)(electricFields[position[1], k, i, 2] * fieldScale) + (float)i);
                            gr.DrawLine(pField, point1, point2);
                        }
                int colorbarHeight = pictureBox2.Height + 10;
                if (colorbarHeight % 8 <= 4)
                    colorbarHeight = colorbarHeight - colorbarHeight % 8;
                else
                    colorbarHeight = colorbarHeight + colorbarHeight % 8;

                pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, map, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical"); //72 + 10 xp
                pictureBox4.Invalidate();
                pictureBox4.Visible = true;
            }
            else
            {
                int colorbarHeight = pictureBox2.Height + 10;
                if (colorbarHeight % 8 <= 4)
                    colorbarHeight = colorbarHeight - colorbarHeight % 8;
                else
                    colorbarHeight = colorbarHeight + colorbarHeight % 8;

                pictureBox4.Image = drawColorBar(pictureBox4.Width, pictureBox4.Height, map, 5, Math.Max(pictureBox1.Height, pictureBox2.Height) + 10, colorbarWidth, colorbarHeight, electricFieldColormapMax, electricFieldColormapMin, "Field Intensity (V/m)", (int)(pictureBox2.Height / 4), "vertical"); //72 + 10 xp
                pictureBox4.Invalidate();
                pictureBox4.Visible = false;
                labelFieldIntensity.Visible = false;
            }
            gr.DrawEllipse(p, position[0] - 5, position[2] - 5, 10, 10);
            if (showBackgroundImage)
                pictureBox1.BackgroundImage = bmp;
            else
                pictureBox1.Image = bmp;
            pictureBox1.Invalidate();

            gr.Dispose();
            p.Dispose();
            pField.Dispose();
            panel2.Invalidate();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            
            if (groupBox1.Visible)
            {
                position[0] = this.initialMousePos.X;
                position[2] = this.initialMousePos.Y;
                domainUpDown1.SelectedIndex = modelFileHeader.x - position[0] -1;
                domainUpDown3.SelectedIndex = modelFileHeader.z - position[2] - 1;
            }
            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
             }
            else if (selectElectrode)
            {
                for (int i = 0; i < currentIntensity.Length; ++i)
                {
                    double distance = Math.Sqrt(Math.Pow(this.initialMousePos.X - electrodeCenterCoorginates[i, 0], 2) + Math.Pow(this.initialMousePos.Y - electrodeCenterCoorginates[i, 1], 2));
                    if (distance <= electrodeSize/2)
                    {
                        dataGridView1.CurrentRow.Cells[0].Value = biosemiLoc.channelName[i];
                        string imagePath = modelDataPath + "Screenshots/" + biosemiLoc.channelName[i] + ".png";
                        if (File.Exists(@imagePath))
                        {
                            pictureBox2BackgroundImage1 = Image.FromFile(@imagePath);
                            pictureBox2.BackgroundImage = pictureBox2BackgroundImage1;
                        }
                        else
                        {
                            MessageBox.Show(biosemiLoc.channelName[i] + ".png does not exist.");
                            pictureBox2BackgroundImage1 = null;
                            pictureBox2.BackgroundImage = pictureBox2BackgroundImage1;
                            return;
                        }
                        break;
                    }                    
                }                
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label5.Text = "Coronal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label7.Text = "Transverse View {" + (this.initialMousePos.X + 1).ToString() + "," + (position[1] + 1).ToString() + "}";
            label6.Text = "Sagittal View {" + (position[1] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            if (groupBox1.Visible)
            {
                position[1] = this.initialMousePos.X;
                position[2] = this.initialMousePos.Y;
                domainUpDown2.SelectedIndex = modelFileHeader.y - position[1] -1;
                domainUpDown3.SelectedIndex = modelFileHeader.z - position[2] -1;
            }

            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            }
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label6.Text = "Sagittal View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label7.Text = "Transverse View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.X + 1).ToString() + "}";
            label5.Text = "Coronal View {" + (position[0] + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            if (groupBox1.Visible)
            {
                position[0] = this.initialMousePos.X;
                position[1] = this.initialMousePos.Y;
                domainUpDown1.SelectedIndex = modelFileHeader.x - position[0] -1;
                domainUpDown2.SelectedIndex = modelFileHeader.y - position[1] -1;
            }
            if (showField)
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            else if (showMRI)
                showImage(mri, colormapGray, 0.0, mriMax);
            else if (showOverlay)
            {
                alpha = 255;
                showBackgroundImage = true;
                showImage(mri, colormapGray, 0.0, mriMax);
                alpha = overlayAlpha;
                showBackgroundImage = false;
                showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
            }
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            this.initialMousePos = e.Location;
            label7.Text = "Transverse View {" + (this.initialMousePos.X + 1).ToString() + "," + (this.initialMousePos.Y + 1).ToString() + "}";
            label6.Text = "Sagittal View {" + (this.initialMousePos.Y + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
            label5.Text = "Coronal View {" + (this.initialMousePos.X + 1).ToString() + "," + (position[2] + 1).ToString() + "}";
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "Transverse View";
            label6.Text = "Sagittal View";
            label5.Text = "Coronal View";
        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            buttonMRI.Visible = false;
            buttonOverlay.Visible = false;
            buttonField.Visible = false;
            float x = 20;
            float y = 20;
            Font font = new Font("Arial", 18, FontStyle.Bold);
            float lineHeight = 2 * font.GetHeight(e.Graphics);

            float W = e.MarginBounds.Width;
            float H = e.MarginBounds.Height;
            string[] toPrint = { "settings", "panelImage.Image" };
            Bitmap panelImage = new Bitmap(panel2.Width, panel2.Height);
            panel2.DrawToBitmap(panelImage, panel2.ClientRectangle);

            if (fileCounter == 2)
            {

                e.Graphics.DrawString("Result", font, Brushes.Black, x, y);
                y += lineHeight;
                lineHeight = font.GetHeight(e.Graphics);

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Model: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                e.Graphics.DrawString(toolStripComboBox1.Text, font, Brushes.Black, x + 230, y);
                y += lineHeight;

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Number of current sources: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                e.Graphics.DrawString(Convert.ToString(modelFileHeader.nSource), font, Brushes.Black, x + 230, y);
                y += lineHeight;

                font = new Font("Arial", 12, FontStyle.Bold);
                e.Graphics.DrawString("Current intensity: ", font, Brushes.Black, x, y);
                font = new Font("Arial", 12);
                y += lineHeight;
                e.Graphics.DrawString("Location" + "\t\t" + "Current (mA)", font, Brushes.Black, x + 230, y);
                y += lineHeight;
                for (int i = 0; i < dataGridView1.RowCount; ++i)
                {
                    e.Graphics.DrawString(Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).ToUpper() + "\t\t" + Convert.ToString(dataGridView1.Rows[i].Cells[1].Value), font, Brushes.Black, x + 230, y);
                    y += lineHeight;
                }

                y += lineHeight;
            }
            if (fileCounter == 1)
            {
                if (panelImage.Width / W < panelImage.Height / H)
                    W = panelImage.Width * H / panelImage.Height;
                else
                    H = panelImage.Height * W / panelImage.Width;
                e.Graphics.DrawImage(panelImage, 20, 20, W, H);
            }

            fileCounter -= 1;
            if (fileCounter > 0)
                e.HasMorePages = true;
            else
                fileCounter = toPrint.Length - 1;

            buttonMRI.Visible = true;
            buttonOverlay.Visible = true;
            buttonField.Visible = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            toolTipDataGridView1.Active = false;
            toolTipDataGridView1.Active = true;
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewCellValueChangedRows = e.RowIndex;
            if (dataGridView1.Rows.Count > 1)
            {
                double count = 0;
                for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
                {
                    if (IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value)))
                    {
                        dataGridView2.Rows[i].Cells[1].Value = dataGridView1.Rows[i].Cells[1].Value;
                        count = count + Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                    }
                    else if (!IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value)) && Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() != "" && Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() != "(REFERENCE)")
                    {
                        count = Math.Sqrt(-1);                         
                        MessageBox.Show("Not a valid entry");
                        dataGridView1.Rows[i].Cells[1].Value = "";
                        dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[1].Value = "(REFERENCE)";                      
                        return;
                    }
                    else
                        count = Math.Sqrt(-1);
                    isElectrode = false;
                    for (int j = 0; j < modelFileHeader.nElectrode; ++j)
                    {
                        if (dataGridView1.Rows[i].Cells[0].Value != null && dataGridView1.Rows[i].Cells[0].Value.ToString().ToUpper().Trim() == biosemiLoc.channelName[j].ToUpper().Trim())
                        {
                            isElectrode = true;
                            break;
                        }
                    }
                    if (!isElectrode && Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).Trim() != "")
                    {
                        MessageBox.Show("Not a valid entry");
                        dataGridView1.Rows[i].Cells[0].Value = "";
                        return;
                    }
                }

                if (!double.IsNaN(count))
                {
                    dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = count * (-1);
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = count * (-1);
                }
                
                currentMax = 0;
                currentMin = 0;
                getCurrentIntensity();
                if (currentValueChanged)
                {
                    showTopplot();
                    if (currentTabIndex == 1)
                        pictureBox1.Enabled = true;
                }
                selectElectrode = true;
                panel2.Invalidate();

                if (CheckDataGridViewAllEntered())
                    button1.Enabled = true;
            }
        }

        private bool CheckDataGridViewAllEntered()
        {
            bool allEntered = true;
            if (dataGridView1.Rows.Count > 1)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    if ((i < dataGridView1.Rows.Count - 1) && !IsNumber(Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim()))
                    {
                        allEntered = false;
                        break;
                    }
                    if (Convert.ToString(dataGridView1.Rows[i].Cells[1].Value).Trim() == "" || Convert.ToString(dataGridView1.Rows[i].Cells[0].Value).Trim() == "")
                    {
                        allEntered = false;
                        break;
                    }
                }
            }
            else
                allEntered = false;
            return allEntered && !isRepeated;
        }

        private void buttonField_Click(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = null;
            pictureBox2.BackgroundImage = null;
            pictureBox3.BackgroundImage = null;
            showField = true;
            showMRI = false;
            showOverlay = false;
            alpha = 255;
            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
        }

        private void buttonOverlay_Click(object sender, EventArgs e)
        {
            showOverlay = true;
            showMRI = false;
            showField = false;
            alpha = 255;
            showBackgroundImage = true;
            showImage(mri, colormapGray, 0.0, mriMax);      
            alpha = overlayAlpha;
            showBackgroundImage = false;
            showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
        }

        private void buttonMRI_Click(object sender, EventArgs e)
        {
            showMRI = true;
            showOverlay = false;
            showField = false;
            alpha = 255;
            showImage(mri, colormapGray, 0.0, mriMax);
        }

        //find percentile using c# writen by jibin 
        //Array sholud be sorted one
        //Count is the no of elements in array
        //Statistical_Level/Percentile level should between 0 - 99
        public double Leq_Stat(double[] InputNos, int count, double Statistical_Level)
        {
            double returnResult = 0;
            try
            {
                #region From the soted array calculate the percentile
                bool valid_Statistical_Level = true;
                double _percentilevalue = 0;

                if ((0 < Statistical_Level) && (Statistical_Level < 1))
                {
                    _percentilevalue = Statistical_Level;
                }
                else if ((1 <= Statistical_Level) && (Statistical_Level <= 99))
                {
                    _percentilevalue = 1 - (Statistical_Level / 100);
                }
                else
                    valid_Statistical_Level = false;

                if (valid_Statistical_Level)
                {
                    double allindex = 0, floatval = 0;
                    int intvalindex = 0;
                    double _percentile = 0;

                    allindex = (count - 1) * _percentilevalue;
                    intvalindex = Convert.ToInt32(allindex);
                    floatval = allindex - intvalindex;

                    //to check floatval is float itself or an integer 
                    double diff = floatval - Convert.ToInt32(floatval);

                    if (diff == 0)
                    {
                        _percentile = InputNos[intvalindex];
                    }
                    else
                    {
                        if (count > intvalindex + 1)
                            _percentile = floatval * (InputNos[intvalindex + 1] - InputNos[intvalindex]) + InputNos[intvalindex];
                        else
                            _percentile = InputNos[intvalindex];
                    }

                    //   _percentile=Math.Round(_percentile, 3);

                    returnResult = _percentile;
                }
                else
                    returnResult = 0;// "Invalid No As Statistical Level";


                #endregion
            }
            catch (Exception e1)
            {
                returnResult = 0;
            }

            return returnResult;
        }

        //http://en.wikipedia.org/wiki/Percentile
        public double percentile(double[] sequence, double excelPercentile)
        {
            // excelPercentile between [0 1]
            Array.Sort(sequence);
            int N = sequence.Length;
            double n = (N - 1) * excelPercentile + 1;
            // Another method: double n = (N + 1) * excelPercentile;
            if (n == 1d) return sequence[0];
            else if (n == N) return sequence[N - 1];
            else
            {
                int k = (int)n;
                double d = n - k;
                return sequence[k - 1] + d * (sequence[k] - sequence[k - 1]);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            PrintPreviewDialog previewdlg = new PrintPreviewDialog();
            doc.PrintPage += new PrintPageEventHandler(Doc_PrintPage);

            PrintDialog dlgSettings = new PrintDialog();
            dlgSettings.Document = doc;
            dlgSettings.UseEXDialog = true;
            fileCounter = 2;
            if (dlgSettings.ShowDialog() == DialogResult.OK)
                doc.Print();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap panelImage = new Bitmap(panel2.Width-1, panel2.Height-1, PixelFormat.Format32bppArgb);

            Graphics graphics = Graphics.FromImage(panelImage as Image);

            Point location = panel2.PointToScreen(Point.Empty);
            graphics.CopyFromScreen(location.X-1, location.Y-1, 0, 0, panelImage.Size, CopyPixelOperation.SourceCopy);

            // Displays a SaveFileDialog so the user can save the Image assigned to buttonSave.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "BITMAP|*.bmp|JPEG|*.jpg|GIF|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            buttonMRI.Visible = false;
            buttonOverlay.Visible = false;
            buttonField.Visible = false;
            panel2.BorderStyle = BorderStyle.None;

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 2:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 3:
                        panelImage.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }
                fs.Close();
            }
            buttonMRI.Visible = true;
            buttonOverlay.Visible = true;
            buttonField.Visible = true;
            panel2.BorderStyle = BorderStyle.Fixed3D;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutHDExploreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            about = new About();
            about.Show();
        }

        private void item22ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = item22ToolStripMenuItem.Text;
        }

        private void item21ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = item21ToolStripMenuItem.Text;
        }

        private void item23ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = item23ToolStripMenuItem.Text;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void domainUpDown1_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown1.Text.Trim()))
            {
                if (Convert.ToInt16(domainUpDown1.Text.Trim()) > 0 && Convert.ToInt16(domainUpDown1.Text.Trim()) <= modelFileHeader.x)
                {
                    position[0] = Convert.ToInt16(domainUpDown1.Text.Trim()) - 1;
                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown1.Text) <= 0)
                {
                    domainUpDown1.SelectedIndex = domainUpDown1.Items.Count - 1;
                }
                else if (Convert.ToInt32(domainUpDown1.Text) > modelFileHeader.x)
                {
                    domainUpDown1.SelectedIndex = 0;
                }
            }
        }

        private void domainUpDown2_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown2.Text.Trim()))
            {
                if (Convert.ToInt32(domainUpDown2.Text.Trim()) > 0 && Convert.ToInt32(domainUpDown2.Text.Trim()) <= modelFileHeader.y)
                {
                    position[1] = Convert.ToInt16(domainUpDown2.Text.Trim()) - 1;

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown2.Text) <= 0)
                {
                    domainUpDown2.SelectedIndex = domainUpDown2.Items.Count-1;
                }
                else if (Convert.ToInt32(domainUpDown2.Text) > modelFileHeader.y)
                {
                    domainUpDown2.SelectedIndex = 0;
                }

            }
        }

        private void domainUpDown3_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true && IsNumber(domainUpDown3.Text.Trim()))
            {
                if (Convert.ToInt16(domainUpDown3.Text.Trim()) > 0 && Convert.ToInt16(domainUpDown3.Text.Trim()) <= modelFileHeader.z)
                {
                    position[2] = Convert.ToInt16(domainUpDown3.Text.Trim()) - 1;

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else if (Convert.ToInt32(domainUpDown3.Text) <= 0)
                {
                    domainUpDown3.SelectedIndex = domainUpDown3.Items.Count - 1;
                }
                else if (Convert.ToInt32(domainUpDown3.Text) > modelFileHeader.z)
                {
                    domainUpDown3.SelectedIndex = 0;
                }
            }
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            Rectangle mouseRect = new Rectangle(e.X, e.Y, 1, 1);
            for (int i = 0; i < tabControl1.TabCount; i++)
            {
                if (tabControl1.GetTabRect(i).IntersectsWith(mouseRect))
                {
                    currentTabIndex = i;
                    break;
                }
            }

            if (currentTabIndex == 2 && groupBox1.Visible == false)
            {
                dataGridView1.Size = new Size(dataGridView1.Width, textBox2.Location.Y - dataGridView1.Location.Y - 10);
                groupBox2.Location = new Point(0, textBox3.Location.Y + textBox3.Height);
                groupBox4.Location = new Point(0, groupBox2.Location.Y + groupBox2.Height);
                groupBox5.Location = new Point(0, textBox4.Location.Y - groupBox5.Height - 3);
                groupBox4.Size = new Size(groupBox4.Width, groupBox5.Location.Y - groupBox4.Location.Y);

                dataGridView2.Size = new Size(dataGridView2.Width, groupBox4.Height - label4.Height - 40);

                if (CheckDataGridViewAllEntered())
                    button1.Enabled = true;
                pictureBox1.Enabled = false;
            }
            if (currentTabIndex == 1 && toolStripComboBox1.Text != "Subject")
            {
                if (tabControl1.SelectedIndex != 1)
                {
                    if (groupBox1.Visible == true)
                    {
                        tabControl1.TabPages[2].Focus();
                        tabControl1.SelectTab(tabPage3);
                        newSessionWindow = new newSession();
                        newSessionWindow.ShowDialog();
                        bool startNewSession = newSessionWindow.getSession();
                        if (startNewSession)
                        {
                            initialGUIState();
                            panel2.BackgroundImage = null;
                            pictureBox2BackgroundImage1 = null;
                            getCurrentIntensity();
                            showTopplot();
                            selectElectrode = true;
                            if (IsNumber(toolStripComboBox2.Text.Trim()))
                                pictureBox1.Enabled = true;
                            panel2.Invalidate();
                            tabControl1.SelectTab(tabPage2);
                        }
                    }
                    else
                    {
                        getCurrentIntensity();
                        colorbarWidth = 30;
                        showTopplot();
                        selectElectrode = true;
                        if (IsNumber(toolStripComboBox2.Text.Trim()))
                            pictureBox1.Enabled = true;
                        panel2.Invalidate();
                        tabControl1.SelectTab(tabPage2);
                    }
                }
            }

            if (currentTabIndex == 0 )
            {
                pictureBox1.Enabled = false;
                if (groupBox1.Visible)
                {
                    if (tabControl1.SelectedIndex != 0)
                    {
                        tabControl1.TabPages[2].Focus();
                        tabControl1.SelectTab(tabPage3);
                        newSessionWindow = new newSession();
                        newSessionWindow.ShowDialog();
                        bool startNewSession = newSessionWindow.getSession();
                        if (startNewSession)
                        {
                            initialGUIState();
                            pictureBox2BackgroundImage1 = null;
                            showElectrodeLocation();
                            tabControl1.SelectTab(tabPage1);
                        }
                    }
                }
            }
        }

        private void domainUpDown5_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled == true)
            {
                if (domainUpDown5.SelectedIndex > domainUpDown4.SelectedIndex) //if-else: in case not readonly
                {
                    electricFieldColormapMin = Convert.ToDouble(domainUpDown5.Items[domainUpDown5.SelectedIndex]);

                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else
                {
                    for (int i = 0; i < domainUpDown5.Items.Count; ++i)
                        if (Math.Round(Convert.ToDouble(domainUpDown5.Items[i]), 2) == Math.Round(electricFieldColormapMin, 2))
                        {
                            domainUpDown5.SelectedIndex = i;
                            break;
                        }
                }
            }
        }

        private void domainUpDown4_TextChanged(object sender, EventArgs e)
        {
            if (printToolStripMenuItem.Enabled)
            {
                if (domainUpDown5.SelectedIndex > domainUpDown4.SelectedIndex)
                {
                    electricFieldColormapMax = Convert.ToDouble(domainUpDown4.Items[domainUpDown4.SelectedIndex]);
                    if (showField)
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    else if (showMRI)
                        showImage(mri, colormapGray, 0.0, mriMax);
                    else if (showOverlay)
                    {
                        alpha = 255;
                        showBackgroundImage = true;
                        showImage(mri, colormapGray, 0.0, mriMax);
                        alpha = overlayAlpha;
                        showBackgroundImage = false;
                        showImage(electricField3D, colormapJet, electricFieldColormapMin, electricFieldColormapMax);
                    }
                }
                else
                {
                    for (int i = 0; i < domainUpDown4.Items.Count; ++i)
                        if (Math.Round(Convert.ToDouble(domainUpDown4.Items[i]), 2) == Math.Round(electricFieldColormapMax, 2))
                        {
                            domainUpDown4.SelectedIndex = i;
                            break;
                        }
                }
            }
        }

        private void domainUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void domainUpDown2_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void domainUpDown3_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void domainUpDown4_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void domainUpDown5_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }

        private void Adult_male_1_93_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_1_93_electrode_model.Name;
        }

        private void Adult_male_1_332_electrode_model_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Text = Adult_male_1_332_electrode_model.Name;
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount != 0 && dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[1].ToString().Trim() != "")
            {
                initialGUIState();
                panel2.BackgroundImage = null;
            }

            if (IsNumber(toolStripComboBox2.Text.Trim()))
            {
                if (currentTabIndex == 1)
                    pictureBox1.Enabled = true;
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                for (int j = 0; j < Convert.ToInt16(toolStripComboBox2.Text); j++)
                {
                    dataGridView1.Rows.Add(" ", " ");
                    dataGridView2.Rows.Add(" ", " ");
                }
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = "(REFERENCE)";
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = "(REFERENCE)";
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].ReadOnly = true;
                modelFileHeader.nSource = Convert.ToInt32(toolStripComboBox2.Text.ToString()) - 1;
                button1.Enabled = false;
                textBox8.Text = toolStripComboBox2.Text;
                selectElectrode = true;

                currentMax = Convert.ToDouble(2.0);
                currentMin = -currentMax;
                pictureBox2BackgroundImage1 = null;
                getCurrentIntensity();
                showTopplot();
                panel2.Invalidate();
            }
            else
            {
                MessageBox.Show("Not a valid entry");
                if (dataGridView1.Rows.Count > 1)
                    toolStripComboBox2.SelectedIndex = dataGridView1.Rows.Count - 2;
            }
        }
    }
}
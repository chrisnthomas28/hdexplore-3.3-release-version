﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HDExplore
{
    public partial class LoadData : Form
    {
        //progressBar1.Minimum = 0;
        //progressBar1.Maximum = x*y*z-1;
        public LoadData(int nCurrentSource)
        {
            InitializeComponent();
            progressBar1.Minimum = 0;
            //progressBar1.Maximum = nCurrentSource;
        }

        public void updateProgressBar(int n)
        {
            progressBar1.Value = n;
        }

        public void setProgressBarMaximum(int max)
        {
            progressBar1.Maximum = max;
        }

        //disable the Close button on the form
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
    }
}

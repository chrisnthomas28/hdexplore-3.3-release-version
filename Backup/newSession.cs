﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
//using System.Text;

namespace HDExplore
{
    public partial class newSession : Form
    {
        private bool startNewSession;
        public newSession()
        {
            InitializeComponent();
          
            ReadXMLDocument();
        }

        private void ReadXMLDocument()
        {
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };

            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            //XmlTextReader reader = new XmlTextReader("../../SoterixMedical_configuration.xml");

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        
                        switch (reader.Name)
                        {
                            case "newSession":
                                {
                                    string newSessionWarningMessage = reader.GetAttribute("newSessionWarningMessage");
                                    labelMessage.Text = newSessionWarningMessage;
                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            startNewSession = true;
            this.Close();
        }

        private void buttonDecline_Click(object sender, EventArgs e)
        {
            startNewSession = false;
            this.Close();
        }

        public bool getSession()
        {
            return startNewSession;
        }
    }
}

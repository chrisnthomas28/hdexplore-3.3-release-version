﻿namespace HDExplore
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.linkLabelWebLink = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.textBoxRef = new System.Windows.Forms.TextBox();
            this.linkReference1 = new System.Windows.Forms.LinkLabel();
            this.linkReference2 = new System.Windows.Forms.LinkLabel();
            this.labelReferences = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 77);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVersion.Location = new System.Drawing.Point(577, 0);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(45, 13);
            this.labelVersion.TabIndex = 1;
            this.labelVersion.Text = "Version:";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(553, 251);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(61, 24);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // linkLabelWebLink
            // 
            this.linkLabelWebLink.AutoSize = true;
            this.linkLabelWebLink.Location = new System.Drawing.Point(158, 250);
            this.linkLabelWebLink.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabelWebLink.Name = "linkLabelWebLink";
            this.linkLabelWebLink.Size = new System.Drawing.Size(46, 13);
            this.linkLabelWebLink.TabIndex = 4;
            this.linkLabelWebLink.TabStop = true;
            this.linkLabelWebLink.Text = "web link";
            this.linkLabelWebLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelWebLink_LinkClicked);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 91);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 145);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(152, 29);
            this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(155, 26);
            this.labelName.TabIndex = 7;
            this.labelName.Text = "software name";
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.BackColor = System.Drawing.Color.White;
            this.textBoxMessage.Location = new System.Drawing.Point(160, 91);
            this.textBoxMessage.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMessage.Size = new System.Drawing.Size(454, 48);
            this.textBoxMessage.TabIndex = 8;
            // 
            // textBoxRef
            // 
            this.textBoxRef.BackColor = System.Drawing.Color.White;
            this.textBoxRef.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRef.Location = new System.Drawing.Point(160, 157);
            this.textBoxRef.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxRef.Multiline = true;
            this.textBoxRef.Name = "textBoxRef";
            this.textBoxRef.ReadOnly = true;
            this.textBoxRef.Size = new System.Drawing.Size(434, 91);
            this.textBoxRef.TabIndex = 9;
            // 
            // linkReference1
            // 
            this.linkReference1.AutoSize = true;
            this.linkReference1.Location = new System.Drawing.Point(585, 156);
            this.linkReference1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkReference1.Name = "linkReference1";
            this.linkReference1.Size = new System.Drawing.Size(29, 13);
            this.linkReference1.TabIndex = 10;
            this.linkReference1.TabStop = true;
            this.linkReference1.Text = "link1";
            this.linkReference1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkReference1_LinkClicked);
            // 
            // linkReference2
            // 
            this.linkReference2.AutoSize = true;
            this.linkReference2.Location = new System.Drawing.Point(585, 196);
            this.linkReference2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkReference2.Name = "linkReference2";
            this.linkReference2.Size = new System.Drawing.Size(29, 13);
            this.linkReference2.TabIndex = 11;
            this.linkReference2.TabStop = true;
            this.linkReference2.Text = "link2";
            this.linkReference2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkReference2_LinkClicked);
            // 
            // labelReferences
            // 
            this.labelReferences.AutoSize = true;
            this.labelReferences.Location = new System.Drawing.Point(158, 142);
            this.labelReferences.Name = "labelReferences";
            this.labelReferences.Size = new System.Drawing.Size(65, 13);
            this.labelReferences.TabIndex = 12;
            this.labelReferences.Text = "References:";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(622, 284);
            this.Controls.Add(this.labelReferences);
            this.Controls.Add(this.linkReference2);
            this.Controls.Add(this.linkReference1);
            this.Controls.Add(this.textBoxRef);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.linkLabelWebLink);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.LinkLabel linkLabelWebLink;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.TextBox textBoxRef;
        private System.Windows.Forms.LinkLabel linkReference1;
        private System.Windows.Forms.LinkLabel linkReference2;
        private System.Windows.Forms.Label labelReferences;
    }
}
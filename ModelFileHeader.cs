﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDExplore
{
    public class ModelFileHeader
    {
        public struct DataHeader
        {
            // current
            public float version;
            public int nTargets;
            public string target;
            public int nOrientations;
            public string orientation;
            public double nCriteria;
            public string criteria;
            public int nCurrentSource;
            public int nSource;
            public int nElectrode;
            // dimensions of the mixing matrix
            public int x; //3D volume x
            public int y; //3D volume y
            public int z; //3D volume z
            public int r; // r = 3;
        };
    }
}

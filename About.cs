﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;

namespace HDExplore
{
    public partial class About : Form
    {
        private string webLink;
        private string reference1;
        private string reference2;
        private string referenceLink1;
        private string referenceLink2;

        public About()
        {
            InitializeComponent();
            labelName.Location = new Point(textBoxMessage.Location.X, 36);
            ReadXMLDocument();
        }

        private void ReadXMLDocument()
        {
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };
            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "aboutMessage":
                                {
                                    string aboutMessage = reader.GetAttribute("version");
                                    labelVersion.Text = "Version: " + aboutMessage;
                                    aboutMessage = reader.GetAttribute("message");
                                    reference1 = reader.GetAttribute("reference1");
                                    reference2 = reader.GetAttribute("reference2");
                                    referenceLink1 = reader.GetAttribute("referenceLink1");
                                    referenceLink2 = reader.GetAttribute("referenceLink2");
                                    webLink = reader.GetAttribute("webLink");
                                    labelName.Text = reader.GetAttribute("name");// +"\u2122";
                                    textBoxMessage.Text = aboutMessage;
                                    //labelMessage.Text = aboutMessage;
                                    linkLabelWebLink.Text = webLink;
                                    //linkLabelreference.Text = "Reference: \n" + reference;
                                    linkReference1.Text = "link";
                                    linkReference2.Text = "link";
                                    textBoxRef.Text = reference1 + Environment.NewLine + Environment.NewLine + reference2;

                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabelreference_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(referenceLink1);
        }

        private void linkReference1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(referenceLink1);
        }

        private void linkReference2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(referenceLink2);
        }

        private void linkLabelWebLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(webLink);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
//using System.Text;

namespace HDExplore
{
    public partial class agreement : Form
    {
        public agreement()
        {
            InitializeComponent();
          
            ReadXMLDocument();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            GUI.Form1 GUIForm = new GUI.Form1();
            GUIForm.FormClosed += new FormClosedEventHandler(GUIForm_FormClosed);
            GUIForm.Show();
            this.Hide();
        }

        private void GUIForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void ReadXMLDocument()
        {
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true };

            XmlReader reader = XmlReader.Create(@"SoterixMedical_HDExplore_configuration.xml", settings);
            //XmlTextReader reader = new XmlTextReader("../../SoterixMedical_configuration.xml");

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        
                        switch (reader.Name)
                        {
                            case "agreement":
                                {
                                    string agreementDialog = reader.GetAttribute("agreementDialog");
                                    labelMessage.Text = agreementDialog;
                                    break;
                                }
                        }
                        break;
                }
            }
        }

        private void buttonDecline_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

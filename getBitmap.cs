﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;

namespace GUI
{
    private Bitmap getBitmap(double[,] img, int height, int width)
        {
           
            byte[] buffer = new byte[height * width*4];
            int count=0;
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    for (int l = 0; l < 3; ++l)
                    {
                        buffer[count++] = (byte)img[i, j];
                    }
                    buffer[count++] = 255;
                }
            }
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
            bitmap.UnlockBits(bmpData);
           
            return bitmap;
             
            /*
            try
            {
                Bitmap image1 = new Bitmap(width, height);
                int oldy;
                int oldx;
                int hex;
                for (int x = 0; x < width; x++)
                {
                    oldx = (int)(Math.Round((double)x * img.GetLength(1) / width));
                    for (int y = 0; y < height; y++)
                    {
                        oldy = (int)(Math.Round((double)y * img.GetLength(0) / height));
                        hex = (int)Math.Round(img[oldy, oldx]);
                            image1.SetPixel(x, y, Color.FromArgb(hex, hex, hex, hex));
                    }
                }
                //image1.SetPixel(1, 1, Color.FromArgb(255, 0, 0));
                return image1;
            }
            catch (Exception e)
            {
                sw.WriteLine("Exception in getBitmap Function, Exception: " + e.Message.ToString() + e.Source.ToString() + e.TargetSite.ToString());
                return null;
            }
             */
            
        }
}

﻿using System.Drawing;

namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.adultHeadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Adult_male_1_93_electrode_model = new System.Windows.Forms.ToolStripMenuItem();
            this.Adult_male_1_332_electrode_model = new System.Windows.Forms.ToolStripMenuItem();
            this.item12ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Adult_male_2_93_electrode_model = new System.Windows.Forms.ToolStripMenuItem();
            this.adultMale3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Adult_male_3_93_electrode_model = new System.Windows.Forms.ToolStripMenuItem();
            this.item1dToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mni152toolstripmenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MNI_152_93_electrode_model = new System.Windows.Forms.ToolStripMenuItem();
            this.item2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_AH = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_DE = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_JC = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_JE = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_KS = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_LM = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_MC = new System.Windows.Forms.ToolStripMenuItem();
            this.Stroke_patient_RS = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton_cathode = new System.Windows.Forms.RadioButton();
            this.radioButton_anode = new System.Windows.Forms.RadioButton();
            this.groupBox_pads = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButton_electrodes = new System.Windows.Forms.RadioButton();
            this.radioButton_pads = new System.Windows.Forms.RadioButton();
            this.label_total_current = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton_flexible = new System.Windows.Forms.RadioButton();
            this.radioButton_uniform = new System.Windows.Forms.RadioButton();
            this.textBox_current = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Current = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonMRI = new System.Windows.Forms.Button();
            this.buttonField = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonOverlay = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAll3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coronalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sagittalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.axialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutHDExploreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTipRun = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipNumElectrode = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipVisualizationGUI = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipDataGridView1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipOpen = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTipTopplot = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipMaxCurrent = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipDataGridView2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_zoom = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_maxcurrent = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.domainUpDown5 = new System.Windows.Forms.DomainUpDown();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.domainUpDown3 = new System.Windows.Forms.DomainUpDown();
            this.domainUpDown2 = new System.Windows.Forms.DomainUpDown();
            this.labelFieldIntensity = new System.Windows.Forms.Label();
            this.domainUpDown4 = new System.Windows.Forms.DomainUpDown();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tissue_image = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox_pads.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tissue_image)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;// System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.pictureBox7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(222, 539);
            this.panel1.TabIndex = 30;
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.SystemColors.Menu; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(2, 131);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(220, 25);
            this.textBoxName.TabIndex = 32;
            this.textBoxName.TabStop = false;
            this.textBoxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(91, 40);
            this.tabControl1.Location = new System.Drawing.Point(0, 172);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(222, 367);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 31;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseDown);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.MenuBar;// System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.comboBox4);
            this.tabPage1.Controls.Add(this.comboBox3);
            this.tabPage1.Controls.Add(this.textBox6);
            this.tabPage1.Controls.Add(this.textBoxMessage);
            this.tabPage1.Controls.Add(this.toolStrip1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(214, 319);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Step 1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(51, 136);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 15);
            this.label18.TabIndex = 42;
            this.label18.Text = "Segmentation masks";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(130, 153);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 15);
            this.label17.TabIndex = 41;
            this.label17.Text = "Orientation:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 153);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 15);
            this.label12.TabIndex = 40;
            this.label12.Text = "Tissue:";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Front",
            "Right",
            "Left",
            "Top"});
            this.comboBox4.Location = new System.Drawing.Point(133, 172);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(65, 23);
            this.comboBox4.TabIndex = 14;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Skin",
            "Bone",
            "CSF",
            "Gray Matter",
            "White Matter"});
            this.comboBox3.Location = new System.Drawing.Point(14, 172);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(102, 23);
            this.comboBox3.TabIndex = 13;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Menu; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(0, 0);
            this.textBox6.Margin = new System.Windows.Forms.Padding(2);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(214, 20);
            this.textBox6.TabIndex = 12;
            this.textBox6.Text = "Select a subject";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.BackColor = System.Drawing.Color.White; // System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBoxMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMessage.Location = new System.Drawing.Point(0, 247);
            this.textBoxMessage.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMessage.Size = new System.Drawing.Size(214, 72);
            this.textBoxMessage.TabIndex = 9;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(14, 66);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(62, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.AutoToolTip = false;
            this.toolStripComboBox1.BackColor = System.Drawing.SystemColors.Window; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.toolStripComboBox1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adultHeadToolStripMenuItem,
            this.item1dToolStripMenuItem,
            this.item2ToolStripMenuItem});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(59, 22);
            this.toolStripComboBox1.Text = "Subject";
            this.toolStripComboBox1.TextChanged += new System.EventHandler(this.toolStripComboBox1_TextChanged);
            // 
            // adultHeadToolStripMenuItem
            // 
            this.adultHeadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item11ToolStripMenuItem,
            this.item12ToolStripMenuItem,
            this.adultMale3ToolStripMenuItem});
            this.adultHeadToolStripMenuItem.Name = "adultHeadToolStripMenuItem";
            this.adultHeadToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.adultHeadToolStripMenuItem.Text = "Adult Head";
            // 
            // item11ToolStripMenuItem
            // 
            this.item11ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Adult_male_1_93_electrode_model,
            this.Adult_male_1_332_electrode_model});
            this.item11ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.item11ToolStripMenuItem.Name = "item11ToolStripMenuItem";
            this.item11ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.item11ToolStripMenuItem.Text = "Adult male 1";
            // 
            // Adult_male_1_93_electrode_model
            // 
            this.Adult_male_1_93_electrode_model.Name = "Adult_male_1_93_electrode_model";
            this.Adult_male_1_93_electrode_model.Size = new System.Drawing.Size(183, 22);
            this.Adult_male_1_93_electrode_model.Text = "93-electrode model";
            this.Adult_male_1_93_electrode_model.Click += new System.EventHandler(this.Adult_male_1_93_electrode_model_Click);
            // 
            // Adult_male_1_332_electrode_model
            // 
            this.Adult_male_1_332_electrode_model.Name = "Adult_male_1_332_electrode_model";
            this.Adult_male_1_332_electrode_model.Size = new System.Drawing.Size(183, 22);
            this.Adult_male_1_332_electrode_model.Text = "332-electrode model";
            this.Adult_male_1_332_electrode_model.Click += new System.EventHandler(this.Adult_male_1_332_electrode_model_Click);
            // 
            // item12ToolStripMenuItem
            // 
            this.item12ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Adult_male_2_93_electrode_model});
            this.item12ToolStripMenuItem.Name = "item12ToolStripMenuItem";
            this.item12ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.item12ToolStripMenuItem.Text = "Adult male 2";
            // 
            // Adult_male_2_93_electrode_model
            // 
            this.Adult_male_2_93_electrode_model.Name = "Adult_male_2_93_electrode_model";
            this.Adult_male_2_93_electrode_model.Size = new System.Drawing.Size(177, 22);
            this.Adult_male_2_93_electrode_model.Text = "93-electrode model";
            this.Adult_male_2_93_electrode_model.Click += new System.EventHandler(this.Adult_male_2_93_electrode_model_Click);
            // 
            // adultMale3ToolStripMenuItem
            // 
            this.adultMale3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Adult_male_3_93_electrode_model});
            this.adultMale3ToolStripMenuItem.Name = "adultMale3ToolStripMenuItem";
            this.adultMale3ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.adultMale3ToolStripMenuItem.Text = "Adult male 3";
            // 
            // Adult_male_3_93_electrode_model
            // 
            this.Adult_male_3_93_electrode_model.Name = "Adult_male_3_93_electrode_model";
            this.Adult_male_3_93_electrode_model.Size = new System.Drawing.Size(177, 22);
            this.Adult_male_3_93_electrode_model.Text = "93-electrode model";
            this.Adult_male_3_93_electrode_model.Click += new System.EventHandler(this.Adult_male_3_93_electrode_model_Click);
            // 
            // item1dToolStripMenuItem
            // 
            this.item1dToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mni152toolstripmenu});
            this.item1dToolStripMenuItem.Name = "item1dToolStripMenuItem";
            this.item1dToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.item1dToolStripMenuItem.Text = "Standard Head";
            this.item1dToolStripMenuItem.Click += new System.EventHandler(this.item1dToolStripMenuItem_Click);
            // 
            // mni152toolstripmenu
            // 
            this.mni152toolstripmenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MNI_152_93_electrode_model});
            this.mni152toolstripmenu.Name = "mni152toolstripmenu";
            this.mni152toolstripmenu.Size = new System.Drawing.Size(118, 22);
            this.mni152toolstripmenu.Text = "MNI 152";
            // 
            // MNI_152_93_electrode_model
            // 
            this.MNI_152_93_electrode_model.Name = "MNI_152_93_electrode_model";
            this.MNI_152_93_electrode_model.Size = new System.Drawing.Size(177, 22);
            this.MNI_152_93_electrode_model.Text = "93-electrode model";
            this.MNI_152_93_electrode_model.Click += new System.EventHandler(this.MNI_152_93_electrode_model_Click);
            // 
            // item2ToolStripMenuItem
            // 
            this.item2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Stroke_patient_AH,
            this.Stroke_patient_DE,
            this.Stroke_patient_JC,
            this.Stroke_patient_JE,
            this.Stroke_patient_KS,
            this.Stroke_patient_LM,
            this.Stroke_patient_MC,
            this.Stroke_patient_RS});
            this.item2ToolStripMenuItem.Name = "item2ToolStripMenuItem";
            this.item2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.item2ToolStripMenuItem.Text = "Stroke Patient";
            // 
            // Stroke_patient_AH
            // 
            this.Stroke_patient_AH.Name = "Stroke_patient_AH";
            this.Stroke_patient_AH.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_AH.Text = "Patient 1";
            // 
            // Stroke_patient_DE
            // 
            this.Stroke_patient_DE.Name = "Stroke_patient_DE";
            this.Stroke_patient_DE.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_DE.Text = "Patient 2";
            // 
            // Stroke_patient_JC
            // 
            this.Stroke_patient_JC.Name = "Stroke_patient_JC";
            this.Stroke_patient_JC.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_JC.Text = "Patient 3";
            // 
            // Stroke_patient_JE
            // 
            this.Stroke_patient_JE.Name = "Stroke_patient_JE";
            this.Stroke_patient_JE.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_JE.Text = "Patient 4";
            // 
            // Stroke_patient_KS
            // 
            this.Stroke_patient_KS.Name = "Stroke_patient_KS";
            this.Stroke_patient_KS.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_KS.Text = "Patient 5";
            this.Stroke_patient_KS.Click += new System.EventHandler(this.Stroke_patient_KS_Click);
            // 
            // Stroke_patient_LM
            // 
            this.Stroke_patient_LM.Name = "Stroke_patient_LM";
            this.Stroke_patient_LM.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_LM.Text = "Patient 6";
            // 
            // Stroke_patient_MC
            // 
            this.Stroke_patient_MC.Name = "Stroke_patient_MC";
            this.Stroke_patient_MC.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_MC.Text = "Patient 7";
            this.Stroke_patient_MC.Click += new System.EventHandler(this.Stroke_patient_MC_Click);
            // 
            // Stroke_patient_RS
            // 
            this.Stroke_patient_RS.Name = "Stroke_patient_RS";
            this.Stroke_patient_RS.Size = new System.Drawing.Size(120, 22);
            this.Stroke_patient_RS.Text = "Patient 8";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.MenuBar;// System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox_pads);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.label_total_current);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.textBox_current);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.toolStrip4);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 44);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(214, 319);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Step 2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton_cathode);
            this.groupBox3.Controls.Add(this.radioButton_anode);
            this.groupBox3.Location = new System.Drawing.Point(21, 99);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(187, 38);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            // 
            // radioButton_cathode
            // 
            this.radioButton_cathode.Location = new System.Drawing.Point(93, 13);
            this.radioButton_cathode.Name = "radioButton_cathode";
            this.radioButton_cathode.Size = new System.Drawing.Size(91, 17);
            this.radioButton_cathode.TabIndex = 0;
            this.radioButton_cathode.TabStop = true;
            this.radioButton_cathode.Text = "Add cathodes";
            this.radioButton_cathode.UseVisualStyleBackColor = true;
            this.radioButton_cathode.CheckedChanged += new System.EventHandler(this.radioButton_cathode_CheckedChanged);
            // 
            // radioButton_anode
            // 
            this.radioButton_anode.AutoSize = true;
            this.radioButton_anode.Location = new System.Drawing.Point(6, 13);
            this.radioButton_anode.Name = "radioButton_anode";
            this.radioButton_anode.Size = new System.Drawing.Size(82, 17);
            this.radioButton_anode.TabIndex = 44;
            this.radioButton_anode.TabStop = true;
            this.radioButton_anode.Text = "Add anodes";
            this.radioButton_anode.UseVisualStyleBackColor = true;
            this.radioButton_anode.CheckedChanged += new System.EventHandler(this.radioButton_anode_CheckedChanged);
            // 
            // groupBox_pads
            // 
            this.groupBox_pads.Controls.Add(this.label16);
            this.groupBox_pads.Controls.Add(this.label1);
            this.groupBox_pads.Controls.Add(this.comboBox1);
            this.groupBox_pads.Controls.Add(this.comboBox2);
            this.groupBox_pads.Location = new System.Drawing.Point(18, 191);
            this.groupBox_pads.Name = "groupBox_pads";
            this.groupBox_pads.Size = new System.Drawing.Size(187, 38);
            this.groupBox_pads.TabIndex = 53;
            this.groupBox_pads.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(89, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 13);
            this.label16.TabIndex = 54;
            this.label16.Text = "Cathode:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Anode:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(42, 11);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(45, 21);
            this.comboBox1.TabIndex = 51;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.comboBox1_SelectionChangeCommitted);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(140, 11);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(45, 21);
            this.comboBox2.TabIndex = 52;
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.radioButton_electrodes);
            this.panel5.Controls.Add(this.radioButton_pads);
            this.panel5.Location = new System.Drawing.Point(24, 54);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(156, 50);
            this.panel5.TabIndex = 46;
            // 
            // radioButton_electrodes
            // 
            this.radioButton_electrodes.AutoSize = true;
            this.radioButton_electrodes.Location = new System.Drawing.Point(3, 0);
            this.radioButton_electrodes.Name = "radioButton_electrodes";
            this.radioButton_electrodes.Size = new System.Drawing.Size(153, 17);
            this.radioButton_electrodes.TabIndex = 41;
            this.radioButton_electrodes.TabStop = true;
            this.radioButton_electrodes.Text = "Choose arbitrary electrodes";
            this.radioButton_electrodes.UseVisualStyleBackColor = true;
            this.radioButton_electrodes.CheckedChanged += new System.EventHandler(this.radioButton_electrodes_CheckedChanged);
            // 
            // radioButton_pads
            // 
            this.radioButton_pads.Location = new System.Drawing.Point(3, 16);
            this.radioButton_pads.Name = "radioButton_pads";
            this.radioButton_pads.Size = new System.Drawing.Size(153, 30);
            this.radioButton_pads.TabIndex = 42;
            this.radioButton_pads.TabStop = true;
            this.radioButton_pads.Text = "Choose pad configurations ( tDCSExplore)";
            this.radioButton_pads.UseVisualStyleBackColor = true;
            this.radioButton_pads.CheckedChanged += new System.EventHandler(this.radioButton_pads_CheckedChanged);
            // 
            // label_total_current
            // 
            this.label_total_current.AutoSize = true;
            this.label_total_current.Location = new System.Drawing.Point(25, 38);
            this.label_total_current.Name = "label_total_current";
            this.label_total_current.Size = new System.Drawing.Size(143, 13);
            this.label_total_current.TabIndex = 50;
            this.label_total_current.Text = "Set total current (max. 2 mA):";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButton_flexible);
            this.panel3.Controls.Add(this.radioButton_uniform);
            this.panel3.Location = new System.Drawing.Point(24, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(156, 20);
            this.panel3.TabIndex = 45;
            // 
            // radioButton_flexible
            // 
            this.radioButton_flexible.AutoSize = true;
            this.radioButton_flexible.Location = new System.Drawing.Point(3, 0);
            this.radioButton_flexible.Name = "radioButton_flexible";
            this.radioButton_flexible.Size = new System.Drawing.Size(60, 17);
            this.radioButton_flexible.TabIndex = 41;
            this.radioButton_flexible.TabStop = true;
            this.radioButton_flexible.Text = "Flexible";
            this.radioButton_flexible.UseVisualStyleBackColor = true;
            this.radioButton_flexible.CheckedChanged += new System.EventHandler(this.radioButton_flexible_CheckedChanged);
            // 
            // radioButton_uniform
            // 
            this.radioButton_uniform.AutoSize = true;
            this.radioButton_uniform.Location = new System.Drawing.Point(87, 0);
            this.radioButton_uniform.Name = "radioButton_uniform";
            this.radioButton_uniform.Size = new System.Drawing.Size(61, 17);
            this.radioButton_uniform.TabIndex = 42;
            this.radioButton_uniform.TabStop = true;
            this.radioButton_uniform.Text = "Uniform";
            this.radioButton_uniform.UseVisualStyleBackColor = true;
            this.radioButton_uniform.CheckedChanged += new System.EventHandler(this.radioButton_uniform_CheckedChanged);
            // 
            // textBox_current
            // 
            this.textBox_current.Location = new System.Drawing.Point(168, 35);
            this.textBox_current.Name = "textBox_current";
            this.textBox_current.Size = new System.Drawing.Size(27, 19);
            this.textBox_current.TabIndex = 43;
            this.textBox_current.TextChanged += new System.EventHandler(this.textBox_current_TextChanged);
            this.textBox_current.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_current_KeyDown);
            this.textBox_current.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_current_KeyPress);
            this.textBox_current.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_current_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Number of electrodes:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(214, 20);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "Select electrodes and apply currents ";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(0, 247);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(214, 72);
            this.textBox2.TabIndex = 10;
            // 
            // toolStrip4
            // 
            this.toolStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip4.Enabled = false;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.toolStrip4.Location = new System.Drawing.Point(115, 46);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip4.Size = new System.Drawing.Size(80, 25);
            this.toolStrip4.TabIndex = 15;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.BackColor = System.Drawing.SystemColors.ButtonFace; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(75, 25);
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control; //System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Current});
            this.dataGridView1.Location = new System.Drawing.Point(21, 141);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 20;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.Size = new System.Drawing.Size(187, 88);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            this.dataGridView1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Location";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Current
            // 
            this.Current.HeaderText = "Current (mA)";
            this.Current.Name = "Current";
            this.Current.ReadOnly = true;
            this.Current.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.MenuBar;//System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Controls.Add(this.textBox4);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 44);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(214, 319);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Step 3";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.buttonMRI);
            this.groupBox5.Controls.Add(this.buttonField);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.buttonOverlay);
            this.groupBox5.Location = new System.Drawing.Point(0, 188);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox5.Size = new System.Drawing.Size(216, 110);
            this.groupBox5.TabIndex = 52;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Step 3";
            // 
            // buttonMRI
            // 
            this.buttonMRI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMRI.Location = new System.Drawing.Point(82, 60);
            this.buttonMRI.Margin = new System.Windows.Forms.Padding(2);
            this.buttonMRI.Name = "buttonMRI";
            this.buttonMRI.Size = new System.Drawing.Size(51, 24);
            this.buttonMRI.TabIndex = 43;
            this.buttonMRI.Text = "MRI";
            this.buttonMRI.UseVisualStyleBackColor = true;
            this.buttonMRI.Click += new System.EventHandler(this.buttonMRI_Click);
            // 
            // buttonField
            // 
            this.buttonField.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonField.Location = new System.Drawing.Point(132, 60);
            this.buttonField.Margin = new System.Windows.Forms.Padding(2);
            this.buttonField.Name = "buttonField";
            this.buttonField.Size = new System.Drawing.Size(51, 24);
            this.buttonField.TabIndex = 42;
            this.buttonField.Text = "Field";
            this.buttonField.UseVisualStyleBackColor = true;
            this.buttonField.Click += new System.EventHandler(this.buttonField_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(31, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Explore Current Flow in Head";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonOverlay
            // 
            this.buttonOverlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOverlay.Location = new System.Drawing.Point(32, 60);
            this.buttonOverlay.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOverlay.Name = "buttonOverlay";
            this.buttonOverlay.Size = new System.Drawing.Size(51, 24);
            this.buttonOverlay.TabIndex = 44;
            this.buttonOverlay.Text = "Overlay";
            this.buttonOverlay.UseVisualStyleBackColor = true;
            this.buttonOverlay.Click += new System.EventHandler(this.buttonOverlay_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGridView2);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.textBox8);
            this.groupBox4.Location = new System.Drawing.Point(0, 72);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(216, 131);
            this.groupBox4.TabIndex = 51;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Step 2";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control; // System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dataGridView2.Location = new System.Drawing.Point(16, 36);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 20;
            this.dataGridView2.ShowCellToolTips = false;
            this.dataGridView2.Size = new System.Drawing.Size(184, 74);
            this.dataGridView2.TabIndex = 49;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Location";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Current (mA)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Number of electrodes:";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(113, 15);
            this.textBox8.Margin = new System.Windows.Forms.Padding(2);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(100, 19);
            this.textBox8.TabIndex = 48;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Location = new System.Drawing.Point(0, 27);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(216, 34);
            this.groupBox2.TabIndex = 50;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Step 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Subject:";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(49, 12);
            this.textBox7.Margin = new System.Windows.Forms.Padding(2);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(164, 19);
            this.textBox7.TabIndex = 46;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Menu;// System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(208)))), ((int)(((byte)(234)))));
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(0, 0);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(214, 20);
            this.textBox3.TabIndex = 13;
            this.textBox3.Text = "Explore current flow in head ";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.White;
            this.textBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(0, 273);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox4.Size = new System.Drawing.Size(214, 46);
            this.textBox4.TabIndex = 12;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(2, 1);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(220, 129);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 30;
            this.pictureBox7.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(764, 24);
            this.menuStrip1.TabIndex = 46;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAll3ToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.printToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveAll3ToolStripMenuItem
            // 
            this.saveAll3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.coronalToolStripMenuItem,
            this.sagittalToolStripMenuItem,
            this.axialToolStripMenuItem});
            this.saveAll3ToolStripMenuItem.Name = "saveAll3ToolStripMenuItem";
            this.saveAll3ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveAll3ToolStripMenuItem.Text = "Save Images";
            // 
            // coronalToolStripMenuItem
            // 
            this.coronalToolStripMenuItem.Name = "coronalToolStripMenuItem";
            this.coronalToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.coronalToolStripMenuItem.Text = "Coronal";
            this.coronalToolStripMenuItem.Click += new System.EventHandler(this.coronalToolStripMenuItem_Click);
            // 
            // sagittalToolStripMenuItem
            // 
            this.sagittalToolStripMenuItem.Name = "sagittalToolStripMenuItem";
            this.sagittalToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.sagittalToolStripMenuItem.Text = "Sagittal";
            this.sagittalToolStripMenuItem.Click += new System.EventHandler(this.sagittalToolStripMenuItem_Click);
            // 
            // axialToolStripMenuItem
            // 
            this.axialToolStripMenuItem.Name = "axialToolStripMenuItem";
            this.axialToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.axialToolStripMenuItem.Text = "Axial";
            this.axialToolStripMenuItem.Click += new System.EventHandler(this.axialToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Enabled = false;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.AutoSize = false;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(137, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutHDExploreToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutHDExploreToolStripMenuItem
            // 
            this.aboutHDExploreToolStripMenuItem.Name = "aboutHDExploreToolStripMenuItem";
            this.aboutHDExploreToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.aboutHDExploreToolStripMenuItem.Text = "About HDExplore";
            this.aboutHDExploreToolStripMenuItem.Click += new System.EventHandler(this.aboutHDExploreToolStripMenuItem_Click);
            // 
            // toolTipRun
            // 
            this.toolTipRun.AutomaticDelay = 0;
            this.toolTipRun.AutoPopDelay = 32000;
            this.toolTipRun.InitialDelay = 0;
            this.toolTipRun.IsBalloon = true;
            this.toolTipRun.ReshowDelay = 0;
            this.toolTipRun.ShowAlways = true;
            // 
            // toolTipNumElectrode
            // 
            this.toolTipNumElectrode.AutomaticDelay = 0;
            this.toolTipNumElectrode.AutoPopDelay = 32000;
            this.toolTipNumElectrode.InitialDelay = 0;
            this.toolTipNumElectrode.IsBalloon = true;
            this.toolTipNumElectrode.ReshowDelay = 0;
            this.toolTipNumElectrode.ShowAlways = true;
            // 
            // toolTipVisualizationGUI
            // 
            this.toolTipVisualizationGUI.AutomaticDelay = 0;
            this.toolTipVisualizationGUI.AutoPopDelay = 32000;
            this.toolTipVisualizationGUI.InitialDelay = 0;
            this.toolTipVisualizationGUI.IsBalloon = true;
            this.toolTipVisualizationGUI.ReshowDelay = 0;
            this.toolTipVisualizationGUI.ShowAlways = true;
            // 
            // toolTipDataGridView1
            // 
            this.toolTipDataGridView1.AutoPopDelay = 32000;
            this.toolTipDataGridView1.InitialDelay = 500;
            this.toolTipDataGridView1.ReshowDelay = 100;
            this.toolTipDataGridView1.ShowAlways = true;
            // 
            // toolTipOpen
            // 
            this.toolTipOpen.AutomaticDelay = 0;
            this.toolTipOpen.AutoPopDelay = 32000;
            this.toolTipOpen.InitialDelay = 0;
            this.toolTipOpen.IsBalloon = true;
            this.toolTipOpen.ReshowDelay = 0;
            this.toolTipOpen.ShowAlways = true;
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 0;
            this.toolTip1.AutoPopDelay = 32000;
            this.toolTip1.InitialDelay = 0;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 0;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.toolTip1.UseFading = false;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(170, 24);
            this.toolStripMenuItem1.Text = "Standard head";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem6.Text = "Adult male 1";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Enabled = false;
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem7.Text = "Adult male 2";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Enabled = false;
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem8.Text = "Adult male 3";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem10,
            this.toolStripMenuItem11});
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(170, 24);
            this.toolStripMenuItem9.Text = "Stroke patient";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem10.Text = "Patient 1";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem11.Text = "Patient 2";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15});
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(170, 24);
            this.toolStripMenuItem12.Text = "Standard head";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem13.Text = "Adult male 1";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Enabled = false;
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem14.Text = "Adult male 2";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Enabled = false;
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem15.Text = "Adult male 3";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17,
            this.toolStripMenuItem18});
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(170, 24);
            this.toolStripMenuItem16.Text = "Stroke patient";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem17.Text = "Patient 1";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem18.Text = "Patient 2";
            // 
            // toolTipTopplot
            // 
            this.toolTipTopplot.AutomaticDelay = 0;
            this.toolTipTopplot.AutoPopDelay = 32000;
            this.toolTipTopplot.InitialDelay = 0;
            this.toolTipTopplot.IsBalloon = true;
            this.toolTipTopplot.ReshowDelay = 0;
            this.toolTipTopplot.ShowAlways = true;
            // 
            // toolTipMaxCurrent
            // 
            this.toolTipMaxCurrent.AutomaticDelay = 0;
            this.toolTipMaxCurrent.AutoPopDelay = 32000;
            this.toolTipMaxCurrent.InitialDelay = 0;
            this.toolTipMaxCurrent.IsBalloon = true;
            this.toolTipMaxCurrent.ReshowDelay = 0;
            this.toolTipMaxCurrent.ShowAlways = true;
            // 
            // toolTipDataGridView2
            // 
            this.toolTipDataGridView2.AutoPopDelay = 32000;
            this.toolTipDataGridView2.InitialDelay = 500;
            this.toolTipDataGridView2.ReshowDelay = 100;
            this.toolTipDataGridView2.ShowAlways = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox3.Location = new System.Drawing.Point(18, 286);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(192, 208);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseDown);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);
            this.pictureBox3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(19, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 208);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Coronal View";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(217, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Sagittal View";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 272);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Transeverse View";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(217, 54);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 32;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 293);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 33;
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Location = new System.Drawing.Point(470, 319);
            this.domainUpDown1.Margin = new System.Windows.Forms.Padding(2);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(40, 20);
            this.domainUpDown1.TabIndex = 50;
            this.domainUpDown1.TextChanged += new System.EventHandler(this.domainUpDown1_TextChanged);
            this.domainUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.domainUpDown1_KeyDown);
            this.domainUpDown1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.domainUpDown1_KeyPress);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox2.Location = new System.Drawing.Point(219, 61);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(153, 208);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
            // 
            // domainUpDown5
            // 
            this.domainUpDown5.Location = new System.Drawing.Point(470, 310);
            this.domainUpDown5.Margin = new System.Windows.Forms.Padding(2);
            this.domainUpDown5.Name = "domainUpDown5";
            this.domainUpDown5.Size = new System.Drawing.Size(18, 20);
            this.domainUpDown5.TabIndex = 54;
            this.domainUpDown5.Text = "0";
            this.domainUpDown5.TextChanged += new System.EventHandler(this.domainUpDown5_TextChanged);
            this.domainUpDown5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.domainUpDown5_KeyDown);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(380, 51);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(32, 208);
            this.pictureBox4.TabIndex = 34;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Location = new System.Drawing.Point(215, 275);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(207, 236);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(165, 11);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(32, 208);
            this.pictureBox6.TabIndex = 36;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(4, 11);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(153, 206);
            this.pictureBox5.TabIndex = 35;
            this.pictureBox5.TabStop = false;
            // 
            // domainUpDown3
            // 
            this.domainUpDown3.Location = new System.Drawing.Point(470, 444);
            this.domainUpDown3.Margin = new System.Windows.Forms.Padding(2);
            this.domainUpDown3.Name = "domainUpDown3";
            this.domainUpDown3.Size = new System.Drawing.Size(40, 20);
            this.domainUpDown3.TabIndex = 52;
            this.domainUpDown3.TextChanged += new System.EventHandler(this.domainUpDown3_TextChanged);
            this.domainUpDown3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.domainUpDown3_KeyDown);
            this.domainUpDown3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.domainUpDown3_KeyPress);
            // 
            // domainUpDown2
            // 
            this.domainUpDown2.Location = new System.Drawing.Point(470, 378);
            this.domainUpDown2.Margin = new System.Windows.Forms.Padding(2);
            this.domainUpDown2.Name = "domainUpDown2";
            this.domainUpDown2.Size = new System.Drawing.Size(40, 20);
            this.domainUpDown2.TabIndex = 51;
            this.domainUpDown2.TextChanged += new System.EventHandler(this.domainUpDown2_TextChanged);
            this.domainUpDown2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.domainUpDown2_KeyDown);
            this.domainUpDown2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.domainUpDown2_KeyPress);
            // 
            // labelFieldIntensity
            // 
            this.labelFieldIntensity.AutoSize = true;
            this.labelFieldIntensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFieldIntensity.Location = new System.Drawing.Point(364, 10);
            this.labelFieldIntensity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFieldIntensity.Name = "labelFieldIntensity";
            this.labelFieldIntensity.Size = new System.Drawing.Size(0, 15);
            this.labelFieldIntensity.TabIndex = 53;
            // 
            // domainUpDown4
            // 
            this.domainUpDown4.Location = new System.Drawing.Point(470, 75);
            this.domainUpDown4.Margin = new System.Windows.Forms.Padding(2);
            this.domainUpDown4.Name = "domainUpDown4";
            this.domainUpDown4.Size = new System.Drawing.Size(18, 20);
            this.domainUpDown4.TabIndex = 55;
            this.domainUpDown4.Text = "0";
            this.domainUpDown4.TextChanged += new System.EventHandler(this.domainUpDown4_TextChanged);
            this.domainUpDown4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.domainUpDown4_KeyDown);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox8.Location = new System.Drawing.Point(24, 70);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(250, 150);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 57;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.DoubleClick += new System.EventHandler(this.pictureBox8_DoubleClick);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.pictureBox8_MouseEnter);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 71;
            this.label13.Text = "Coronal Slice:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(217, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 72;
            this.label14.Text = "Sagittal Slice:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(83, 275);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 73;
            this.label15.Text = "Axial Slice:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(141, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 15);
            this.label11.TabIndex = 74;
            this.label11.Text = "label11";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.tissue_image);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.pictureBox8);
            this.panel2.Controls.Add(this.domainUpDown4);
            this.panel2.Controls.Add(this.labelFieldIntensity);
            this.panel2.Controls.Add(this.domainUpDown2);
            this.panel2.Controls.Add(this.domainUpDown3);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.domainUpDown5);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.domainUpDown1);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel2.Location = new System.Drawing.Point(222, 24);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(542, 539);
            this.panel2.TabIndex = 32;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label30.Location = new System.Drawing.Point(114, 500);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 17);
            this.label30.TabIndex = 87;
            this.label30.Text = "B";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label29.Location = new System.Drawing.Point(97, 275);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 17);
            this.label29.TabIndex = 86;
            this.label29.Text = "F";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label28.Location = new System.Drawing.Point(291, 264);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 17);
            this.label28.TabIndex = 85;
            this.label28.Text = "B";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label27.Location = new System.Drawing.Point(294, 54);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 17);
            this.label27.TabIndex = 84;
            this.label27.Text = "T";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label26.Location = new System.Drawing.Point(116, 274);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(17, 17);
            this.label26.TabIndex = 83;
            this.label26.Text = "B";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label25.Location = new System.Drawing.Point(107, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 17);
            this.label25.TabIndex = 82;
            this.label25.Text = "T";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label24.Location = new System.Drawing.Point(212, 362);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 17);
            this.label24.TabIndex = 81;
            this.label24.Text = "R";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label23.Location = new System.Drawing.Point(7, 353);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 17);
            this.label23.TabIndex = 80;
            this.label23.Text = "L";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label22.Location = new System.Drawing.Point(374, 133);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 17);
            this.label22.TabIndex = 79;
            this.label22.Text = "P";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label21.Location = new System.Drawing.Point(228, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 17);
            this.label21.TabIndex = 78;
            this.label21.Text = "A";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label20.Location = new System.Drawing.Point(195, 141);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 17);
            this.label20.TabIndex = 77;
            this.label20.Text = "R";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label19.Location = new System.Drawing.Point(-5, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 17);
            this.label19.TabIndex = 76;
            this.label19.Text = "L";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // tissue_image
            // 
            this.tissue_image.Location = new System.Drawing.Point(0, 0);
            this.tissue_image.Name = "tissue_image";
            this.tissue_image.Size = new System.Drawing.Size(57, 50);
            this.tissue_image.TabIndex = 75;
            this.tissue_image.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(141, 16);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 15);
            this.label8.TabIndex = 31;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(764, 563);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "HDExplore";
            this.Move += new System.EventHandler(this.Form1_Move);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox_pads.ResumeLayout(false);
            this.groupBox_pads.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tissue_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTipRun;
        private System.Windows.Forms.ToolTip toolTipNumElectrode;
        private System.Windows.Forms.ToolTip toolTipVisualizationGUI;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.ToolTip toolTipDataGridView1;
        private System.Windows.Forms.ToolTip toolTipOpen;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutHDExploreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonOverlay;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonField;
        private System.Windows.Forms.Button buttonMRI;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTipTopplot;
        private System.Windows.Forms.ToolTip toolTipMaxCurrent;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ToolStripDropDownButton toolStripComboBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolTip toolTipDataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Current;
        private System.Windows.Forms.TextBox textBox_current;
        private System.Windows.Forms.RadioButton radioButton_anode;
        private System.Windows.Forms.RadioButton radioButton_cathode;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton_flexible;
        private System.Windows.Forms.RadioButton radioButton_uniform;
        private System.Windows.Forms.Label label_total_current;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton radioButton_electrodes;
        private System.Windows.Forms.RadioButton radioButton_pads;
        private System.Windows.Forms.GroupBox groupBox_pads;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem saveAll3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coronalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sagittalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem axialToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip_zoom;
        private System.Windows.Forms.ToolTip toolTip_maxcurrent;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DomainUpDown domainUpDown5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.DomainUpDown domainUpDown3;
        private System.Windows.Forms.DomainUpDown domainUpDown2;
        private System.Windows.Forms.Label labelFieldIntensity;
        private System.Windows.Forms.DomainUpDown domainUpDown4;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem adultHeadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Adult_male_1_93_electrode_model;
        private System.Windows.Forms.ToolStripMenuItem Adult_male_1_332_electrode_model;
        private System.Windows.Forms.ToolStripMenuItem item12ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item13ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Adult_male_2_93_electrode_model;
        private System.Windows.Forms.ToolStripMenuItem item1dToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mni152toolstripmenu;
        private System.Windows.Forms.ToolStripMenuItem MNI_152_93_electrode_model;
        private System.Windows.Forms.ToolStripMenuItem item2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_AH;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_DE;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_JC;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_JE;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_KS;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_LM;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_MC;
        private System.Windows.Forms.ToolStripMenuItem Stroke_patient_RS;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.PictureBox tissue_image;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ToolStripMenuItem adultMale3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Adult_male_3_93_electrode_model;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
    }
}

